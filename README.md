SpeakEnglishSite Administracion Back-end

Rutas de la api URL: https://us-central1-admin-speakenglishsite.cloudfunctions.net/api

  Users:
    - URL: /users/id
      metodo: GET
      parametros:
        name: Nombre del usuario
        lastnem: Apellido del usuario

      respuesta:
  Languages:
    - URL: /lenguages
      -- Agregar Lenguajes nuevos 
          metodo: POSt
          parametro:
            name: Nombre del lenguaje
            acronym: Acronimo del nombre del lenguaje
          response: 
            success: Boolean (true/false)
            mes: Mensaje del estado de la operación
      -- Buscar un solo Lenguaje
          metodo: GET
          parametros:
            id: identificador de lenguaje (uid)
          response: 
            success: Boolean (true/false)
            item: Object
                    acronym: Acronimo del lenguaje
                    name: Nombre del lenguaje
            id: identificador de lenguaje consultado
      -- Modifacr Lenguaje
          metodo: PUT
          parametros:
            name: Nombre del lenguaje
            acronym: Acronimo del nombre del lenguaje
          response: 
            success: Boolean (true/false)
            mes: Mensaje del estado de la operación
          Nota: "Para los parametro a modicar es opcional cualquiera de los dos parametro especificados, pero es necesario enviar aunque sea uno de ellos"
      -- Eliminar Lenguage
          metodo: DELETE
          parametros:
            id: identificador de lenguaje (uid)
          response: 
            success: Boolean (true/false)
            mes: Mensaje del estado de la operación
            id: identificador de lenguaje consultado
SpeakEnglishSite Administracion Front-end

URL del de la pagina: https://www.admin.speakenglish.com  
  
  Modulo Users:
    Listar Users -> branch: master
    Crear Users -> branch: master
    Modificar Users -> branch: master
    Delete Users -> branch: master
    Ver Users -> branch: master
  Modulo Languages:
    Listar Languages -> branch: master / JM
    Crear Languages -> branch: master / JM
    Modificar Languages -> branch: master / JM
    Delete Languages -> branch: master / JM
    Ver Languages:
      Nota: "Esta funcionalidad no va crearse porque no es necesaria."
  Modulo Money:
    Listar Money -> branch: master / JM
    Crear Money -> branch: master / JM
    Modificar Money -> branch master / JM
    Delete Money -> branch: master / JM
    Ver Money:
      Nota: "Esta funcionalidad no va crearse porque no es necesaria."






<!-- Errores:  -->
  <!-- => Error de react-material, /src/component/users/index.js -->

