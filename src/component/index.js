import Home from './home';
import Layout from './layout';
import Auth from './auth';
import Users from './users';
import Languages from './languages';
import currentMoney from './currentMoney';
import Applications from './applications';
import Teachers from './teachers';
export {
	Home,
	Layout,
	Auth,
	Users,
	Languages,
	currentMoney,
	Applications,
	Teachers
}