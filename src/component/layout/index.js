import React, { Component } from 'react';
import { Route } from "react-router-dom";

import AppBarCustom from './appbar';
import DrawerLeft from './drawer';

import { Snackbar, Profile } from '../../utils';

import './index.css';

class Layout extends Component {
	state = {
		title: null,
		drawer: false,
	}
	constructor(props){
		super(props)
		this.LoopRoutes = this.LoopRoutes.bind(this)
		this.changeTitle = this.changeTitle.bind(this)
		this.toggleDrawer = this.toggleDrawer.bind(this)
	}
	toggleDrawer = event => {
		this.setState({drawer: event})
	}
	changeTitle = title => {
		this.setState({title: title})
	}
	LoopRoutes = (routes, snackbar, snackbarClose) => routes.map((route, i) =>
		<Route
			key={i}
			path={route.path}
			exact={route.exact}
			render={props => 
				<route.component 
					{...props} 
					{...this.props} 
					handleSnackbar={snackbar} 
					handleSnackbarClose={snackbarClose} 
					routes={route.routes} 
					title={this.changeTitle}
				/>
			}
		/>
	)
	render(){
		const { routes } = this.props;
		const { title, drawer } = this.state;
		return (
			<div className="wrapper">
				<AppBarCustom 
					title={title} 
					toggleDrawer={this.toggleDrawer} 
					drawer={drawer} 
					{...this.props} 
					handleOpenAccount={this.state.onClickOpenAccount}
				/>
				<DrawerLeft 
					toggleDrawer={this.toggleDrawer} 
					drawer={drawer} 
					{...this.props}
				/>
				<Profile 
					handleOpen={event => this.setState({ onClickOpenAccount: event })} 
					handleClose={event => this.setState({ onClickCloseAccount: event })} 
					{...this.props} 
					handleSnackbar={this.state.snackbar} 
					handleSnackbarClose={this.state.snackbarClose}
				/>
				<main>
					<div className="white-space"></div>
					{
						routes 
							&& 
						this.LoopRoutes(
							routes, 
							this.state.snackbar, 
							this.state.snackbarClose
						)
					}	
				</main>
				<Snackbar 
					handleClick={event => this.setState({ snackbar: event })} 
					handleClose={event => this.setState({ snackbarClose: event })}
				/>
			</div>
		)
	}
}

export default Layout;