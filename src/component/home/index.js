import React, { Component } from 'react';
import axios from 'axios';
import { Calendar } from '../../utils';
import './index.css';

const schedule = [
  {
    date: '2018-6-30 00:00',
    booking: true,
  },
  {
    date: '2018-6-30 23:00',
    booking: false,
  },
  {
    date: '2018-6-30 12:00',
    booking: false,
  },
  {
    date: '2018-6-30 00:00',
    booking: true,
  },
  {
    date: '2018-6-30 05:00',
    booking: false,
  },
  {
    date: '2018-7-01 10:00',
    booking: true,
  },
  {
    date: '2018-7-01 13:00',
    booking: false,
  },
  {
    date: '2018-7-01 19:00',
    booking: true,
  }
]

class Home extends Component {
	state = {}
	constructor(props){
		super()
		props.title('Tablero')
	}
	componentDidMount(){
		document.title = 'Tablero - SpeakEnglishSite'
  }
	render(){
		return (
			<div>
				<h1>{this.props.user.auth.displayName}</h1>
				<span>{this.props.user.auth.email}</span>
				<Calendar
					schedule={schedule} 
					seletedAll={true}
					// limit={2}
					onChange={ event => console.log(event) }
				/>
			</div>
		)
	}
}

export default Home;