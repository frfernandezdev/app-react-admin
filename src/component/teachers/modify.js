import React, { Component } from 'react';
import axios from 'axios';
import {
	MenuItem,
	TextField,
	Typography,
	Paper,
	Grid,
	Icon,
	IconButton, 
	FormControl,
	FormGroup,
	FormControlLabel,
	Checkbox,
	Input,
	InputLabel,
	InputAdornment
} from '@material-ui/core';
import * as FontAwesome from 'react-icons/lib/fa';

import {
	SelectLanguageToLearn,
	SummaryCv,
	UploadButton,
	Calendar
} from '../../utils';

class Modify extends Component {
  state = {
		open: false,
		form: {
			skill: {
				test: {}
			},
			materialLearn: {}
		},
		teachers: {
			skill: {
				test: {}
			}
		},
		users: [],
		showpassword: false,
		disabledLanguage: true
  }
  constructor(props){
		super(props)

		this.firestore = props.firebase.firestore().collection('teachers');
		this.textFieldUsers = props.firebase.firestore().collection('users');
		this.storage   = props.firebase.storage();
		this.onSubmit  = this.onSubmit.bind(this)
		this.handleChange = this.handleChange.bind(this)
		this.handleOnClick = this.handleOnClick.bind(this)
		this.props.handleOnClick(this.handleOnClick)
  }
  componentWillMount(){
		if(
			this.props.location.state !== undefined	 
				&&
			'id' in this.props.location.state
				&&
			this.props.location.state.id !== undefined
		){
			console.log('here')
			
			this.setState({ 
				uid: this.props.location.state.id 
			}, () => this.handleDateInfo())
		}
		this.handleUsers()
  }
  componentWillUpdate(prevProps ,prevState){
		if(
			prevProps.location.state !== undefined 
				&& 
			'id' in prevProps.location.state 
				&&
			prevProps.location.state.id !== undefined
				&& 
			prevState.uid !== prevProps.location.state.id
		){
			this.setState(
				{ 
					uid: prevProps.location.state.id,
					send: { tools: {} } 
				}, () => this.handleDateInfo())
		}
  }
  handleDateInfo = () => {
		this.firestore
			.doc(this.state.uid)
			.onSnapshot(snapshot => {
				if(snapshot.exists){
					this.setState({
						teachers: {
							...snapshot.data(),
							id: snapshot.id
						}
					})
				}else console.log('Not Found teachers')
			}, error => console.log(error))
	}
	handleUsers(){
		this.textFieldUsers
			.orderBy('fullname')
			.onSnapshot(snapshot => {
				this.setState({ users: snapshot.docs })
			}, error => {
				console.log(error)
			})
	}
  handleChange = name => event => {
		if(this.state.teachers[name] === this.state.form[name])
			return;
		this.setState({
			form: {
				...this.state.form,
				[name]: event.target.value
			}
		})
	}
	handleChangeExpirent = name => event => {
		this.setState({
			form: {
				...this.state.form,
				summaryCV: event.target.value
			}
		})
	}
	handleChangeCheckbox = name => (event, checked) => {
		this.setState({
			form: {
				...this.state.form,
				skill: {
					...this.state.form.skill,
					[name]: checked
				}
			}
		})
	}
	handleChangeCheckboxTest = name => (event, checked) => {
		this.setState({
			form: {
				...this.state.form,
				skill: {
					...this.state.form.skill,
					test: {
						...this.state.form.skill.test,
						[name]: checked
					}
				}
			}
		})
	}
	handleChangeCheckboxLearn = name => (event, checked) => {
		this.setState({
			form: {
				...this.state.form,
				materialLearn: {
					...this.state.form.materialLearn,
					[name]: checked
				}
			}
		})
	}
  onSubmit = event => {
		event.preventDefault();
		this.props.handleOnLoading('show')

		let formData = new FormData();

		let object = Object.assign({}, this.state.form);

		delete object.summaryCV; 
		delete object.skill; 
		delete object.materialLearn;

		if(this.state.form.summaryCV && this.state.form.summaryCV.student)
			object.student = this.state.form.summaryCV.student;
		if(this.state.form.summaryCV && this.state.form.summaryCV.professional_expirence)
			object.professional_expirence = this.state.form.summaryCV.professional_expirence;
		if(Object.keys(this.state.form.skill).length > 1 || Object.keys(this.state.form.skill.test).length > 0){
			if(Object.keys(this.state.form.skill).length > 1){
				let skill = Object.assign({}, this.state.form.skill);
				delete skill.test;
				object.skill = skill;
			}
			if(Object.keys(this.state.form.skill.test).length > 0){
				if(object.hasOwnProperty('skill'))
					object.skill.test = this.state.form.skill.test;
				else {
					object.skill = {}
					object.skill.test = this.state.form.skill.test; 
				}
			}
		}
		formData.append('data', JSON.stringify(object));
		formData.append('fileDni', this.state.form.fileDni);
		
		if(object.student && object.student.length)
			object.student.forEach(docs => {
				formData.append('fileStudents', docs.file)
			})
		if(object.professional_expirence && object.professional_expirence.length)
			object.professional_expirence.forEach(docs => {
				formData.append('filePE', docs.file)
			})
		
		// http://localhost:5000/admin-speakenglishsite/us-central1/api/applications
		// https://us-central1-admin-speakenglishsite.cloudfunctions.net/api/applications
		axios.put(`https://us-central1-admin-speakenglishsite.cloudfunctions.net/api/teachers/${this.state.uid}`, 
			formData,
			{
				params: {
					token: this.props.user.auth.token
				}
			}
		)
		.then(response => {
			this.props.handleOnLoading('hide')
			if(response.data.success === true){
				this.props.handleSnackbar({type: 'custom', message: response.data.mes})
				setTimeout(() => {
					this.props.handleOnLoading('hide')
					this.setState({
						form: {
							skill: {
								test: {}
							},
							materialLearn: {}
						},
					})
					this.props.handleChange(null, 0) 
				}, 2500)
				this.props.handleOnLoading('done')
			}else {
				this.props.handleOnLoading('hide')
				if(response.data.error.code)
					return this.props.handleSnackbar({ type: 'auth', message: response.data.error.code, on: true })
				return this.props.handleSnackbar({ type: 'custom', message: response.data.error, on: true })
			}
		})
		.catch(error => {
			console.log(error)
			this.props.handleOnLoading('hide')
			if(error.response && error.response.data && error.response.data.error)
				return this.props.handleSnackbar({ type: 'auth', message: error.response.data.error.code, on: true })
			return this.props.handleSnackbar({ type: 'custom', message: '404', on: true })
		})
  }
  handleOnClick = () => {
		document.getElementById('modify/submit').click()
  }
  render(){
		const { id, value } = this.props;
		const {
			teachers,
			form,
			users,
			disabledLanguage
		} = this.state;
    return(
      <div>
        <Paper style={{padding: 15}}>
          <form
            id="modify"
            onSubmit={this.onSubmit}
            style={{ display: value === id ? 'block' : 'none' }}
          >
            <Typography variant="title" gutterBottom>Editar Profesor</Typography>
            <Grid container>
							<Grid
								item 
								xs={12}
								sm={12}
								md={12}
								lg={12}
								xl={12}
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<Typography variant="body2" style={{marginTop: 10}}>Datos del Profesor</Typography>
							</Grid>
							<Grid
								item 
								xs={12} 
								sm={12} 
								md={6} 
								lg={6} 
								xl={6} 
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<TextField
									id="new/user"
									label="Usuarios"
									select
									fullWidth
									margin="normal"
									disabled
									className="input-view"
									InputProps={{
										startAdornment: 
											<InputAdornment position="start">
												<Icon>account_circle</Icon>
											</InputAdornment>,
									}}
									value={
										form.hasOwnProperty('user') 
											? 
										form.user 
											: 
										teachers.user
											?
										teachers.user
											:
										''
									}
									onChange={this.handleChange('user')}
								>
									{users.map((docs, i) => {
										let collection = docs.data();
										return (
											<MenuItem key={i} value={docs.id}>
												{collection.fullname}
											</MenuItem>
										);
									})}
								</TextField>								
							</Grid>
							<Grid
								item 
								xs={12} 
								sm={12} 
								md={6} 
								lg={6} 
								xl={6} 
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<TextField
									id="new/video"
									label="Link Video"
									margin="normal"
									fullWidth
									type="url"
									InputProps={{
										startAdornment: 
											<InputAdornment>
												<Icon>video_library</Icon>
											</InputAdornment>	
									}}
									value={
										form.hasOwnProperty('video') 
											? 
										form.video 
											: 
										teachers.video
											?
										teachers.video
											:
										''
									}
									onChange={this.handleChange('video')}
								/>
							</Grid>
							<Grid 
								item 
								xs={12} 
								sm={12} 
								md={6} 
								lg={6} 
								xl={6} 
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<TextField
									id="new/about_me"
									label="Sobre mi"
									multiline
									fullWidth
									margin="normal"
									value={
										form.hasOwnProperty('aboutMe') 
											? 
										form.aboutMe 
											:
										teachers.aboutMe
											?
										teachers.aboutMe
											:
										''
									}
									onChange={this.handleChange('aboutMe')}
								/>
							</Grid>
							<Grid
								item 
								xs={12} 
								sm={12} 
								md={6} 
								lg={6} 
								xl={6} 
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<TextField
									id="new/methodology"
									label="Metodologia"
									multiline
									margin="normal"
									fullWidth
									value={
										form.hasOwnProperty('methodology') 
											? 
										form.methodology 
											:
										teachers.methodology
											?
										teachers.methodology
											: 
										''
									}
									onChange={this.handleChange('methodology')}
								/>
							</Grid>
							<Grid 
								item
								xs={12}
								sm={12}
								md={12}
								lg={12}
								xl={12}
							>
								<UploadButton 
									value={
										form.hasOwnProperty('fileDni') 
											? 
										form.fileDni 
											:
										teachers.fileDni
											?
										teachers.fileDni
											: 
										''
									} 
									onChange={this.handleChange('fileDni')}
									labelButton="Subir imagen del DNI" 
									labelButtonSuccess="Ver DNI" 
									labelDialog="Subir image del DNI"
									style={
										{
											textAlign: 'center',
											paddingTop: 50,
											paddingBottom: 20
										}
									}
								/>
							</Grid>
							<Grid 
								item 
								xs={12} 
								sm={12} 
								md={12} 
								lg={12} 
								xl={12} 
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<Typography variant="body2" style={{marginTop: 10}}>Idiomas a Enseñar</Typography>
							</Grid>
							<Grid
								item 
								xs={12} 
								sm={12} 
								md={12} 
								lg={12} 
								xl={12} 
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<SelectLanguageToLearn 
									id="new/languageToLearn"
									value={
										form.hasOwnProperty('languageToLearn')
											?
										form.languageToLearn
											:
										teachers.languageToLearn
											?
										teachers.languageToLearn
											:
										''											
									}
									onChange={this.handleChange('languageToLearn')}
									firebase={this.props.firebase}
								/>
							</Grid>
							<Grid 
								item 
								xs={12} 
								sm={12} 
								md={12} 
								lg={12} 
								xl={12} 
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<Typography variant="body2" style={{marginTop: 10}}>Educacion y Experiencia Laboral</Typography>
							</Grid>
							<Grid
								item 
								xs={12} 
								sm={12} 
								md={12} 
								lg={12} 
								xl={12} 
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<SummaryCv
									value={
										form.hasOwnProperty('summaryCV')
											?
										form.summaryCV
											:
										teachers.student || teachers.professional_expirence
											?
											{
												student: teachers.student ? [...teachers.student] : [],
												professional_expirence:  teachers.professional_expirence ? [...teachers.professional_expirence] : []
											}
											:
										''
									}
									// delete={form.summaryCV ? false : true}
									onChange={this.handleChangeExpirent('summaryCV')}
								/>
							</Grid>
							<Grid
								item
								xs={12}
								sm={12}
								md={12}
								lg={12}
								xl={12}
							>
								<Calendar
									schedule={
										form.hasOwnProperty('schedule')
											?
										form.schedule
											:
										teachers.schedule 
											? 
										teachers.schedule 
											: 
										''
									} 
									seletedAll={true}
									onChange={this.handleChange('schedule')}
								/>
							</Grid>
							<Grid 
								item 
								xs={12} 
								sm={12} 
								md={12} 
								lg={12} 
								xl={12} 
								style={{
									marginTop: 10,
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<Typography variant="body2" style={{marginTop: 10}}>Habilidades de enseñanza</Typography>
							</Grid>
							<Grid
								item
								xs={12}
								sm={12}
								md={6}
								lg={6}
								xl={6}
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<FormControl 
									margin="normal"
									fullWidth
									component="fieldset"
								>
									<FormGroup>
										<FormControlLabel
											control={
												<Checkbox
													checked={
														form.skill.hasOwnProperty('junnior') 
															? 
														form.skill.junnior 
															:
														teachers.skill.hasOwnProperty('junnior') 
															?
														teachers.skill.junnior
															: 
														false
													}
													onChange={this.handleChangeCheckbox('junnior')}
												/>	
											}
											label="Principiantes"
										/>
									</FormGroup>
								</FormControl>
								<FormControl 
									margin="normal"
									fullWidth
									component="fieldset"
								>
									<FormGroup>
										<FormControlLabel
											control={
												<Checkbox
													checked={
														form.skill.hasOwnProperty('business') 
															? 
														form.skill.business 
															:
														teachers.skill.hasOwnProperty('business') 
															?
														teachers.skill.business
															: 
														false
													}
													onChange={this.handleChangeCheckbox('business')}
												/>	
											}
											label="Negocios"
										/>
									</FormGroup>
								</FormControl>
							</Grid>
							<Grid
								item
								xs={12}
								sm={12}
								md={6}
								lg={6}
								xl={6}
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<FormControl 
									margin="normal"
									fullWidth
									component="fieldset"
								>
									<FormGroup>
										<FormControlLabel
											control={
												<Checkbox
													checked={
														form.skill.hasOwnProperty('kids') 
															? 
														form.skill.kids 
															: 
														teachers.skill.hasOwnProperty('kids') 
															?
														teachers.skill.kids
															:
														false
													}
													onChange={this.handleChangeCheckbox('kids')}
												/>	
											}
											label="Niños"
										/>
									</FormGroup>
								</FormControl>
								<FormControl 
									margin="normal"
									fullWidth
									component="fieldset"
								>
									<FormGroup>
										<FormControlLabel
											control={
												<Checkbox
													checked={
														form.skill.hasOwnProperty('teen') 
															? 
														form.skill.teen 
															: 
														teachers.skill.hasOwnProperty('teen') 
															?
														teachers.skill.teen
															:
														false
													}
													onChange={this.handleChangeCheckbox('teen')}
												/>	
											}
											label="Jovenes"
										/>
									</FormGroup>
								</FormControl>
							</Grid>
							<Grid 
								item 
								xs={12} 
								sm={12} 
								md={12} 
								lg={12} 
								xl={12} 
								style={{
									marginTop: 10,
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<Typography variant="body2" style={{marginTop: 10}}>Preparacion para Test</Typography>
							</Grid>
							<Grid
								item
								xs={12}
								sm={12}
								md={3}
								lg={3}
								xl={3}
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<FormControl 
									margin="normal"
									fullWidth
									component="fieldset"
								>
									<FormGroup>
										<FormControlLabel
											control={
												<Checkbox
													checked={
														form.skill.test.hasOwnProperty('IELTS') 
															? 
														form.skill.test.IELTS 
															: 
														teachers.skill.test.hasOwnProperty('IELTS') 
															?
														teachers.skill.test.IELTS
															:
														false
													}
													onChange={this.handleChangeCheckboxTest('IELTS')}
												/>	
											}
											label="IELTS"
										/>
									</FormGroup>
								</FormControl>
								<FormControl 
									margin="normal"
									fullWidth
									component="fieldset"
								>
									<FormGroup>
										<FormControlLabel
											control={
												<Checkbox
													checked={
														form.skill.test.hasOwnProperty('TOEIC')  
															? 
														form.skill.test.TOEIC 
															: 
														teachers.skill.test.hasOwnProperty('TOEIC') 
															?
														teachers.skill.test.TOEIC
															:
														false
													}
													onChange={this.handleChangeCheckboxTest('TOEIC')}
												/>	
											}
											label="TOEIC"
										/>
									</FormGroup>
								</FormControl>
								<FormControl 
									margin="normal"
									fullWidth
									component="fieldset"
								>
									<FormGroup>
										<FormControlLabel
											control={
												<Checkbox
													checked={
														form.skill.test.hasOwnProperty('CELU') 
															? 
														form.skill.test.CELU 
															: 
														teachers.skill.test.hasOwnProperty('CELU') 
															?
														teachers.skill.test.CELU
															:
														false
													}
													onChange={this.handleChangeCheckboxTest('CELU')}
												/>	
											}
											label="CELU"
										/>
									</FormGroup>
								</FormControl>
							</Grid>
							<Grid
								item
								xs={12}
								sm={12}
								md={3}
								lg={3}
								xl={3}
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<FormControl 
									margin="normal"
									fullWidth
									component="fieldset"
								>
									<FormGroup>
										<FormControlLabel
											control={
												<Checkbox
													checked={
														form.skill.test.hasOwnProperty('BEC') 
															? 
														form.skill.test.BEC 
															: 
														teachers.skill.test.hasOwnProperty('BEC') 
															?
														teachers.skill.test.BEC
															:
														false
													}
													onChange={this.handleChangeCheckboxTest('BEC')}
												/>	
											}
											label="BEC"
										/>
									</FormGroup>
								</FormControl>
								<FormControl 
									margin="normal"
									fullWidth
									component="fieldset"
								>
									<FormGroup>
										<FormControlLabel
											control={
												<Checkbox
													checked={
														form.skill.test.hasOwnProperty('CAE') 
															? 
														form.skill.test.CAE 
															:
														teachers.skill.test.hasOwnProperty('CAE')
															?
														teachers.skill.test.CAE 
															:
														false
													}
													onChange={this.handleChangeCheckboxTest('CAE')}
												/>	
											}
											label="CAE"
										/>
									</FormGroup>
								</FormControl>
								<FormControl 
									margin="normal"
									fullWidth
									component="fieldset"
								>
									<FormGroup>
										<FormControlLabel
											control={
												<Checkbox
													checked={
														form.skill.test.hasOwnProperty('KET')
															? 
														form.skill.test.KET 
															: 
														teachers.skill.test.hasOwnProperty('KET')
															?
														teachers.skill.test.KET
															:
														false
													}
													onChange={this.handleChangeCheckboxTest('KET')}
												/>	
											}
											label="KET"
										/>
									</FormGroup>
								</FormControl>
							</Grid>
							<Grid
								item
								xs={12}
								sm={12}
								md={3}
								lg={3}
								xl={3}
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<FormControl 
									margin="normal"
									fullWidth
									component="fieldset"
								>
									<FormGroup>
										<FormControlLabel
											control={
												<Checkbox
													checked={
														form.skill.test.hasOwnProperty('TOEFL') 
															? 
														form.skill.test.TOEFL 
															: 
														teachers.skill.test.hasOwnProperty('TOEFL') 
															?
														teachers.skill.test.TOEFL
															:
														false
													}
													onChange={this.handleChangeCheckboxTest('TOEFL')}
												/>	
											}
											label="TOEFL"
										/>
									</FormGroup>
								</FormControl>
								<FormControl 
									margin="normal"
									fullWidth
									component="fieldset"
								>
									<FormGroup>
										<FormControlLabel
											control={
												<Checkbox
													checked={
														form.skill.test.hasOwnProperty('DELE') 
															? 
														form.skill.test.DELE 
															: 
														teachers.skill.test.hasOwnProperty('DELE') 
															?
														teachers.skill.test.DELE
															:
														false
													}
													onChange={this.handleChangeCheckboxTest('DELE')}
												/>	
											}
											label="DELE"
										/>
									</FormGroup>
								</FormControl>
								<FormControl 
									margin="normal"
									fullWidth
									component="fieldset"
								>
									<FormGroup>
										<FormControlLabel
											control={
												<Checkbox
													checked={
														form.skill.test.hasOwnProperty('FCE') 
															? 
														form.skill.test.FCE 
															:
														teachers.skill.test.hasOwnProperty('FCE') 
															?
														teachers.skill.test.FCE
															:
														false
													}
													onChange={this.handleChangeCheckboxTest('FCE')}
												/>	
											}
											label="FCE"
										/>
									</FormGroup>
								</FormControl>
							</Grid>
							<Grid
								item
								xs={12}
								sm={12}
								md={3}
								lg={3}
								xl={3}
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<FormControl 
									margin="normal"
									fullWidth
									component="fieldset"
								>
									<FormGroup>
										<FormControlLabel
											control={
												<Checkbox
													checked={
														form.skill.test.hasOwnProperty('PET') 
															? 
														form.skill.test.PET 
															: 
														teachers.skill.test.hasOwnProperty('PET') 
															?
														teachers.skill.test.PET
															:
														false
													}
													onChange={this.handleChangeCheckboxTest('PET')}
												/>	
											}
											label="PET"
										/>
									</FormGroup>
								</FormControl>
								<FormControl 
									margin="normal"
									fullWidth
									component="fieldset"
								>
									<FormGroup>
										<FormControlLabel
											control={
												<Checkbox
													checked={
														form.skill.test.hasOwnProperty('CPE') 
															? 
														form.skill.test.CPE 
															: 
														teachers.skill.test.hasOwnProperty('CPE') 
															?
														teachers.skill.test.CPE
															:
														false
													}
													onChange={this.handleChangeCheckboxTest('CPE')}
												/>	
											}
											label="CPE"
										/>
									</FormGroup>
								</FormControl>
								<FormControl 
									margin="normal"
									fullWidth
									component="fieldset"
								>
									<FormGroup>
										<FormControlLabel
											control={
												<Checkbox
													checked={
														form.skill.test.hasOwnProperty('ILEC') 
															? 
														form.skill.test.ILEC 
															: 
														teachers.skill.test.hasOwnProperty('ILEC') 
															?
														teachers.skill.test.ILEC
															:
														false
													}
													onChange={this.handleChangeCheckboxTest('ILEC')}
												/>	
											}
											label="ILEC"
										/>
									</FormGroup>
								</FormControl>
							</Grid>
							<Grid 
								item 
								xs={12} 
								sm={12} 
								md={12} 
								lg={12} 
								xl={12} 
								style={{
									marginTop: 10,
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<Typography variant="body2" style={{marginTop: 10}}>Materiales de enseñanza</Typography>
							</Grid>
							<Grid
								item
								xs={12}
								sm={12}
								md={3}
								lg={3}
								xl={3}
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<FormControl 
									margin="normal"
									fullWidth
									component="fieldset"
								>
									<FormGroup>
										<FormControlLabel
											control={
												<Checkbox
													checked={
														form.materialLearn.hasOwnProperty('file_pdf') 
															? 
														form.materialLearn.file_pdf 
															: 
														teachers.materialLearn
															?
														teachers.materialLearn.file_pdf
															:
														false
													}
													onChange={this.handleChangeCheckboxLearn('file_pdf')}
												/>	
											}
											label="Archivos PDF"
										/>
									</FormGroup>
								</FormControl>
								<FormControl 
									margin="normal"
									fullWidth
									component="fieldset"
								>
									<FormGroup>
										<FormControlLabel
											control={
												<Checkbox
													checked={
														form.materialLearn.hasOwnProperty('article_news') 
															? 
														form.materialLearn.article_news 
															: 
														teachers.materialLearn
															?
														teachers.materialLearn.article_news
															:
														false
													}
													onChange={this.handleChangeCheckboxLearn('article_news')}
												/>	
											}
											label="Articulos de noticias"
										/>
									</FormGroup>
								</FormControl>
								<FormControl 
									margin="normal"
									fullWidth
									component="fieldset"
								>
									<FormGroup>
										<FormControlLabel
											control={
												<Checkbox
													checked={
														form.materialLearn.hasOwnProperty('task') 
															? 
														form.materialLearn.task 
															: 
														teachers.materialLearn
															?
														teachers.materialLearn.task
															:
														false
													}
													onChange={this.handleChangeCheckboxLearn('task')}
												/>	
											}
											label="Tareas"
										/>
									</FormGroup>
								</FormControl>
							</Grid>
							<Grid
								item
								xs={12}
								sm={12}
								md={3}
								lg={3}
								xl={3}
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<FormControl 
									margin="normal"
									fullWidth
									component="fieldset"
								>
									<FormGroup>
										<FormControlLabel
											control={
												<Checkbox
													checked={
														form.materialLearn.hasOwnProperty('template_quiz') 
															? 
														form.materialLearn.template_quiz 
															: 
														teachers.materialLearn
															?
														teachers.materialLearn.template_quiz
															:
														false
													}
													onChange={this.handleChangeCheckboxLearn('template_quiz')}
												/>	
											}
											label="Quiz"
										/>
									</FormGroup>
								</FormControl>
								<FormControl 
									margin="normal"
									fullWidth
									component="fieldset"
								>
									<FormGroup>
										<FormControlLabel
											control={
												<Checkbox
													checked={
														form.materialLearn.hasOwnProperty('grafic_table') 
															? 
														form.materialLearn.grafic_table 
															: 
														teachers.materialLearn
															?
														teachers.materialLearn.grafic_table
															:
														false
													}
													onChange={this.handleChangeCheckboxLearn('grafic_table')}
												/>	
											}
											label="Tabla de Graficos"
										/>
									</FormGroup>
								</FormControl>
								<FormControl 
									margin="normal"
									fullWidth
									component="fieldset"
								>
									<FormGroup>
										<FormControlLabel
											control={
												<Checkbox
													checked={
														form.materialLearn.hasOwnProperty('file_video') 
															? 
														form.materialLearn.file_video 
															: 
														teachers.materialLearn
															?
														teachers.materialLearn.file_video
															:
														false
													}
													onChange={this.handleChangeCheckboxLearn('file_video')}
												/>	
											}
											label="Videos"
										/>
									</FormGroup>
								</FormControl>
							</Grid>
							<Grid
								item
								xs={12}
								sm={12}
								md={3}
								lg={3}
								xl={3}
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<FormControl 
									margin="normal"
									fullWidth
									component="fieldset"
								>
									<FormGroup>
										<FormControlLabel
											control={
												<Checkbox
													checked={
														form.materialLearn.hasOwnProperty('file_text') 
															? 
														form.materialLearn.file_text 
															: 
														teachers.materialLearn
															?
														teachers.materialLearn.file_text
															:
														false
													}
													onChange={this.handleChangeCheckboxLearn('file_text')}
												/>	
											}
											label="Archivos de texto"
										/>
									</FormGroup>
								</FormControl>
								<FormControl 
									margin="normal"
									fullWidth
									component="fieldset"
								>
									<FormGroup>
										<FormControlLabel
											control={
												<Checkbox
													checked={
														form.materialLearn.hasOwnProperty('presentation') 
															? 
														form.materialLearn.presentation 
															:
														teachers.materialLearn
															?
														teachers.materialLearn.presentation
															: 
														false
													}
													onChange={this.handleChangeCheckboxLearn('presentation')}
												/>	
											}
											label="Presentaciones"
										/>
									</FormGroup>
								</FormControl>
								<FormControl 
									margin="normal"
									fullWidth
									component="fieldset"
								>
									<FormGroup>
										<FormControlLabel
											control={
												<Checkbox
													checked={
														form.materialLearn.hasOwnProperty('file_audio') 
															? 
														form.materialLearn.file_audio 
															:
														teachers.materialLearn 
															?
														teachers.materialLearn.file_audio
															:
														false
													}
													onChange={this.handleChangeCheckboxLearn('file_audio')}
												/>	
											}
											label="Audios"
										/>
									</FormGroup>
								</FormControl>
							</Grid>
							<Grid
								item
								xs={12}
								sm={12}
								md={3}
								lg={3}
								xl={3}
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<FormControl 
									margin="normal"
									fullWidth
									component="fieldset"
								>
									<FormGroup>
										<FormControlLabel
											control={
												<Checkbox
													checked={
														form.materialLearn.hasOwnProperty('file_image') 
															? 
														form.materialLearn.file_image 
															:
														teachers.materialLearn 
															?
														teachers.materialLearn.file_image
															:
														false
													}
													onChange={this.handleChangeCheckboxLearn('file_image')}
												/>	
											}
											label="Imagenes"
										/>
									</FormGroup>
								</FormControl>
								<FormControl 
									margin="normal"
									fullWidth
									component="fieldset"
								>
									<FormGroup>
										<FormControlLabel
											control={
												<Checkbox
													checked={
														form.materialLearn.hasOwnProperty('questionnaries') 
															? 
														form.materialLearn.questionnaries 
															: 
														teachers.materialLearn
															?
														teachers.materialLearn.questionnaries
															:
														false
													}
													onChange={this.handleChangeCheckboxLearn('questionnaries')}
												/>	
											}
											label="Cuestionarios"
										/>
									</FormGroup>
								</FormControl>
							</Grid>
            </Grid>
						<button type="submit" id="modify/submit" style={{display: 'none'}}>Enviar</button>	
          </form>
        </Paper>
      </div>
    )
  }
}

export default Modify;