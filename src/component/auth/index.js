import React, { Component } from 'react';
import { 
	Grid,
	Paper,
	AppBar,
	Icon,
	Tabs,
	Tab
} from '@material-ui/core';
import SwipeableViews from 'react-swipeable-views';
import { Snackbar, SnackbarLoading } from '../../utils';

import Login from './login';
import Register from './register';

import './index.css';

class Auth extends Component {
	state = {
		value: 0,
		open: false,
		msj: null
	}
	componentWillMount(){
		this.props.history.push('/')
	}
	handleChange = (event, value) => {
		this.setState({ value })
	}
	render() {
		const height = window.innerHeight - 1;
		const { error } = this.props;
		const { 
			snackbar, 
			snackbarClose,
			snackbarLoading,
			snackbarLoadingClose
		} = this.state;
		return (
			<div 
				className="background" 
				// style={{height: height}}
			>
				<Grid container spacing={16}>
					<Grid 
						item 
						xs={12} 
						sm={12} 
						md={8} 
						lg={9} 
						xl={9} 
						// style={{height: height}} 
						className="p-0"
					>
					</Grid>
					<Grid
						id="grid-form-login" 
						item 
						xs={12} 
						sm={12} 
						md={4} 
						lg={3}
						xl={3} 
						// style={{height: height}} 
						className="p-0"
					>
						<Paper>
							<Grid
								container
								direction='column'
								justify='center'
								style={{ display: 'flex', height: '100vh' }} 
							>
								<Grid>
									<SwipeableViews index={this.state.value}>
										<Login {...this.props} snackbar={snackbar} snackbarClose={snackbarClose} snackbarLoading={snackbarLoading} snackbarLoadingClose={snackbarLoadingClose} tabs={this.handleChange}/>
										{/* <Register {...this.props} snackbar={snackbar} snackbarClose={snackbarClose} tabs={this.handleChange}/> */}
									</SwipeableViews>
								</Grid>
								{/* <Grid>
									<AppBar position="static" color="default">
										<Tabs
											value={this.state.value}
											onChange={this.handleChange}
											indicatorColor="primary"
											textColor="primary"
											fullWidth
											centered
										>
											<Tab label="Iniciar Sesión" />
											<Tab label="Registrate" />
										</Tabs>
									</AppBar>
								</Grid> */}
							</Grid>
						</Paper>
					</Grid>
				</Grid>
				<Snackbar handleClick={event => this.setState({ snackbar: event })} handleClose={event => this.setState({ snackbarClose: event })}/>
				<SnackbarLoading handleClick={event => this.setState({ snackbarLoading: event })} handleClose={event => this.setState({ snackbarLoadingClose: event })}></SnackbarLoading>
			</div>
		);
	}
}

export default Auth;