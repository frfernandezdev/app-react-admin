import React, { Component } from 'react';

import {
	TextField,
	Typography,
	Button,
	CircularProgress,
	MenuItem,
	Icon
} from '@material-ui/core';

const Regex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

class Register extends Component {
	state = {
		form: {
			gender: 'false',
			birthdate: '1996-03-28'
		},
		snack: {
			open: false,
			msj: null
		},
		loading: 'Registrate',
		error: {
			email: false,
			password: false
		}
	}
	constructor(props){
		super();

		let today = new Date();

		this.state = {
			...this.state,
			today: `${today.getFullYear()}-${today.getMonth() < 10 ? `0${today.getMonth()}`: today.getMonth()}-${today.getDate() < 10 ? `0${today.getDate()}`: today.getDate()}`,
		}

		this.functions = props.firebase.functions();
		this.handleAuth = this.handleAuth.bind(this)
		this.handlerError =this.handlerError.bind(this)
	}
	handlerError(){
		this.setState({
			error: {
				username: this.state.form.username ? false: true,
				email: this.state.form.email && Regex.test(this.state.form.email) ? false: true,
				password: this.state.form.password && this.state.form.password.length===8 ? false: true,
			},
			msj: {
				email: this.state.form.email && Regex.test(this.state.form.email) ? '': 'Formato Invalido del Correo Electronico',
				password: this.state.form.password && this.state.form.password.length===8 ? '': 'La contraseña debe contener una minimo de 8 carateres'
			}			
		})
	}
	handleAuth(e) {
		e.preventDefault();

		this.setState({loading: <CircularProgress size={20} color='inherit' />})

		let form = {
			email: this.state.form.email,
			password: this.state.form.password,
			fullname: this.state.form.fullname,
			username: this.state.form.username,
			gender: this.state.form.gender,
			birthdate: this.state.form.birthdate,
			phone: this.state.form.phone
		}
		var register = this.functions.httpsCallable('registerUser');
 
		register(form)
			.then(res => {
				console.log(res)
				if(res.data.success){
					this.setState({loading: 'Registrate'})
					// this.props.snackbar(true, res.data.mes)
					this.props.tabs(0, 0);
				}
					// this.props.snackbar(true, res, true)
			})
			.catch(err => {
				console.log(err)
				this.setState({loading: 'Registrate'})
				// this.props.snackbar(true, 'Oops, ¡Ha ocurrido un error interno!', true)
			})
	}
	render(){
		return (
			<div className='form'>
				<form onSubmit={this.handleAuth}>
					<Typography variant="title" gutterBottom>Registrate</Typography>
					<TextField
						id='username'
						label="Nombre de Usuario"
						margin="normal"
						fullWidth={true}
						required={true}
						error={this.state.error.username}
						type='text'
						onChange={event => this.setState({
							form:{
								...this.state.form,
								username: event.target.value
							}	
						})}
					/>
					<TextField
						id="register/mail"
						label="Correo Electronico"
						margin="normal"
						fullWidth={true}
						required={true}
						error={this.state.error.email}
						type='email'
						onChange={event => this.setState({
							form:{
								...this.state.form,
								email: event.target.value
							}	
						})}
					/>
					<TextField
						id="register/password"
						label="Contraseña"
						margin="normal"
						fullWidth={true}
						required={true}
						error={false}
						type='password'
						onChange={event => this.setState({
							form: {
								...this.state.form,
								password: event.target.value
							}
						})}
					/>
					<TextField
						id="fullname"
						label="Nombre Completo"
						margin="normal"
						fullWidth={true}
						required={true}
						error={false}
						type='text'
						inputProps={{
							pattern: '[A-Za-z ]{1,}',
							title: 'Solo se aceptan letras'
						}}
						onChange={event => this.setState({
							form: {
								...this.state.form,
								fullname: event.target.value
							}
						})}
					/>
					<TextField
						id="phone"
						label="Telefono"
						placeholder="+581234567890"
						margin="normal"
						fullWidth={true}
						required={true}
						error={false}
						type='phone'
						inputProps={{
							pattern: '[+][0-9]{12}',
							title: 'Solo se aceptan letras'
						}}
						onChange={event => this.setState({
							form: {
								...this.state.form,
								phone: event.target.value
							}
						})}
					/>
					<TextField
						id="gender"
						select
						label="Select"
						value={this.state.form.gender}
						onChange={event => this.setState({
							form: {
								...this.state.form,
								gender: event.target.value
							}
						})}
						fullWidth={true}
						margin="normal"
					>
						<MenuItem key={'false'} value={'false'}>
							Selecciona un Opcion
						</MenuItem>
						<MenuItem key={'M'} value={'M'}>
							Masculino
						</MenuItem>
						<MenuItem key={'F'} value={'F'}>
							Femenino
						</MenuItem>
					</TextField>
					<TextField
						id="birthdate"
						label="Fecha de Cumpleaños"
						margin="normal"
						defaultValue={this.state.today}
						fullWidth={true}
						required={true}
						error={false}
						type='date'
						onChange={event => this.setState({
							form: {
								...this.state.form,
								birthdate: event.target.value
							}
						})}
					/>
					<Button 
						type="submit"
						variant="raised" 
						fullWidth={true} 
						className="btn btn-signin"
						color='primary'
						onClick={this.handlerError}
					>
						{this.state.loading}
					</Button>
				</form>
			</div>
		)
	}
}

export default Register;