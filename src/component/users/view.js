import React, { Component } from 'react';
import axios from 'axios';
import {
	Button,
	Dialog,
	AppBar,
	Toolbar,
	IconButton,
	Typography,
	Grid,
	Paper,
	TextField,
	FormControl,
	InputLabel,
	Input, 
	InputAdornment,
	Icon,
	MenuItem,
	Slide,
	Avatar,
	List,
	ListSubheader,
	ListItem,
	ListItemText,
	Divider,
	Card,
	CardMedia,
	CardContent,
	CardActions,
	CardHeader
} from '@material-ui/core';
import Google from 'react-icons/lib/fa/google';
import Facebook from 'react-icons/lib/fa/facebook-official';
import SwipeableViews from 'react-swipeable-views';
import * as FontAwesome from 'react-icons/lib/fa';
import {
	SelectLocalities,
	SelectTimeZone,
	SelectCurrentMoney,
	SelectLanguage,
	SelectLanguageNative,
	SnackbarLoading
} from '../../utils';

function Transition(props) {
	return <Slide direction="up" {...props} />;
}

class View extends Component {
	state = { 
		open: false, 
		uid: null, 
		user: {}, 
		page: 0 
	}
	constructor(props){
		super(props)

		this.auth = this.props.firebase.auth;
		this.handleOpen  = this.handleOpen.bind(this)
		this.handleClose = this.handleClose.bind(this)
		this.props.handleOpen(this.handleOpen)
		this.props.handleClose(this.handleClose)
	}
	handleDataInfo = () => {
		this.state.onClickSnackLoadingOpen()

		axios.get(`https://us-central1-admin-speakenglishsite.cloudfunctions.net/api/users/${this.state.uid}`, {
			params: {
				token: this.props.user.auth.token
			}
		})
		.then(response => {
			if(response.data.success === true){
				this.setState({ 
					open: true, 
					user: response.data.item, 
					google: response.data.item.providerData.find(docs => docs.providerId === 'google.com' ? docs : null),
					facebook: response.data.item.providerData.find(docs => docs.providerId === 'facebook.com' ? docs : null)
				})
			}else {
				if(response.data.error.code)
					return this.props.handleSnackbar({ type: 'auth', message: response.data.error.code, on: true })
				return this.props.handleSnackbar({ type: 'custom', message: response.data.error, on: true })
			}
		})
		.catch(error => {
			console.log(error, error.response.data.error)
			this.setState({ uid: null })
			if(error.response.data && error.response.data.error)
				return this.props.handleSnackbar({ type: 'auth', message: error.response.data.error.code, on: true })
			return this.props.handleSnackbar({ type: 'custom', message: '404', on: true })
		})
		.finally(() => this.state.onClickSnackLoadingClose())
	}
	handleOpen = uid => {
		if(uid !== this.state.uid)
			this.setState({ uid }, () => this.handleDataInfo())
		else if(uid === this.state.uid && this.state.user !== null)
			this.setState({ open: true })
		else if(uid === this.state.uid && this.state.user === null)
			this.handleDataInfo()
	}
	handlePage = page => event => {
		this.setState({ page })
	}
	handleClose = () => this.setState({ open: false })
	render(){
		const { page, showpassword } = this.state;
		return(
			<div className="root" id="view">
				<Dialog
					fullScreen
					open={this.state.open}
					onClose={this.handleClose}
					TransitionComponent={Transition}
				>
					<AppBar style={{position: 'relative'}}>
						<Toolbar>
							<IconButton
								color="inherit"
								onClick={this.handleClose}
							>
								<Icon>close</Icon>
							</IconButton>
							<Typography variant="title" color="inherit">
								Detalles del Usuario
							</Typography>
						</Toolbar>
					</AppBar>
					<div style={{ flexGrow: 1, padding: 16 }}>
						<Grid container>
							<Grid 
								item
								xs={12}
								sm={12}
								md={3}
								lg={3}
								xl={3}
							>
								<Paper style={{ display: 'flex', position: 'relative', flexDirection: 'column' }}>
									<div style={{ display: 'flex', justifyContent: 'center',paddingTop: 16 }}>
										<Avatar
											style={{ width: 160, height: 160, boxShadow: '0 0 1px black' }} 
											alt={this.state.user ? this.state.user.username : ''}
											src={this.state.user ? this.state.user.photo : ''}
										/>
									</div>
									<List
										component="nav"
										style={{ paddingBottom: 0 }}
										subheader={
											<div>
												<ListSubheader component="div" style={{ textAlign: 'center', lineHeight: '36px', color: 'black', paddingBottom: 12 }}>
													{/* { this.state.user ? this.state.user.username : '' }
													<br/> */}
													{ this.state.user ? this.state.user.fullname : '' }
													<br/>
													{
														this.state.user ?
															this.state.user.priv === 1 ? 'Administrador'
																:
															this.state.user.priv === 2 ? 'Supervisor'
																:
															this.state.user.priv === 3 ? 'Usuario'
																:
															null
															:
														''
													}
												</ListSubheader>
											</div>
										}
									>
										<Divider/>
										<ListItem button style={{textAlign: 'center'}} onClick={this.handlePage(0)} divider>
											<ListItemText primary="Información del Usuario"></ListItemText>
										</ListItem>
										<ListItem button style={{textAlign: 'center'}} onClick={this.handlePage(1)} divider>
											<ListItemText primary="Credenciales"></ListItemText>
										</ListItem>
										{
											this.state.user.priv !== 1 && this.state.user.priv !== 2
												?
											<ListItem button style={{textAlign: 'center'}} onClick={this.handlePage(2)} divider>
												<ListItemText primary="Idiomas"></ListItemText>
											</ListItem>
												:
											null
										}
										{
											this.state.user.priv !== 1 && this.state.user.priv !== 2
												?
											<ListItem button style={{textAlign: 'center'}} onClick={this.handlePage(3)} divider>
												<ListItemText primary="Herramientas de Comunicación"></ListItemText>
											</ListItem>
												:
											null
										}
									</List>
								</Paper>
							</Grid>
							<Grid 
								item
								xs={12}
								sm={12}
								md={9}
								lg={9}
								xl={9}
							>
								<div style={{padding: 15, paddingTop: 0}}>
									<SwipeableViews
										index={page}
										slideStyle={{
											alignItems: 'inherit',
											display: 'initial',
											padding: 5
										}}
									>
										<Paper style={{ padding: 15, display: 'flex', position: 'relative', flexDirection: 'column' }}>
											<Typography variant="title" gutterBottom>Información del Usuario</Typography>
											<Divider/>
											<Grid container spacing={8}>
												<Grid container>
													<Grid 
														item
														xl={6}
														lg={6}
														md={6}
														sm={12}
														xs={12}
														style={{
															paddingLeft: 10,
															paddingRight: 10
														}}
													>
														<TextField
															id="view/fullname"
															label="Nombre completo"
															className="input-view"
															margin="normal"
															fullWidth
															disabled
															type='text'
															value={this.state.user ? this.state.user.fullname : ''}
															inputProps={{
																pattern: '[A-Za-z ]{1,}',
																title: 'Solo se aceptan letras'
															}}
														/>
														<TextField
															id="new/gender"
															className="input-view"
															select
															label="Genero"
															disabled
															value={this.state.user ? this.state.user.gender : ''}
															fullWidth
															margin="normal"
														>
															<MenuItem key={'M'} value={'M'}>
																Masculino
															</MenuItem>
															<MenuItem key={'F'} value={'F'}>
																Femenino
															</MenuItem>
														</TextField>
														<TextField
															id="new/watch"
															className="input-view"
															select
															label="Tipo hora"
															disabled
															value={this.state.user ? this.state.user.watch : ''}
															fullWidth
															margin="normal"
														>
															<MenuItem key={1} value={1}>
																24Hrs
															</MenuItem>
															<MenuItem key={2} value={2}>
																12Hrs
															</MenuItem>
														</TextField>
														<TextField
															id="new/address"
															label="Direccion"
															disabled
															className="input-view"
															multiline
															fullWidth
															margin="normal"
															value={this.state.user ? this.state.user.address : ''}
														/>
													</Grid>	
													<Grid 
														item
														xl={6}
														lg={6}
														md={6}
														sm={12}
														xs={12}
														style={{
															paddingLeft: 10,
															paddingRight: 10
														}}
													>
														<TextField
															id="new/birthdate"
															label="Fecha de cumpleaños"
															margin="normal"
															fullWidth
															disabled
															className="input-view"
															type='date'
															InputLabelProps={{
																shrink: true,
															}}
															value={this.state.user ? this.state.user.birthdate : ''}
														/>
														<TextField
															id="new/phone"
															label="Telefono casa"
															className="input-view"
															placeholder="+581234567890"
															margin="normal"
															fullWidth
															disabled
															type='phone'
															inputProps={{
																pattern: '[+][0-9]{12}',
																title: 'Solo se aceptan letras'
															}}
															value={this.state.user ? this.state.user.phone : ''}
														/>
														<SelectTimeZone 
															id="new/timezone"
															label="Zona horaria"
															disabled
															className="input-view"
															value={this.state.user ? this.state.user.timezone : ''}
														/>
														{
															this.state.user.priv !== 1 && this.state.user.priv !== 2
																?
															<SelectCurrentMoney 
																id="new/money"
																className="input-view" 
																disabled
																label="Moneda"
																value={this.state.user ? this.state.user.money : '' }
																firebase={this.props.firebase}
															/>
																:
															null
														}
													</Grid>	
												</Grid>
												{
													this.state.user.priv !== 1 && this.state.user.priv !== 2
														?
													<Grid container>
														<Grid 
															item
															xl={6}
															lg={6}
															md={6}
															sm={12}
															xs={12}
															style={{
																paddingLeft: 10,
																paddingRight: 10
															}}
														>
															<TextField
																id="new/company"
																label="Compañia"
																margin="normal"
																disabled
																className="input-view"
																type="text"
																fullWidth
																value={this.state.user ? this.state.user.company : ''}
															/>
														</Grid>
														<Grid 
															item
															xl={6}
															lg={6}
															md={6}
															sm={12}
															xs={12}
															style={{
																paddingLeft: 10,
																paddingRight: 10
															}}
														>
															<TextField
																id="new/website"
																label="Sitio web"
																margin="normal"
																disabled
																className="input-view"
																type="url"
																fullWidth
																value={this.state.user ? this.state.user.website : ''}
															/>
														</Grid>
													</Grid>
														:
													null
												}
												{
													this.state.user.priv !== 1 && this.state.user.priv !== 2
														?
													<Grid container>
														<Grid 
															item
															xl={6}
															lg={6}
															md={6}
															sm={12}
															xs={12}
															style={{
																paddingLeft: 10,
																paddingRight: 10
															}}
														>
															<SelectLocalities 
																id="new/born"
																label="¿Donde naciste?"
																disabled
																className="input-view"
																value={this.state.user ? this.state.user.bornLocalities : ''}
															/>
														</Grid>
														<Grid 
															item
															xl={6}
															lg={6}
															md={6}
															sm={12}
															xs={12}
														>
															<SelectLocalities 
																id="new/live"
																label="¿Donde vives?"
																disabled
																className="input-view"
																value={this.state.user ? this.state.user.liveLocalities : ''}
															/>
														</Grid>
													</Grid>
														:
													null
												}
											</Grid>
										</Paper>
										<div style={{ 
											padding: 15,
    									display: 'flex',
											position: 'relative',
											flexDirection: 'column' 
										}}>
											<Paper style={{ padding: 15, display: 'flex', position: 'relative', flexDirection: 'column' }}>
												<Typography variant="title" gutterBottom>Credenciales</Typography>
												<Divider/>
												<Grid container spacing={8}>
													<Grid 
														item
														xs={12}
														sm={12}
														md={6}
														lg={6}
														xl={6}
													>
														<TextField
															id='view/username'
															label="Nombre de usuario"
															margin="normal"
															fullWidth
															disabled
															className="input-view"
															type='text'
															value={this.state.user ? this.state.user.username : ''}
														/>
														<TextField
															id="view/mail"
															label="Correo electronico"
															margin="normal"
															disabled
															className="input-view"
															fullWidth
															type='email'
															value={this.state.user ? this.state.user.email : ''}
														/>
													</Grid>
													<Grid 
														item
														xs={12}
														sm={12}
														md={6}
														lg={6}
														xl={6}
													>
														<TextField
															id="view/mobile"
															label="Telefono movil"
															placeholder="+581234567890"
															margin="normal"
															fullWidth
															disabled 
															className="input-view"
															type='phone'
															inputProps={{
																pattern: '[+][0-9]{12}',
																title: 'Solo se aceptan letras'
															}}
															value={this.state.user ? this.state.user.mobile : ''}
														/>
														<FormControl
															margin="normal"
															fullWidth
															disabled
															className="input-view"
														>
															<InputLabel htmlFor="password">Contraseña</InputLabel> 	
															<Input
																id="view/password"
																label="Contraseña"
																value={'12345678'}
																type={ showpassword ? 'text' : 'password' }
																// endAdornment={
																// 	<InputAdornment position="end">
																// 		<IconButton
																// 			aria-label="Toggle password visibility"
																// 			disabled
																// 			onClick={
																// 				event => this.setState((prevState, props) => {
																// 					return {showpassword: !prevState.showpassword}
																// 				}
																// 			)}
																// 			onMouseDown={event => event.preventDefault()}
																// 		>
																// 			<Icon>{ showpassword ? 'visibility' : 'visibility_off' }</Icon>
																// 		</IconButton>
																// 	</InputAdornment>
																// }
															/>
														</FormControl>
													</Grid>
												</Grid>
											</Paper>
											<Paper style={{ marginTop: 10, padding: 15, display: 'flex', position: 'relative', flexDirection: 'column' }}>
												<Typography variant="title" gutterBottom>Cuentas Vinculadas</Typography>
												<Divider/>
												<Grid container spacing={16} style={{ padding: '15px 0' }}>
													<Grid
														item
														xs={12}
														sm={12}
														md={6}
														lg={6}
														xl={6}
													>
														{
															this.state.google
																&&
															<Card style={{ maxWidth: 345 }}>
																<CardHeader
																	avatar={
																		<Avatar style={{ border: '1px solid #eee', backgroundColor: 'transparent' }}>
																			<Google className="google"/>		
																		</Avatar>
																	}
																	title="Google Proveedor"
																/>
																<CardMedia
																	style={{ height: 0, paddingTop: '56.25%' }}
																	image={this.state.google.photoURL}
																	title={this.state.google.displayName}
																/>
																<CardContent>
																	<Typography gutterBottom variant="headline" component="h4">
																		{this.state.google.displayName}
																	</Typography>
																	<Typography component="p">{`Correo Electronico: ${this.state.google.email}`}</Typography>
																	<Typography component="p">{`ID Proveedor: ${this.state.google.uid}`}</Typography>
																</CardContent>
															</Card>
														}
														{
															!this.state.google
																&&
															<Button
																variant="raised"
																fullWidth
																style={{ backgroundColor: '#fff' }}
															>
																<Google size={26} className="google icon-button"/>
																Google
															</Button>
														}
													</Grid>
													<Grid
														item
														xs={12}
														sm={12}
														md={6}
														lg={6}
														xl={6}
													>
														{
															this.state.facebook
																&&
															<Card style={{ maxWidth: 345 }}>
																<CardHeader
																	avatar={
																		<Avatar style={{ border: '1px solid #eee', backgroundColor: 'transparent !important' }}>
																			<Facebook className="facebook"/>		
																		</Avatar>
																	}
																	title="Facebook Proveedor"
																/>
																<CardMedia
																	style={{ height: 0, paddingTop: '56.25%' }}
																	image={this.state.facebook.photoURL}
																	title={this.state.facebook.displayName}
																/>
																<CardContent>
																	<Typography gutterBottom variant="headline" component="h4">
																		{this.state.facebook.displayName}
																	</Typography>
																	<Typography component="p">{`Correo Electronico: ${this.state.facebook.email}`}</Typography>
																	<Typography component="p">{`ID Proveedor: ${this.state.facebook.uid}`}</Typography>
																</CardContent>
															</Card>
														}
														{
															!this.state.facebook
																&&
															<Button
																variant="raised"
																fullWidth
																style={{ backgroundColor: '#fff' }}
															>
																<Facebook size={26} className="facebook icon-button"/>
																Facebook
															</Button>
														}
													</Grid>
												</Grid>
											</Paper>
										</div>
										<Paper style={{ padding: 15, display: 'flex', position: 'relative', flexDirection: 'column' }}>
											<Typography variant="title" gutterBottom>Idiomas</Typography>
											<Divider/>
											{
												this.state.user.priv !== 1 && this.state.user.priv !== 2
													?
												<Grid container spacing={8}>
													<Grid 
														item
														xs={12}
													>
														<Typography style={{ margin: '10px 0px' }}>Idiomas Nativo</Typography>
														<SelectLanguageNative 
															id="view/languageNative"
															disabled
															className="input-view"
															value={this.state.user ? this.state.user.languageNative : ''}
															firebase={this.props.firebase}
														/>
														<Typography style={{ margin: '10px 0px' }}>Idiomas Estudiandos y Aprendidos</Typography>
														<SelectLanguage 
															id="view/language"
															disabled
															className="input-view"
															value={this.state.user ? this.state.user.language : ''}
															firebase={this.props.firebase}
														/>
													</Grid>
												</Grid>
													:
												null
											}
										</Paper>
										<Paper style={{ padding: 15, display: 'flex', position: 'relative', flexDirection: 'column' }}>
											<Typography variant="title" gutterBottom>Herramientas de Comunicación</Typography>
											<Divider/>
											{
												this.state.user.priv !== 1 && this.state.user.priv !== 2
													?
												<Grid container spacing={8}>
													<Grid 
														item
														xs={12}
														sm={12}
														md={6}
														lg={6}
														xl={6}
													>
													<TextField
														id="modify/tool-skype"
														label="Skype"
														margin="normal"
														fullWidth
														disabled
														className="input-view"
														type="email"
														value={this.state.user ? this.state.user.skype : ''}
														InputProps={{
															startAdornment: (
																<InputAdornment position="start">
																	<FontAwesome.FaSkype/>
																</InputAdornment>
															)
														}}
													/>
													<TextField
														id="modify/tool-facetime"
														label="FaceTime"
														margin="normal"
														fullWidth
														disabled
														className="input-view"
														type="email"
														value={this.state.user ? this.state.user.facetime : ''}
														InputProps={{
															startAdornment: (
																<InputAdornment position="start">
																	<Icon>videocam</Icon>
																</InputAdornment>
															)
														}}
													/>
													<TextField
														id="modify/tool-qqhangouts"
														label="Google Hangouts"
														margin="normal"
														fullWidth
														disabled
														className="input-view"
														type="email"
														value={this.state.user ? this.state.user.gHangouts : ''}
														InputProps={{
															startAdornment: (
																<InputAdornment position="start">
																	<Icon>format_quote</Icon>
																</InputAdornment>
															)
														}}
													/>
													</Grid>
													<Grid 
														item
														xs={12}
														sm={12}
														md={6}
														lg={6}
														xl={6}
													>
													<TextField
														id="modify/tool-wechat"
														label="WeChat"
														margin="normal"
														fullWidth
														disabled
														className="input-view"
														type="email"
														value={this.state.user ? this.state.user.wechat : ''}
														InputProps={{
															startAdornment: (
																<InputAdornment position="start">
																	<FontAwesome.FaWechat/>
																</InputAdornment>
															)
														}}
													/>
													<TextField
														id="modify/tool-qq"
														label="Tencent QQ"
														margin="normal"
														fullWidth
														disabled
														className="input-view"
														type="email"
														value={this.state.user ? this.state.user.qq : ''}
														InputProps={{
															startAdornment: (
																<InputAdornment position="start">
																	<FontAwesome.FaQq/>
																</InputAdornment>
															)
														}}
													/>
													</Grid>
												</Grid>
													:
												null
											}
										</Paper>
									</SwipeableViews>
								</div>
							</Grid>
						</Grid>
					</div>
				</Dialog>
				<SnackbarLoading 
					handleClick={event => this.setState({ onClickSnackLoadingOpen: event })} 
					handleClose={event => this.setState({ onClickSnackLoadingClose: event })} 
				/>
			</div>
		)
	}
}

export default View;