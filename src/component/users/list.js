import React, { Component } from 'react';
import { Paper, Tooltip } from '@material-ui/core';

import { TableCustom } from '../../utils';

const headCollection = [
	{
		title: '#',
		tooltip: false
	},
	{
		title: 'Imagen',
		tooltip: true,
		position: 'bottom-end',
		delay: 300,
		ref: 'photo',
		disabled: true
	},
	{
		title: 'Nombre de Usuario',
		tooltip: true,
		position: 'bottom-end',
		delay: 300,
		ref: 'username'
	},
	{
		title: 'Nombre Completo',
		tooltip: true,
		position: 'bottom-end',
		delay: 300,
		ref: 'fullname'
	},
	{
		title: 'Correo Electronico',
		tooltip: true,
		position: 'bottom-end',
		delay: 300,
		ref: 'email'
	},
	{
		title: 'Privilegios',
		tooltip: true,
		position: 'bottom-end',
		delay: 300,
		ref: 'priv'
	},
]

class List extends Component {
	state = {
		users: [],
		orderBy: 'username',
		order: 'asc',
		limit: 5,
		page: 0,
		count: 0,
		startAfter: null,
		endBefore: null
	}
	constructor(props){
		super()
		this.firestore = props.firebase.firestore().collection('users');
	}
	componentWillMount(){
		this.makeCount();
	}
	makeCount = () => {
		this.firestore
			.onSnapshot(snapshot => {
				this.setState({ 
					count: snapshot.size
				}, () => this.makeList())
			}, error => console.log(error))
	}
	makeList = () => {
		const { orderBy, order, limit, startAfter, endBefore } = this.state;
		let query;

		if(startAfter){
			query = this.firestore.orderBy(orderBy, order).limit(limit).startAfter(startAfter);
		}
		if(!query){
			query = this.firestore.orderBy(orderBy, order).limit(limit);
		}

		this.observer =	query.onSnapshot(snapshot => {
			this.setState({ 
				users: snapshot.docs })
		}, error => console.log(error))
	}
	sortOrder = event => {
		this.observer();
		if(event.ref === this.state.orderBy){
			this.setState(prevState => {
				
				prevState.order === 'asc'
					?
				prevState.order = 'desc'
					:
				prevState.order = 'asc'

				return prevState;
			}, () => this.makeList());
		}else 
			this.setState(prevState => {
				prevState.order = 'desc';
				prevState.orderBy = event.ref;
				return prevState;
			}, () => this.makeList())
	}
	onChangePage = (event, page) => {
		this.observer();
		this.setState(prevState => {
			if(page > prevState.page){
				prevState.previous = prevState.startAfter; 
				prevState.startAfter = this.state.users[ this.state.users.length - 1 ];
			}else if(page < prevState.page)
				prevState.startAfter = prevState.page === 1 ? false : prevState.previous;
			
			prevState.page = page;
			return prevState;
		}, () => {
			this.makeList();
		})
	}
	onChangeRowsPerPage = event => {
		this.observer();
		this.setState({
			limit: event.target.value
		}, () => this.makeList())
	}
	render(){
		const { users, orderBy, order, limit, page } = this.state;
		const { 
			handleChange, 
			id, 
			value 
		} = this.props;
		return (
			<Paper style={{
				width: '100%',
				overflowX: 'auto',
			}}>
				<div style={{ display: value === id ? 'block' : 'none' }}>
					<TableCustom
						headCollection={headCollection}
						bodyCollection={users}
						handleEdit={handleChange}
						handleDelete={this.props.handleDelete}
						handleView={this.props.handleView}
						handleClickHead={this.sortOrder}
						orderBy={orderBy}
						order={order === 'desc' ? 'asc' : 'desc'}
						count={this.state.count}
						page={page}
						rowsPerPage={limit}
						onChangePage={this.onChangePage}
						onChangeRowsPerPage={this.onChangeRowsPerPage}
						footer=""
						style={{
							minWidth: 700,
						}}
						template="users"
					/>
				</div>
			</Paper>
		)
	}
}

export default List;