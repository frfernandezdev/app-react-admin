import React, { Component } from 'react';
import axios from 'axios';
import { Paper, Tooltip } from '@material-ui/core';

import { TableCustom } from '../../utils';

const headCollection = [
	{
		title: '#',
		tooltip: false
	},
	{
		title: 'Imagen',
		tooltip: true,
		position: 'bottom-end',
		delay: 300,
		ref: 'photo',
		disabled: true
	},
	{
		title: 'Nombre de Usuario',
		tooltip: true,
		position: 'bottom-end',
		delay: 300,
		ref: 'username'
	},
	{
		title: 'Nombre Completo',
		tooltip: true,
		position: 'bottom-end',
		delay: 300,
		ref: 'fullname'
	},
	{
		title: 'Correo Electronico',
		tooltip: true,
		position: 'bottom-end',
		delay: 300,
		ref: 'email'
	},
	{
		title: 'Fecha',
		tooltip: true,
		position: 'bottom-end',
		delay: 300,
		ref: 'application_date'
	},
	{
		title: 'Estado',
		tooltip: true,
		position: 'bottom-end',
		delay: 300,
		ref: 'status',
		disabled: true
	}
]

class List extends Component {
	state = {
		applications: [],
		orderBy: {
			name: 'username',
			value: 'asc'
		},
		limit: 5,
		page: 0,
		count: 0,
		startAfter: null,
		endBefore: null
	}
	constructor(props){
		super(props)
		this.firestore 		= props.firebase.firestore();
		this.props.handleReset(this.makeList);
	}
	componentWillMount(){
		this.makeList();
	}
	makeList = () => {
		const { orderBy, limit, startAfter } = this.state;

		let params = {
			token: this.props.user.auth.token
		}

		if(startAfter)
			params.start   = startAfter;
		if(orderBy)
			params.orderBy = orderBy;
		if(limit)
			params.limit   = limit;
		// http://localhost:5000/admin-speakenglishsite/us-central1/api/applications
		// https://us-central1-admin-speakenglishsite.cloudfunctions.net/api/applications
		axios.get(`http://localhost:5000/admin-speakenglishsite/us-central1/api/applications`, { params })
			.then(response => {
				console.log(response)
				if(response.data.success){
					this.setState({
						applications: response.data.items,
						count: response.data.count
					})
				}else {
					if(response.data.error)
						return this.props.handleSnackbar({ type: 'auth', message: response.data.error.code, on: true })
					return this.props.handleSnackbar({ type: 'custom', message: response.data.error, on: true })
				} 
			})
			.catch(error => {
				console.log(error.response)
				if(error.response.data && error.response.data.error)
					return this.props.handleSnackbar({ type: 'auth', message: error.response.data.error.code, on: true })
				return this.props.handleSnackbar({ type: 'custom', message: '404', on: true })
			})
	}
	sortOrder = event => {
		if(event.ref === this.state.orderBy){
			this.setState(prevState => {
				
				prevState.orderBy.value === 'asc'
					?
				prevState.orderBy.value = 'desc'
					:
				prevState.orderBy.value = 'asc'

				return prevState;
			}, () => this.makeList());
		}else
			this.setState(prevState => {
				prevState.orderBy.value = 'desc';
				prevState.orderBy.name = event.ref;
				return prevState;
			}, () => this.makeList())
	}
	onChangePage = (event, page) => {
		this.setState(prevState => {
			if(page > prevState.page){
				prevState.previous = prevState.startAfter; 
				prevState.startAfter = this.state.users[ this.state.users.length - 1 ];
			}else if(page < prevState.page)
				prevState.startAfter = prevState.page === 1 ? false : prevState.previous;
			
			prevState.page = page;
			return prevState;
		}, () => this.makeList())
	}
	onChangeRowsPerPage = event => {
		this.setState({
			limit: event.target.value
		}, () => this.makeList())
	}
	onSubmitAccept = event => {
		let id = event;
		this.props.onClickSnackLoadingOpen();
		axios.put(`http://localhost:5000/admin-speakenglishsite/us-central1/api/applications/accept/${id}`, {
			token: this.props.user.auth.token
		})
		.then(response => {
			if(response.data.success){
				this.makeList()
				this.props.handleSnackbar({type: 'custom', message: response.data.mes})
			}else {
				if(response.data.error.code)
					return this.props.handleSnackbar({ type: 'auth', message: response.data.error.code, on: true })
				return this.props.handleSnackbar({ type: 'custom', message: response.data.error, on: true })
			}
		})
		.catch(error => {
			console.log(error)
			return this.props.handleSnackbar({ type: 'custom', message: '404', on: true })
		})
		.finally(this.props.onClickSnackLoadingClose)
	}
	onSubmitCancel = event => {
		let id = event;
		this.props.onClickSnackLoadingOpen();
		axios.put(`http://localhost:5000/admin-speakenglishsite/us-central1/api/applications/cancel/${id}`, {
			token: this.props.user.auth.token
		})
		.then(response => {
			if(response.data.success){
				this.makeList()
				this.props.handleSnackbar({type: 'custom', message: response.data.mes})
			}else {
				if(response.data.error.code)
					return this.props.handleSnackbar({ type: 'auth', message: response.data.error.code, on: true })
				return this.props.handleSnackbar({ type: 'custom', message: response.data.error, on: true })
			}
		})
		.catch(error => {
			console.log(error)
			return this.props.handleSnackbar({ type: 'custom', message: '404', on: true })
		})
		.finally(this.props.onClickSnackLoadingClose)
	}
	render(){
		const { applications, orderBy, order, limit, page, count } = this.state;
		const { 
			handleChange,
			id, 
			value,
			handleView 
		} = this.props;
		return (
			<Paper style={{
				width: '100%',
				overflowX: 'auto',
			}}>
				<div style={{ display: value === id ? 'block' : 'none' }}>
					<TableCustom
						headCollection={headCollection}
						bodyCollection={applications}
						handleEdit={this.onSubmitAccept}
						handleDelete={this.onSubmitCancel}
						handleView={handleView}
						handleClickHead={this.sortOrder}
						orderBy={orderBy}
						order={order === 'desc' ? 'asc' : 'desc'}
						count={count}
						page={page}
						rowsPerPage={limit}
						onChangePage={this.onChangePage}
						onChangeRowsPerPage={this.onChangeRowsPerPage}
						footer=""
						style={{
							minWidth: 700,
						}}
						template="applications"
						options="options"
					/>
				</div>
			</Paper>
		)
	}
}

export default List;