import React, { Component } from 'react';
import axios from 'axios';
import {
	MenuItem,
	TextField,
	Typography,
	Paper,
	Grid, 
	Icon,
	IconButton, 
	FormControl,
	FormLabel,
	FormControlLabel,
	FormGroup,
	Input,
	InputLabel,
	InputAdornment,
	Button,
	Checkbox,
} from '@material-ui/core';
import * as FontAwesome from 'react-icons/lib/fa';

import {
  SelectLocalities,
	SelectTimeZone,
	SelectCurrentMoney,
	SelectLanguageToLearn,
	SelectLanguageNative,
	UploadButton,
	DateNow,
	SummaryCv,
	Calendar
} from '../../utils';

class New extends Component {
	state = {
		open: false,
		form: {
			skill: {
				test: {}
			},
			materialLearn: {}
		},
		showpassword: false,
		users: [],
		disabledLanguage: true
	}
	constructor(props){
		super(props);

		this.firestore = props.firebase.firestore().collection('postulaciones');
		this.textFieldUsers = props.firebase.firestore().collection('users');
		this.storage   = props.firebase.storage();
		this.onSubmit  = this.onSubmit.bind(this)
		this.handleChange  = this.handleChange.bind(this)
		this.handleOnClick = this.handleOnClick.bind(this)
		this.props.handleOnClick(this.handleOnClick)
	}
	componentWillMount(){
		this.textFieldUsers
			.orderBy('fullname')
			.onSnapshot(snapshot => {
        this.setState({ users: snapshot.docs })
			}, error => {
				console.log(error)
      })
	}
	AutoCompleteLanguageToLearn = (name, event) => {
		let data = this.state.users.find(
			docs => 
				docs.id === event.target.value 
					? 
				docs 
					: 
				null
		);
		var language = data.data().language.map(docs => {
			return {
				language: docs.language,
				level: docs.level
			}
		})
		return this.setState({ 
			form: {
				...this.state.form,
				[name]: event.target.value,
				languageToLearn: language,
			},
			disabledLanguage: false
		 }, () => console.log(this.state.form.languageToLearn))
	}
	handleChange = name => event => {
		if(name === 'user')
			return this.AutoCompleteLanguageToLearn(name ,event);
		
		return this.setState({
				form: {
					...this.state.form,
					[name]: event.target.value
				}
		})
	}
	handleChangeCheckbox = name => (event, checked) => {
		this.setState({
			form: {
				...this.state.form,
				skill: {
					...this.state.form.skill,
					[name]: checked
				}
			}
		})
	}
	handleChangeCheckboxTest = name => (event, checked) => {
		this.setState({
			form: {
				...this.state.form,
				skill: {
					...this.state.form.skill,
					test: {
						...this.state.form.skill.test,
						[name]: checked
					}
				}
			}
		})
	}
	handleChangeCheckboxLearn = name => (event, checked) => {
		this.setState({
			form: {
				...this.state.form,
				materialLearn: {
					...this.state.form.materialLearn,
					[name]: checked
				}
			}
		})
	}
	onSubmit = event => {
		event.preventDefault();
		this.props.handleOnLoading('show')

		let formData = new FormData();

		let object = Object.assign({}, this.state.form);

		delete object.summaryCV;

		if(this.state.form.summaryCV && this.state.form.summaryCV.student)
			object.student = this.state.form.summaryCV.student;
		if(this.state.form.summaryCV && this.state.form.summaryCV.professional_expirence)
			object.professional_expirence = this.state.form.summaryCV.professional_expirence;
		

		formData.append('data', JSON.stringify(object))
		formData.append('fileDni', this.state.form.fileDni);

		if(object.student && object.student.length)
			object.student.forEach(docs => {
				formData.append('fileStudents', docs.file)
			})
		if(object.professional_expirence && object.professional_expirence.length)
			object.professional_expirence.forEach(docs => {
				formData.append('filePE', docs.file)
			})

		// http://localhost:5000/admin-speakenglishsite/us-central1/api/applications
		// https://us-central1-admin-speakenglishsite.cloudfunctions.net/api/applications
		axios.post(`http://localhost:5000/admin-speakenglishsite/us-central1/api/applications`, 
			formData,
			{
				params: {
					token: this.props.user.auth.token
				}
			} 
		)
		.then(response => {
			this.props.handleOnLoading('hide')
			if(response.data.success === true){
				this.props.handleSnackbar({type: 'custom', message: response.data.mes})
				setTimeout(() => {
					this.setState({ 
						form: {
							skill: {
								test: {}
							},
							materialLearn: {}
						}
					})
					this.props.handleOnLoading('hide')
				}, 2500)
				this.props.handleOnLoading('done')
			}else {
				this.props.handleOnLoading('hide')
				if(response.data.error.code)
					return this.props.handleSnackbar({ type: 'auth', message: response.data.error.code, on: true })
				return this.props.handleSnackbar({ type: 'custom', message: response.data.error, on: true })
			}
		})
		.catch(error => {
			console.log(error)
			this.props.handleOnLoading('hide')
			if(error.response.data && error.response.data.error)
				return this.props.handleSnackbar({ type: 'auth', message: error.response.data.error.code, on: true })
			return this.props.handleSnackbar({ type: 'custom', message: '404', on: true })
		})
	}
	handleOnClick = () => {
		document.getElementById('new/submit').click()
	}
	render() {
			const { id, value } = this.props;
			const { 
				users,
				form,
				open,
				disabledLanguage
			} = this.state;
			return (
				<Paper style={{ padding: 15 }}>
						<form 
								id="new"
								onSubmit={this.onSubmit} 
								style={{ display: value === id ? 'block' : 'none' }}
						>
							<Typography variant="title" gutterBottom>Postulaciones</Typography>
							<Grid container>
								<Grid
									item 
									xs={12}
									sm={12}
									md={12}
									lg={12}
									xl={12}
									style={{
										paddingLeft: 10,
										paddingRight: 10
									}}
								>
									<Typography variant="body2" style={{marginTop: 10}}>Datos de la postulacion</Typography>
								</Grid>
								<Grid
									item 
									xs={12} 
									sm={12} 
									md={6} 
									lg={6} 
									xl={6} 
									style={{
										paddingLeft: 10,
										paddingRight: 10
									}}
								>
									<TextField
										id="new/user"
										label="Usuarios"
										select
										fullWidth
										margin="normal"
										InputProps={{
											startAdornment: 
												<InputAdornment position="start">
													<Icon>account_circle</Icon>
												</InputAdornment>,
										}}
										value={form.user ? form.user : ''}
										onChange={this.handleChange('user')}
									>
										{users.map((docs, i) => {
											let collection = docs.data();
											return (
												<MenuItem key={i} value={docs.id}>
													{collection.fullname}
												</MenuItem>
											);
										})}
									</TextField>
								</Grid>
								<Grid
									item 
									xs={12} 
									sm={12} 
									md={6} 
									lg={6} 
									xl={6} 
									style={{
										paddingLeft: 10,
										paddingRight: 10
									}}
								>
									<TextField
										id="new/video"
										label="Link Video"
										margin="normal"
										fullWidth
										type="url"
										InputProps={{
											startAdornment: 
												<InputAdornment>
													<Icon>video_library</Icon>
												</InputAdornment>	
										}}
										value={form.video ? form.video : ''}
										onChange={this.handleChange('video')}
									/>
								</Grid>
								<Grid 
									item 
									xs={12} 
									sm={12} 
									md={6} 
									lg={6} 
									xl={6} 
									style={{
										paddingLeft: 10,
										paddingRight: 10
									}}
								>
									
									<TextField
										id="new/about_me"
										label="Sobre mi"
										multiline
										fullWidth
										margin="normal"
										value={form.aboutMe ? form.aboutMe : ''}
										onChange={this.handleChange('aboutMe')}
									/>
								</Grid>
								<Grid
									item 
									xs={12} 
									sm={12} 
									md={6} 
									lg={6} 
									xl={6} 
									style={{
										paddingLeft: 10,
										paddingRight: 10
									}}
								>
									<TextField
										id="new/methodology"
										label="Metodologia"
										multiline
										margin="normal"
										fullWidth
										value={form.methodology ? form.methodology : ''}
										onChange={this.handleChange('methodology')}
									/>
								</Grid>
								<Grid 
									item
									xs={12}
									sm={12}
									md={12}
									lg={12}
									xl={12}
								>
									<UploadButton 
										value={form.fileDni ? form.fileDni : ''} 
										onChange={this.handleChange('fileDni')}
										labelButton="Subir imagen del DNI" 
										labelButtonSuccess="Ver DNI" 
										labelDialog="Subir image del DNI"
										style={
											{
												textAlign: 'center',
												paddingTop: 50,
												paddingBottom: 20
											}
										}
									/>
								</Grid>
								<Grid 
									item 
									xs={12} 
									sm={12} 
									md={12} 
									lg={12} 
									xl={12} 
									style={{
										paddingLeft: 10,
										paddingRight: 10
									}}
								>
									<Typography variant="body2" style={{marginTop: 10}}>Idiomas a Enseñar</Typography>
								</Grid>
								<Grid
									item 
									xs={12} 
									sm={12} 
									md={12} 
									lg={12} 
									xl={12} 
									style={{
										paddingLeft: 10,
										paddingRight: 10
									}}
								>
									<SelectLanguageToLearn 
										id="new/languageToLearn"
										value={
											form.languageToLearn
												?
											form.languageToLearn
												:
											''											
										}
										disabled={disabledLanguage}
										delete={form.languageToLearn ? false : true}
										onChange={this.handleChange('languageToLearn')}
										firebase={this.props.firebase}
									/>
								</Grid>
								<Grid 
									item 
									xs={12} 
									sm={12} 
									md={12} 
									lg={12} 
									xl={12} 
									style={{
										paddingLeft: 10,
										paddingRight: 10
									}}
								>
									<Typography variant="body2" style={{marginTop: 10}}>Educacion y Experiencia Laboral</Typography>
								</Grid>
								<Grid
									item 
									xs={12} 
									sm={12} 
									md={12} 
									lg={12} 
									xl={12} 
									style={{
										paddingLeft: 10,
										paddingRight: 10
									}}
								>
									<SummaryCv
										delete={form.summaryCV ? false : true}
										onChange={this.handleChange('summaryCV')}
									/>
								</Grid>
								<Grid
									item
									xs={12}
									sm={12}
									md={12}
									lg={12}
									xl={12}
								>
									<Calendar
										seletedAll={true}
										onChange={this.handleChange('schedule')}
									/>
								</Grid>
								<Grid 
									item 
									xs={12} 
									sm={12} 
									md={12} 
									lg={12} 
									xl={12} 
									style={{
										marginTop: 10,
										paddingLeft: 10,
										paddingRight: 10
									}}
								>
									<Typography variant="body2" style={{marginTop: 10}}>Habilidades de enseñanza</Typography>
								</Grid>
								<Grid
									item
									xs={12}
									sm={12}
									md={6}
									lg={6}
									xl={6}
									style={{
										paddingLeft: 10,
										paddingRight: 10
									}}
								>
									<FormControl 
										margin="normal"
										fullWidth
										component="fieldset"
									>
										<FormGroup>
											<FormControlLabel
												control={
													<Checkbox
														checked={form.skill.junnior ? form.skill.junnior : false}
														onChange={this.handleChangeCheckbox('junnior')}
													/>	
												}
												label="Principiantes"
											/>
										</FormGroup>
									</FormControl>
									<FormControl 
										margin="normal"
										fullWidth
										component="fieldset"
									>
										<FormGroup>
											<FormControlLabel
												control={
													<Checkbox
														checked={form.skill.business ? form.skill.business : false}
														onChange={this.handleChangeCheckbox('business')}
													/>	
												}
												label="Negocios"
											/>
										</FormGroup>
									</FormControl>
								</Grid>
								<Grid
									item
									xs={12}
									sm={12}
									md={6}
									lg={6}
									xl={6}
									style={{
										paddingLeft: 10,
										paddingRight: 10
									}}
								>
									<FormControl 
										margin="normal"
										fullWidth
										component="fieldset"
									>
										<FormGroup>
											<FormControlLabel
												control={
													<Checkbox
														checked={form.skill.kids ? form.skill.kids : false}
														onChange={this.handleChangeCheckbox('kids')}
													/>	
												}
												label="Niños"
											/>
										</FormGroup>
									</FormControl>
									<FormControl 
										margin="normal"
										fullWidth
										component="fieldset"
									>
										<FormGroup>
											<FormControlLabel
												control={
													<Checkbox
														checked={form.skill.teen ? form.skill.teen : false}
														onChange={this.handleChangeCheckbox('teen')}
													/>	
												}
												label="Jovenes"
											/>
										</FormGroup>
									</FormControl>
								</Grid>
								<Grid 
									item 
									xs={12} 
									sm={12} 
									md={12} 
									lg={12} 
									xl={12} 
									style={{
										marginTop: 10,
										paddingLeft: 10,
										paddingRight: 10
									}}
								>
									<Typography variant="body2" style={{marginTop: 10}}>Preparacion para Test</Typography>
								</Grid>
								<Grid
									item
									xs={12}
									sm={12}
									md={3}
									lg={3}
									xl={3}
									style={{
										paddingLeft: 10,
										paddingRight: 10
									}}
								>
									<FormControl 
										margin="normal"
										fullWidth
										component="fieldset"
									>
										<FormGroup>
											<FormControlLabel
												control={
													<Checkbox
														checked={form.skill.test.IELTS ? form.skill.test.IELTS : false}
														onChange={this.handleChangeCheckboxTest('IELTS')}
													/>	
												}
												label="IELTS"
											/>
										</FormGroup>
									</FormControl>
									<FormControl 
										margin="normal"
										fullWidth
										component="fieldset"
									>
										<FormGroup>
											<FormControlLabel
												control={
													<Checkbox
														checked={form.skill.test.TOEIC ? form.skill.test.TOEIC : false}
														onChange={this.handleChangeCheckboxTest('TOEIC')}
													/>	
												}
												label="TOEIC"
											/>
										</FormGroup>
									</FormControl>
									<FormControl 
										margin="normal"
										fullWidth
										component="fieldset"
									>
										<FormGroup>
											<FormControlLabel
												control={
													<Checkbox
														checked={form.skill.test.CELU ? form.skill.test.CELU : false}
														onChange={this.handleChangeCheckboxTest('CELU')}
													/>	
												}
												label="CELU"
											/>
										</FormGroup>
									</FormControl>
								</Grid>
								<Grid
									item
									xs={12}
									sm={12}
									md={3}
									lg={3}
									xl={3}
									style={{
										paddingLeft: 10,
										paddingRight: 10
									}}
								>
									<FormControl 
										margin="normal"
										fullWidth
										component="fieldset"
									>
										<FormGroup>
											<FormControlLabel
												control={
													<Checkbox
														checked={form.skill.test.BEC ? form.skill.test.BEC : false}
														onChange={this.handleChangeCheckboxTest('BEC')}
													/>	
												}
												label="BEC"
											/>
										</FormGroup>
									</FormControl>
									<FormControl 
										margin="normal"
										fullWidth
										component="fieldset"
									>
										<FormGroup>
											<FormControlLabel
												control={
													<Checkbox
														checked={form.skill.test.CAE ? form.skill.test.CAE : false}
														onChange={this.handleChangeCheckboxTest('CAE')}
													/>	
												}
												label="CAE"
											/>
										</FormGroup>
									</FormControl>
									<FormControl 
										margin="normal"
										fullWidth
										component="fieldset"
									>
										<FormGroup>
											<FormControlLabel
												control={
													<Checkbox
														checked={form.skill.test.KET ? form.skill.test.KET : false}
														onChange={this.handleChangeCheckboxTest('KET')}
													/>	
												}
												label="KET"
											/>
										</FormGroup>
									</FormControl>
								</Grid>
								<Grid
									item
									xs={12}
									sm={12}
									md={3}
									lg={3}
									xl={3}
									style={{
										paddingLeft: 10,
										paddingRight: 10
									}}
								>
									<FormControl 
										margin="normal"
										fullWidth
										component="fieldset"
									>
										<FormGroup>
											<FormControlLabel
												control={
													<Checkbox
														checked={form.skill.test.TOEFL ? form.skill.test.TOEFL : false}
														onChange={this.handleChangeCheckboxTest('TOEFL')}
													/>	
												}
												label="TOEFL"
											/>
										</FormGroup>
									</FormControl>
									<FormControl 
										margin="normal"
										fullWidth
										component="fieldset"
									>
										<FormGroup>
											<FormControlLabel
												control={
													<Checkbox
														checked={form.skill.test.DELE ? form.skill.test.DELE : false}
														onChange={this.handleChangeCheckboxTest('DELE')}
													/>	
												}
												label="DELE"
											/>
										</FormGroup>
									</FormControl>
									<FormControl 
										margin="normal"
										fullWidth
										component="fieldset"
									>
										<FormGroup>
											<FormControlLabel
												control={
													<Checkbox
														checked={form.skill.test.FCE ? form.skill.test.FCE : false}
														onChange={this.handleChangeCheckboxTest('FCE')}
													/>	
												}
												label="FCE"
											/>
										</FormGroup>
									</FormControl>
								</Grid>
								<Grid
									item
									xs={12}
									sm={12}
									md={3}
									lg={3}
									xl={3}
									style={{
										paddingLeft: 10,
										paddingRight: 10
									}}
								>
									<FormControl 
										margin="normal"
										fullWidth
										component="fieldset"
									>
										<FormGroup>
											<FormControlLabel
												control={
													<Checkbox
														checked={form.skill.test.PET ? form.skill.test.PET : false}
														onChange={this.handleChangeCheckboxTest('PET')}
													/>	
												}
												label="PET"
											/>
										</FormGroup>
									</FormControl>
									<FormControl 
										margin="normal"
										fullWidth
										component="fieldset"
									>
										<FormGroup>
											<FormControlLabel
												control={
													<Checkbox
														checked={form.skill.test.CPE ? form.skill.test.CPE : false}
														onChange={this.handleChangeCheckboxTest('CPE')}
													/>	
												}
												label="CPE"
											/>
										</FormGroup>
									</FormControl>
									<FormControl 
										margin="normal"
										fullWidth
										component="fieldset"
									>
										<FormGroup>
											<FormControlLabel
												control={
													<Checkbox
														checked={form.skill.test.ILEC ? form.skill.test.ILEC : false}
														onChange={this.handleChangeCheckboxTest('ILEC')}
													/>	
												}
												label="ILEC"
											/>
										</FormGroup>
									</FormControl>
								</Grid>
								<Grid 
									item 
									xs={12} 
									sm={12} 
									md={12} 
									lg={12} 
									xl={12} 
									style={{
										marginTop: 10,
										paddingLeft: 10,
										paddingRight: 10
									}}
								>
									<Typography variant="body2" style={{marginTop: 10}}>Materiales de enseñanza</Typography>
								</Grid>
								<Grid
									item
									xs={12}
									sm={12}
									md={3}
									lg={3}
									xl={3}
									style={{
										paddingLeft: 10,
										paddingRight: 10
									}}
								>
									<FormControl 
										margin="normal"
										fullWidth
										component="fieldset"
									>
										<FormGroup>
											<FormControlLabel
												control={
													<Checkbox
														checked={form.materialLearn.file_pdf ? form.materialLearn.file_pdf : false}
														onChange={this.handleChangeCheckboxLearn('file_pdf')}
													/>	
												}
												label="Archivos PDF"
											/>
										</FormGroup>
									</FormControl>
									<FormControl 
										margin="normal"
										fullWidth
										component="fieldset"
									>
										<FormGroup>
											<FormControlLabel
												control={
													<Checkbox
														checked={form.materialLearn.article_news ? form.materialLearn.article_news : false}
														onChange={this.handleChangeCheckboxLearn('article_news')}
													/>	
												}
												label="Articulos de noticias"
											/>
										</FormGroup>
									</FormControl>
									<FormControl 
										margin="normal"
										fullWidth
										component="fieldset"
									>
										<FormGroup>
											<FormControlLabel
												control={
													<Checkbox
														checked={form.materialLearn.task ? form.materialLearn.task : false}
														onChange={this.handleChangeCheckboxLearn('task')}
													/>	
												}
												label="Tareas"
											/>
										</FormGroup>
									</FormControl>
								</Grid>
								<Grid
									item
									xs={12}
									sm={12}
									md={3}
									lg={3}
									xl={3}
									style={{
										paddingLeft: 10,
										paddingRight: 10
									}}
								>
									<FormControl 
										margin="normal"
										fullWidth
										component="fieldset"
									>
										<FormGroup>
											<FormControlLabel
												control={
													<Checkbox
														checked={form.materialLearn.template_quiz ? form.materialLearn.template_quiz : false}
														onChange={this.handleChangeCheckboxLearn('template_quiz')}
													/>	
												}
												label="Quiz"
											/>
										</FormGroup>
									</FormControl>
									<FormControl 
										margin="normal"
										fullWidth
										component="fieldset"
									>
										<FormGroup>
											<FormControlLabel
												control={
													<Checkbox
														checked={form.materialLearn.grafic_table ? form.materialLearn.grafic_table : false}
														onChange={this.handleChangeCheckboxLearn('grafic_table')}
													/>	
												}
												label="Tabla de Graficos"
											/>
										</FormGroup>
									</FormControl>
									<FormControl 
										margin="normal"
										fullWidth
										component="fieldset"
									>
										<FormGroup>
											<FormControlLabel
												control={
													<Checkbox
														checked={form.materialLearn.file_video ? form.materialLearn.file_video : false}
														onChange={this.handleChangeCheckboxLearn('file_video')}
													/>	
												}
												label="Videos"
											/>
										</FormGroup>
									</FormControl>
								</Grid>
								<Grid
									item
									xs={12}
									sm={12}
									md={3}
									lg={3}
									xl={3}
									style={{
										paddingLeft: 10,
										paddingRight: 10
									}}
								>
									<FormControl 
										margin="normal"
										fullWidth
										component="fieldset"
									>
										<FormGroup>
											<FormControlLabel
												control={
													<Checkbox
														checked={form.materialLearn.file_text ? form.materialLearn.file_text : false}
														onChange={this.handleChangeCheckboxLearn('file_text')}
													/>	
												}
												label="Archivos de texto"
											/>
										</FormGroup>
									</FormControl>
									<FormControl 
										margin="normal"
										fullWidth
										component="fieldset"
									>
										<FormGroup>
											<FormControlLabel
												control={
													<Checkbox
														checked={form.materialLearn.presentation ? form.materialLearn.presentation : false}
														onChange={this.handleChangeCheckboxLearn('presentation')}
													/>	
												}
												label="Presentaciones"
											/>
										</FormGroup>
									</FormControl>
									<FormControl 
										margin="normal"
										fullWidth
										component="fieldset"
									>
										<FormGroup>
											<FormControlLabel
												control={
													<Checkbox
														checked={form.materialLearn.file_audio ? form.materialLearn.file_audio : false}
														onChange={this.handleChangeCheckboxLearn('file_audio')}
													/>	
												}
												label="Audios"
											/>
										</FormGroup>
									</FormControl>
								</Grid>
								<Grid
									item
									xs={12}
									sm={12}
									md={3}
									lg={3}
									xl={3}
									style={{
										paddingLeft: 10,
										paddingRight: 10
									}}
								>
									<FormControl 
										margin="normal"
										fullWidth
										component="fieldset"
									>
										<FormGroup>
											<FormControlLabel
												control={
													<Checkbox
														checked={form.materialLearn.file_image ? form.materialLearn.file_image : false}
														onChange={this.handleChangeCheckboxLearn('file_image')}
													/>	
												}
												label="Imagenes"
											/>
										</FormGroup>
									</FormControl>
									<FormControl 
										margin="normal"
										fullWidth
										component="fieldset"
									>
										<FormGroup>
											<FormControlLabel
												control={
													<Checkbox
														checked={form.materialLearn.questionnaries ? form.materialLearn.questionnaries : false}
														onChange={this.handleChangeCheckboxLearn('questionnaries')}
													/>	
												}
												label="Cuestionarios"
											/>
										</FormGroup>
									</FormControl>
								</Grid>
								<button type="submit" id="new/submit" style={{display: 'none'}}>Enviar</button>	
							</Grid>
						</form>
					</Paper>
        )
    }
}   
 export default New;