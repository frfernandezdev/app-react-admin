import React, { Component } from 'react';
import axios from 'axios';
import {
	Button,
	Dialog,
	AppBar,
	Toolbar,
	IconButton,
	Typography,
	Grid,
	Paper,
	TextField,
	FormControl,
	InputAdornment,
	Icon,
	MenuItem,
	Slide,
	Avatar,
	List,
	ListSubheader,
	ListItem,
	ListItemText,
	Divider,
	FormControlLabel,
	FormGroup,
	Checkbox,
	CircularProgress
} from '@material-ui/core';
import green from '@material-ui/core/colors/green';
import SwipeableViews from 'react-swipeable-views';
import * as FontAwesome from 'react-icons/lib/fa';
import {
	SelectLocalities,
	SelectTimeZone,
	SelectCurrentMoney,
	SelectLanguage,
	SelectLanguageNative,
	UploadButton,
	DateNow,
	SnackbarLoading,
	SelectLanguageToLearn,
	SummaryCv,
	Calendar
} from '../../utils';
import moment from 'moment';

function Transition(props) {
	return <Slide direction="up" {...props} />;
}
class View extends Component {
	state = { 
		open: false, 
		uid: null, 
		applications: null, 
		page: 0,
		show: '',
		loading: false,
		loadingCancel: false
	}
	constructor(props){
		super(props)

		this.auth = this.props.firebase.auth;
		this.handleOpen  = this.handleOpen.bind(this)
		this.handleClose = this.handleClose.bind(this)
		this.props.handleOpen(this.handleOpen)
		this.props.handleClose(this.handleClose)
	}
	handleDataInfo = () => {
		this.state.onClickSnackLoadingOpen()
		// http://localhost:5000/admin-speakenglishsite/us-central1/api/applications
		// https://us-central1-admin-speakenglishsite.cloudfunctions.net/api/applications
		axios.get(`http://localhost:5000/admin-speakenglishsite/us-central1/api/applications/${this.state.uid}`, {
			params: {
				token: this.props.user.auth.token
			}
		})
		.then(response => {
			if(response.data.success === true){
				this.setState({ 
					open: true, 
					applications: response.data.item,
					show: 'show'
				})
			}else {
				if(response.data.error.code)
					return this.props.handleSnackbar({ type: 'auth', message: response.data.error.code, on: true })
				return this.props.handleSnackbar({ type: 'custom', message: response.data.error, on: true })
			}
		})
		.catch(error => {
			console.log(error)
			this.setState({ uid: null })
			if(error.response.data && error.response.data.error)
				return this.props.handleSnackbar({ type: 'auth', message: error.response.data.error.code, on: true })
			return this.props.handleSnackbar({ type: 'custom', message: '404', on: true })
		})
		.finally(() => this.state.onClickSnackLoadingClose())
	}
	handleOpen = uid => {
		if(uid !== this.state.uid)
			this.setState({ uid }, () => this.handleDataInfo())
		else if(uid === this.state.uid && this.state.applications !== null)
			this.setState({ open: true, show: 'show' })
		else if(uid === this.state.uid && this.state.applications === null)
			this.handleDataInfo()
	}
	handleClose = () => this.setState({ open: false, show: '', applications: null })
	handlePage = page => event => {
		this.setState({ page })
	}
	onSubmitAccept = event => {
		this.setState({ loading: !this.state.loading })
		axios.put(`http://localhost:5000/admin-speakenglishsite/us-central1/api/applications/accept/${this.state.uid}`, {
			token: this.props.user.auth.token
		})
		.then(response => {
			if(response.data.success){
				this.props.resetList && this.props.resetList();
				this.props.handleSnackbar({type: 'custom', message: response.data.mes})
				this.handleClose();
			}else {
				if(response.data.error.code)
					return this.props.handleSnackbar({ type: 'auth', message: response.data.error.code, on: true })
				return this.props.handleSnackbar({ type: 'custom', message: response.data.error, on: true })
			}
		})
		.catch(error => {
			console.log(error)
			return this.props.handleSnackbar({ type: 'custom', message: '404', on: true })
		})
		.finally(() => this.setState({ loading: !this.state.loading }))
	}
	onSubmitCancel = evnt => {
		this.setState({ loadingCancel: !this.state.loadingCancel })
		axios.put(`http://localhost:5000/admin-speakenglishsite/us-central1/api/applications/cancel/${this.state.uid}`, {
			token: this.props.user.auth.token
		})
		.then(response => {
			if(response.data.success){
				this.props.resetList && this.props.resetList();
				this.props.handleSnackbar({type: 'custom', message: response.data.mes})
				this.handleClose();
			}else {
				if(response.data.error.code)
					return this.props.handleSnackbar({ type: 'auth', message: response.data.error.code, on: true })
				return this.props.handleSnackbar({ type: 'custom', message: response.data.error, on: true })
			}
		})
		.catch(error => {
			console.log(error)
			return this.props.handleSnackbar({ type: 'custom', message: '404', on: true })
		})
		.finally(() => this.setState({ loadingCancel: !this.state.loadingCancel }))
	}
	YearStart = () => {
    let array = [];
    for(let index = 0; index < 73; index++) {
      array[index] = moment().subtract(index, 'year').format('YYYY');
    }
    return array;
  }
  YearEnd = () => {
    let array = [];
    for(let index = 0; index < 5; index++) {
      array[index] = moment().add(index, 'year').format('YYYY');
    }
    return array;
  }
	render(){
		const { page, applications, done, value, loading, loadingCancel } = this.state;	

		return(
			<div>
				<div className={`btn-fab bottom ${this.state.show}`}>
					<div style={{ position: 'relative', display: 'inline-block' }}>
						<Button
							variant="fab"
							color="secondary"
							onClick={this.onSubmitCancel}
							style={{
								width: 40,
								height: 40,
								marginRight: 12
							}}
						>
							<Icon>delete_forever</Icon>
						</Button>
						{ 
							loadingCancel 
								&&
							<CircularProgress 
								size={52} 
								style={{color: 'white'}} 
								className="circle-progress" 
							/>
						}
					</div>
					<div style={{ position: 'relative', display: 'inline-block' }}>
						<Button
							variant="fab"
							color="default"
							onClick={this.onSubmitAccept}
							style={
								{
									color: 'white',
									backgroundColor: green[500]
								} 
							}
						>
							<Icon>done_all</Icon>
						</Button>
						{ 
							loading 
								&&
							<CircularProgress 
								size={68} 
								style={{color: 'white'}} 
								className="circle-progress" 
							/>
						}
					</div>
				</div>
				<Dialog
					fullScreen
					open={this.state.open}
					onClose={this.handleClose}
					TransitionComponent={Transition}
				>
					<AppBar style={{position: 'relative'}}>
						<Toolbar>
							<IconButton
								color="inherit"
								onClick={this.handleClose}
							>
								<Icon>close</Icon>
							</IconButton>
							<Typography variant="title" color="inherit">
								Detalles de la postulacion
							</Typography>
						</Toolbar>
					</AppBar>
					<div style={{ flexGrow: 1, padding: 16 }}>
						<Grid container>
							<Grid 
								item
								xs={12}
								sm={12}
								md={3}
								lg={3}
								xl={3}
							>
								<Paper style={{ display: 'flex', position: 'relative', flexDirection: 'column' }}>
									<div style={{ display: 'flex', justifyContent: 'center',paddingTop: 16 }}>
										<Avatar
											style={{ width: 160, height: 160, boxShadow: '0 0 1px black' }} 
											alt={
												applications 
													? 
												applications.user.username 
													: 
												''
											}
											src={
												applications 
													? 
												applications.user.photo 
													: 
												''
											}
										/>
									</div>
									<List
										component="nav"
										style={{ paddingBottom: 0 }}
										subheader={
											<div>
												<ListSubheader component="div" style={{ textAlign: 'center', lineHeight: '36px', color: 'black', paddingBottom: 12 }}>
													{ 
														applications 
															? 
														applications.user.fullname 
															: 
														'' 
													}
													<br/>
													Fecha de postulacion:
													&nbsp;
													{
														applications 
															? 
														moment(applications.application_date).format('YYYY-MMMM-DD') 
															: 
														''
													}
												</ListSubheader>
											</div>
										}
									>
										<Divider/>
										<ListItem button style={{textAlign: 'center'}} onClick={this.handlePage(0)} divider>
											<ListItemText primary="Información del Usuario"></ListItemText>
										</ListItem>
										<ListItem button style={{textAlign: 'center'}} onClick={this.handlePage(1)} divider>
											<ListItemText primary="Idiomas y Experiencia"></ListItemText>
										</ListItem>
										<ListItem button style={{textAlign: 'center'}} onClick={this.handlePage(2)} divider>
											<ListItemText primary="Habilidades y Materiales de enseñanza"></ListItemText>
										</ListItem>
										<ListItem button style={{textAlign: 'center'}} onClick={this.handlePage(3)} divider>
											<ListItemText primary="Calendario de clases"></ListItemText>
										</ListItem>
									</List>
								</Paper>
							</Grid>
							<Grid 
								item
								xs={12}
								sm={12}
								md={9}
								lg={9}
								xl={9}
							>
								<div style={{padding: 15, paddingTop: 0}}>
									<SwipeableViews
										index={page}
										slideStyle={{
											alignItems: 'inherit',
											display: 'initial',
											padding: 5
										}}
									>
										<Paper style={{ padding: 15, display: 'flex', position: 'relative', flexDirection: 'column' }}>
											<Typography variant="title" gutterBottom>Información del Usuario</Typography>
											<Divider/>
											<Grid container spacing={8}>
												<Grid 
													item
													xl={6}
													lg={6}
													md={6}
													sm={12}
													xs={12}
												>
													<TextField
														id="view/fullname"
														label="Nombre completo"
														className="input-view"
														margin="normal"
														fullWidth
														disabled
														type='text'
														value={applications ? applications.user.fullname : ''}
														inputProps={{
															pattern: '[A-Za-z ]{1,}',
															title: 'Solo se aceptan letras'
														}}
													/>
													<TextField
														id="view/mail"
														label="Correo electronico"
														margin="normal"
														disabled
														className="input-view"
														fullWidth
														type='email'
														value={applications ? applications.user.email : ''}
													/>
													<TextField
														id="new/phone"
														label="Telefono casa"
														className="input-view"
														placeholder="+581234567890"
														margin="normal"
														fullWidth
														disabled
														type='phone'
														inputProps={{
															pattern: '[+][0-9]{12}',
															title: 'Solo se aceptan letras'
														}}
														value={applications ? applications.user.phone : ''}
													/>
													<TextField
														id="new/watch"
														className="input-view"
														select
														label="Tipo hora"
														disabled
														value={applications ? applications.user.watch : ''}
														fullWidth
														margin="normal"
													>
														<MenuItem key={1} value={1}>
															24Hrs
														</MenuItem>
														<MenuItem key={2} value={2}>
															12Hrs
														</MenuItem>
													</TextField>
													<SelectTimeZone 
														id="new/timezone"
														label="Zona horaria"
														disabled
														className="input-view"
														value={applications ? applications.user.timezone : ''}
													/>
												</Grid>
												<Grid 
													item
													xl={6}
													lg={6}
													md={6}
													sm={12}
													xs={12}
												>
													<TextField
														id='view/username'
														label="Nombre de usuario"
														margin="normal"
														fullWidth
														disabled
														className="input-view"
														type='text'
														value={applications ? applications.user.username : ''}
													/>
													<TextField
														id="new/gender"
														className="input-view"
														select
														label="Genero"
														disabled
														value={applications ? applications.user.gender : ''}
														fullWidth
														margin="normal"
													>
														<MenuItem key={'M'} value={'M'}>
															Masculino
														</MenuItem>
														<MenuItem key={'F'} value={'F'}>
															Femenino
														</MenuItem>
													</TextField>
													<TextField
														id="view/mobile"
														label="Telefono movil"
														placeholder="+581234567890"
														margin="normal"
														fullWidth
														disabled 
														className="input-view"
														type='phone'
														inputProps={{
															pattern: '[+][0-9]{12}',
															title: 'Solo se aceptan letras'
														}}
														value={applications ? applications.user.mobile : ''}
													/>
													<TextField
														id="new/company"
														label="Compañia"
														margin="normal"
														disabled
														className="input-view"
														type="text"
														fullWidth
														value={applications ? applications.user.company : ''}
													/>
													<TextField
														id="new/address"
														label="Direccion"
														disabled
														className="input-view"
														multiline
														fullWidth
														margin="normal"
														value={applications ? applications.user.address : ''}
													/>
												</Grid>
												<Grid
													item
													xs={12}
													sm={12}
													md={12}
													lg={12}
													xl={12}
												>
													<TextField
														id="new/website"
														label="Sitio web"
														margin="normal"
														disabled
														className="input-view"
														type="url"
														fullWidth
														value={applications ? applications.user.website : ''}
													/>
												</Grid>
												<Grid
													item
													xs={12}
													sm={12}
													md={12}
													lg={12}
													xl={12}
												>
													<Typography variant="title" gutterBottom>Como profesor</Typography>
													<Divider/>
												</Grid>
												<Grid
													item
													xl={6}
													lg={6}
													md={6}
													sm={12}
													xs={12}
												>
													<TextField
														id="new/about_me"
														label="Sobre mi"
														multiline
														fullWidth
														disabled
														className="input-view"
														margin="normal"
														value={applications ? applications.aboutMe : ''}
													/>
												</Grid>
												<Grid
													item
													xl={6}
													lg={6}
													md={6}
													sm={12}
													xs={12}
												>
													<TextField
														id="new/methodology"
														label="Metodologia"
														multiline
														fullWidth
														disabled
														className="input-view"
														margin="normal"
														value={applications ? applications.methodology : ''}
													/>
												</Grid>
												<Grid
													item
													xl={12}
													lg={12}
													md={12}
													sm={12}
													xs={12}
												>
													<TextField
														id="new/video"
														label="Link Video"
														fullWidth
														disabled
														className="input-view"
														margin="normal"
														type="url"
														InputProps={{
															startAdornment: 
																<InputAdornment>
																	<Icon>video_library</Icon>
																</InputAdornment>	
														}}
														value={applications ? applications.video : ''}
													/>
												</Grid>
											</Grid>
										</Paper>
										<Paper style={{ padding: 15, display: 'flex', position: 'relative', flexDirection: 'column' }}>
											<Typography variant="title" style={{marginTop: 10}}>Idiomas a Enseñar</Typography>
											<Divider/>
											<Grid container spacing={8}>
												<Grid
													item 
													xs={12} 
													sm={12} 
													md={12} 
													lg={12} 
													xl={12} 
													style={{
														paddingLeft: 10,
														paddingRight: 10
													}}
												>
													<SelectLanguageToLearn 
														id="new/languageToLearn"
														className="input-view"
														value={
															applications
																?
															applications.languageToLearn
																:
															''											
														}
														disabled
														firebase={this.props.firebase}
													/>
												</Grid>
												<Grid 
													item 
													xs={12} 
													sm={12} 
													md={12} 
													lg={12} 
													xl={12}
												>
													<Typography variant="title" style={{marginTop: 10}}>Educacion y Experiencia Laboral</Typography>
													<Divider/>
												</Grid>
												{	
													applications 
														&&
													applications.student.length > 0
														&&
													applications.student.map((docs, i) => (
														<Grid container key={i}>
															<Grid
																item
																xs={12}
																sm={12}
																md={12}
																lg={12}
																xl={12}
																style={{
																	paddingLeft: 10,
																	paddingRight: 10
																}}
															>
																<TextField
																	select
																	label="Titulo"
																	fullWidth
																	margin="normal"
																	disabled
																	className="input-view"
																	value={docs ? docs.title : ''}
																>
																	<MenuItem value="Licenciatura">
																		Licenciatura
																	</MenuItem>
																	<MenuItem value="Maestría">
																		Maestría
																	</MenuItem>
																	<MenuItem value="Doctorado">
																		Doctorado
																	</MenuItem>
																	<MenuItem value="Postdoctorado">
																		Postdoctorado
																	</MenuItem>
																	<MenuItem value="Otros">
																		Otros
																	</MenuItem>
																</TextField>
																<TextField
																	label="Escuela ó Institución"
																	fullWidth
																	margin="normal"
																	disabled
																	className="input-view"
																	value={docs ? docs.institute : ''}
																/>
																<TextField
																	label="Especialidad ó Tema de estudio"
																	fullWidth
																	margin="normal"
																	disabled
																	className="input-view"
																	value={docs ? docs.especial_team : ''}
																/>
															</Grid>
															<Grid
																item
																xs={12}
																sm={12}
																md={6}
																lg={6}
																xl={6}
																style={{
																	paddingLeft: 10,
																	paddingRight: 10
																}}
															>
																<TextField
																	select
																	label="Fecha de Inicio"
																	fullWidth
																	margin="normal"
																	disabled
																	className="input-view"
																	value={docs ? docs.start : ''}
																>
																	{
																		this.YearStart().map((docs) => (
																			<MenuItem key={docs} value={docs}>
																				{docs}
																			</MenuItem>
																		))
																	}
																</TextField>
															</Grid>
															<Grid
																item
																xs={12}
																sm={12}
																md={6}
																lg={6}
																xl={6}
																style={{
																	paddingLeft: 10,
																	paddingRight: 10
																}}
															>
																<TextField
																	select
																	label="Fecha de Culminacion"
																	fullWidth
																	margin="normal"
																	disabled
																	className="input-view"
																	value={docs ? docs.end : ''}
																>
																	{
																		this.YearEnd().map((docs) => (
																			<MenuItem key={docs} value={docs}>
																				{docs}
																			</MenuItem>
																		))
																	}
																</TextField> 
															</Grid>
															{
																docs && docs.description
																	&&
																<Grid
																	item
																	xs={12}
																	sm={12}
																	md={12}
																	lg={12}
																	xl={12}
																	style={{
																		paddingLeft: 10,
																		paddingRight: 10
																	}}
																>
																	<TextField
																		label="Description (Options)"
																		multiline
																		rows="4"
																		fullWidth
																		margin="normal"
																		disabled
																		className="input-view"
																		value={docs ? docs.description : ''}
																	/>
																</Grid>
															}
															<Grid
																item
																xs={12}
																sm={12}
																md={12}
																lg={12}
																xl={12}
																style={{
																	paddingLeft: 10,
																	paddingRight: 10
																}}
															>
																<UploadButton
																	value={docs ? docs.file.file : ''}
																	// disabled
																	labelButton="Subir image"
																	labelButtonSuccess="Ver image"
																	labelDialog="Subir image del documento"
																	style={
																		{
																			textAlign: 'center',
																			paddingTop: 50,
																			paddingBottom: 20
																		}
																	}
																/> 
															</Grid>
														</Grid>
													))
												}
												{
													applications 
														&&
													applications.professional_expirence.length > 0
														&&
													applications.professional_expirence.map((docs, i) => (
														<Grid container key={i}>
															<Grid
																item
																xs={12}
																sm={12}
																md={6}
																lg={6}
																xl={6}
																style={{
																	paddingLeft: 10,
																	paddingRight: 10
																}}
															>
																<TextField
																	label="Empresa"
																	margin="normal"
																	fullWidth
																	disabled
																	className="input-view"
																	value={docs ? docs.enterprise : ''}
																/>
															</Grid>
															<Grid
																item
																xs={12}
																sm={12}
																md={6}
																lg={6}
																xl={6}
																style={{
																	paddingLeft: 10,
																	paddingRight: 10
																}}
															>
																<TextField
																	label="Puesto"
																	margin="normal"
																	fullWidth
																	disabled
																	className="input-view"
																	value={docs ? docs.position : ''}
																/>
															</Grid>
															<Grid
																item
																xs={12}
																sm={12}
																md={6}
																lg={6}
																xl={6}
																style={{
																	paddingLeft: 10,
																	paddingRight: 10
																}}
															>
																<TextField
																	select
																	label="Fecha de Inicio"
																	fullWidth
																	margin="normal"
																	disabled
																	className="input-view"
																	value={docs ? docs.start : ''}
																>
																	{
																		this.YearStart().map((docs) => (
																			<MenuItem key={docs} value={docs}>
																				{docs}
																			</MenuItem>
																		))
																	}
																</TextField>
															</Grid>
															<Grid
																item
																xs={12}
																sm={12}
																md={6}
																lg={6}
																xl={6}
																style={{
																	paddingLeft: 10,
																	paddingRight: 10
																}}
															>
																<TextField
																	select
																	label="Fecha de Culminacion"
																	fullWidth
																	margin="normal"
																	disabled
																	className="input-view"
																	value={docs ? docs.end : ''}
																>
																	{
																		this.YearEnd().map((docs) => (
																			<MenuItem key={docs} value={docs}>
																				{docs}
																			</MenuItem>
																		))
																	}
																</TextField>
															</Grid>
															<Grid
																item
																xs={12}
																sm={12}
																md={12}
																lg={12}
																xl={12}
																style={{
																	paddingLeft: 10,
																	paddingRight: 10
																}}
															>
																<SelectLocalities
																	label="Pais"
																	value={docs ? docs.country : ''}
																/>
																<TextField
																	label="Ciudad"
																	margin="normal"
																	fullWidth
																	disabled
																	className="input-view"
																	value={docs ? docs.city : ''}
																/>
																<TextField
																	label="Description (Options)"
																	multiline
																	rows="4"
																	fullWidth
																	margin="normal"
																	className="input-view"
																	value={docs ? docs.description : ''}
																/>
															</Grid>
															<Grid
																item
																xs={12}
																sm={12}
																md={12}
																lg={12}
																xl={12}
																style={{
																	paddingLeft: 10,
																	paddingRight: 10
																}}
															>
																<UploadButton
																	value={docs ? docs.file.file : ''}
																	// disabled
																	labelButton="Subir image"
																	labelButtonSuccess="Ver image"
																	labelDialog="Subir image del documento"
																	style={
																		{
																			textAlign: 'center',
																			paddingTop: 50,
																			paddingBottom: 20
																		}
																	}
																/> 
															</Grid>
														</Grid>
													))
												}
											</Grid>
										</Paper>
										<Paper style={{ padding: 15, display: 'flex', position: 'relative', flexDirection: 'column' }}>
											<Typography variant="title" style={{marginTop: 10}}>Habilidades de enseñanza</Typography>
											<Divider/>
											<Grid container spacing={8}>
												<Grid
													item
													xs={12}
													sm={12}
													md={6}
													lg={6}
													xl={6}
													style={{
														paddingLeft: 10,
														paddingRight: 10
													}}
												>
													<FormControl 
														margin="normal"
														fullWidth
														component="fieldset"
														disabled
														className="checkboxDisabled"
													>
														<FormGroup>
															<FormControlLabel
																control={
																	<Checkbox
																		checked={applications ? applications.skill.junnior : false}
																	/>	
																}
																label="Principiantes"
															/>
														</FormGroup>
													</FormControl>
													<FormControl 
														margin="normal"
														fullWidth
														component="fieldset"
														disabled
														className="checkboxDisabled"
													>
														<FormGroup>
															<FormControlLabel
																control={
																	<Checkbox
																		checked={applications ? applications.skill.business : false}
																	/>	
																}
																label="Negocios"
															/>
														</FormGroup>
													</FormControl>
												</Grid>
												<Grid
													item
													xs={12}
													sm={12}
													md={6}
													lg={6}
													xl={6}
													style={{
														paddingLeft: 10,
														paddingRight: 10
													}}
												>
													<FormControl 
														margin="normal"
														fullWidth
														component="fieldset"
														disabled
														className="checkboxDisabled"
													>
														<FormGroup>
															<FormControlLabel
																control={
																	<Checkbox
																		checked={applications ? applications.skill.kids : false}
																	/>	
																}
																label="Niños"
															/>
														</FormGroup>
													</FormControl>
													<FormControl 
														margin="normal"
														fullWidth
														component="fieldset"
														disabled
														className="checkboxDisabled"
													>
														<FormGroup>
															<FormControlLabel
																control={
																	<Checkbox
																		checked={applications ? applications.skill.teen : false}
																	/>	
																}
																label="Jovenes"
															/>
														</FormGroup>
													</FormControl>
												</Grid>
												<Grid 
													item 
													xs={12} 
													sm={12} 
													md={12} 
													lg={12} 
													xl={12} 
													style={{
														marginTop: 10,
														paddingLeft: 10,
														paddingRight: 10
													}}
												>
													<Typography variant="title" style={{marginTop: 10}}>Preparacion para Test</Typography>
												</Grid>
												<Grid
													item
													xs={12}
													sm={12}
													md={3}
													lg={3}
													xl={3}
													style={{
														paddingLeft: 10,
														paddingRight: 10
													}}
												>
													<FormControl 
														margin="normal"
														fullWidth
														component="fieldset"
														disabled
														className="checkboxDisabled"
													>
														<FormGroup>
															<FormControlLabel
																control={
																	<Checkbox
																		checked={applications ? applications.skill.test.IELTS : false}
																	/>	
																}
																label="IELTS"
															/>
														</FormGroup>
													</FormControl>
													<FormControl 
														margin="normal"
														fullWidth
														component="fieldset"
														disabled
														className="checkboxDisabled"
													>
														<FormGroup>
															<FormControlLabel
																control={
																	<Checkbox
																		checked={applications ? applications.skill.test.TOEIC : false}
																	/>	
																}
																label="TOEIC"
															/>
														</FormGroup>
													</FormControl>
													<FormControl 
														margin="normal"
														fullWidth
														component="fieldset"
														disabled
														className="checkboxDisabled"
													>
														<FormGroup>
															<FormControlLabel
																control={
																	<Checkbox
																		checked={applications ? applications.skill.test.CELU : false}
																	/>	
																}
																label="CELU"
															/>
														</FormGroup>
													</FormControl>
												</Grid>
												<Grid
													item
													xs={12}
													sm={12}
													md={3}
													lg={3}
													xl={3}
													style={{
														paddingLeft: 10,
														paddingRight: 10
													}}
												>
													<FormControl 
														margin="normal"
														fullWidth
														component="fieldset"
														disabled
														className="checkboxDisabled"
													>
														<FormGroup>
															<FormControlLabel
																control={
																	<Checkbox
																		checked={applications ? applications.skill.test.BEC : false}
																	/>	
																}
																label="BEC"
															/>
														</FormGroup>
													</FormControl>
													<FormControl 
														margin="normal"
														fullWidth
														component="fieldset"
														disabled
														className="checkboxDisabled"
													>
														<FormGroup>
															<FormControlLabel
																control={
																	<Checkbox
																		checked={applications ? applications.skill.test.CAE : false}
																	/>	
																}
																label="CAE"
															/>
														</FormGroup>
													</FormControl>
													<FormControl 
														margin="normal"
														fullWidth
														component="fieldset"
														disabled
														className="checkboxDisabled"
													>
														<FormGroup>
															<FormControlLabel
																control={
																	<Checkbox
																		checked={applications ? applications.skill.test.KET : false}
																	/>	
																}
																label="KET"
															/>
														</FormGroup>
													</FormControl>
												</Grid>
												<Grid
													item
													xs={12}
													sm={12}
													md={3}
													lg={3}
													xl={3}
													style={{
														paddingLeft: 10,
														paddingRight: 10
													}}
												>
													<FormControl 
														margin="normal"
														fullWidth
														component="fieldset"
														disabled
														className="checkboxDisabled"
													>
														<FormGroup>
															<FormControlLabel
																control={
																	<Checkbox
																		checked={applications ? applications.skill.test.TOEFL : false}
																	/>	
																}
																label="TOEFL"
															/>
														</FormGroup>
													</FormControl>
													<FormControl 
														margin="normal"
														fullWidth
														component="fieldset"
														disabled
														className="checkboxDisabled"
													>
														<FormGroup>
															<FormControlLabel
																control={
																	<Checkbox
																		checked={applications ? applications.skill.test.DELE : false}
																	/>	
																}
																label="DELE"
															/>
														</FormGroup>
													</FormControl>
													<FormControl 
														margin="normal"
														fullWidth
														component="fieldset"
														disabled
														className="checkboxDisabled"
													>
														<FormGroup>
															<FormControlLabel
																control={
																	<Checkbox
																		checked={applications ? applications.skill.test.FCE : false}
																	/>	
																}
																label="FCE"
															/>
														</FormGroup>
													</FormControl>
												</Grid>
												<Grid
													item
													xs={12}
													sm={12}
													md={3}
													lg={3}
													xl={3}
													style={{
														paddingLeft: 10,
														paddingRight: 10
													}}
												>
													<FormControl 
														margin="normal"
														fullWidth
														component="fieldset"
														disabled
														className="checkboxDisabled"
													>
														<FormGroup>
															<FormControlLabel
																control={
																	<Checkbox
																		checked={applications ? applications.skill.test.PET : false}
																	/>	
																}
																label="PET"
															/>
														</FormGroup>
													</FormControl>
													<FormControl 
														margin="normal"
														fullWidth
														component="fieldset"
														disabled
														className="checkboxDisabled"
													>
														<FormGroup>
															<FormControlLabel
																control={
																	<Checkbox
																		checked={applications ? applications.skill.test.CPE : false}
																	/>	
																}
																label="CPE"
															/>
														</FormGroup>
													</FormControl>
													<FormControl 
														margin="normal"
														fullWidth
														component="fieldset"
														disabled
														className="checkboxDisabled"
													>
														<FormGroup>
															<FormControlLabel
																control={
																	<Checkbox
																		checked={applications ? applications.skill.test.ILEC : false}
																	/>	
																}
																label="ILEC"
															/>
														</FormGroup>
													</FormControl>
												</Grid>
												<Grid 
													item 
													xs={12} 
													sm={12} 
													md={12} 
													lg={12} 
													xl={12} 
													style={{
														marginTop: 10,
														paddingLeft: 10,
														paddingRight: 10
													}}
												>
													<Typography variant="title" style={{marginTop: 10}}>Materiales de enseñanza</Typography>
												</Grid>
												<Grid
													item
													xs={12}
													sm={12}
													md={3}
													lg={3}
													xl={3}
													style={{
														paddingLeft: 10,
														paddingRight: 10
													}}
												>
													<FormControl 
														margin="normal"
														fullWidth
														component="fieldset"
														disabled
														className="checkboxDisabled"
													>
														<FormGroup>
															<FormControlLabel
																control={
																	<Checkbox
																		checked={applications ? applications.materialLearn.file_pdf : false}
																	/>	
																}
																label="Archivos PDF"
															/>
														</FormGroup>
													</FormControl>
													<FormControl 
														margin="normal"
														fullWidth
														component="fieldset"
														disabled
														className="checkboxDisabled"
													>
														<FormGroup>
															<FormControlLabel
																control={
																	<Checkbox
																		checked={applications ? applications.materialLearn.article_news : false}
																	/>	
																}
																label="Articulos de noticias"
															/>
														</FormGroup>
													</FormControl>
													<FormControl 
														margin="normal"
														fullWidth
														component="fieldset"
														disabled
														className="checkboxDisabled"
													>
														<FormGroup>
															<FormControlLabel
																control={
																	<Checkbox
																		checked={applications ? applications.materialLearn.task : false}
																	/>	
																}
																label="Tareas"
															/>
														</FormGroup>
													</FormControl>
												</Grid>
												<Grid
													item
													xs={12}
													sm={12}
													md={3}
													lg={3}
													xl={3}
													style={{
														paddingLeft: 10,
														paddingRight: 10
													}}
												>
													<FormControl 
														margin="normal"
														fullWidth
														component="fieldset"
														disabled
														className="checkboxDisabled"
													>
														<FormGroup>
															<FormControlLabel
																control={
																	<Checkbox
																		checked={applications ? applications.materialLearn.template_quiz : false}
																	/>	
																}
																label="Quiz"
															/>
														</FormGroup>
													</FormControl>
													<FormControl 
														margin="normal"
														fullWidth
														component="fieldset"
														disabled
														className="checkboxDisabled"
													>
														<FormGroup>
															<FormControlLabel
																control={
																	<Checkbox
																		checked={applications ? applications.materialLearn.grafic_table : false}
																	/>	
																}
																label="Tabla de Graficos"
															/>
														</FormGroup>
													</FormControl>
													<FormControl 
														margin="normal"
														fullWidth
														component="fieldset"
														disabled
														className="checkboxDisabled"
													>
														<FormGroup>
															<FormControlLabel
																control={
																	<Checkbox
																		checked={applications ? applications.materialLearn.file_video : false}
																	/>	
																}
																label="Videos"
															/>
														</FormGroup>
													</FormControl>
												</Grid>
												<Grid
													item
													xs={12}
													sm={12}
													md={3}
													lg={3}
													xl={3}
													style={{
														paddingLeft: 10,
														paddingRight: 10
													}}
												>
													<FormControl 
														margin="normal"
														fullWidth
														component="fieldset"
														disabled
														className="checkboxDisabled"
													>
														<FormGroup>
															<FormControlLabel
																control={
																	<Checkbox
																		checked={applications ? applications.materialLearn.file_text : false}
																	/>	
																}
																label="Archivos de texto"
															/>
														</FormGroup>
													</FormControl>
													<FormControl 
														margin="normal"
														fullWidth
														component="fieldset"
														disabled
														className="checkboxDisabled"
													>
														<FormGroup>
															<FormControlLabel
																control={
																	<Checkbox
																		checked={applications ? applications.materialLearn.presentation : false}
																	/>	
																}
																label="Presentaciones"
															/>
														</FormGroup>
													</FormControl>
													<FormControl 
														margin="normal"
														fullWidth
														component="fieldset"
														disabled
														className="checkboxDisabled"
													>
														<FormGroup>
															<FormControlLabel
																control={
																	<Checkbox
																		checked={applications ? applications.materialLearn.file_audio : false}
																	/>	
																}
																label="Audios"
															/>
														</FormGroup>
													</FormControl>
												</Grid>
												<Grid
													item
													xs={12}
													sm={12}
													md={3}
													lg={3}
													xl={3}
													style={{
														paddingLeft: 10,
														paddingRight: 10
													}}
												>
													<FormControl 
														margin="normal"
														fullWidth
														component="fieldset"
														disabled
														className="checkboxDisabled"
													>
														<FormGroup>
															<FormControlLabel
																control={
																	<Checkbox
																		checked={applications ? applications.materialLearn.file_image : false}
																	/>	
																}
																label="Imagenes"
															/>
														</FormGroup>
													</FormControl>
													<FormControl 
														margin="normal"
														fullWidth
														component="fieldset"
														disabled
														className="checkboxDisabled"
													>
														<FormGroup>
															<FormControlLabel
																control={
																	<Checkbox
																		checked={applications ? applications.materialLearn.questionnaires : false}
																	/>	
																}
																label="Cuestionarios"
															/>
														</FormGroup>
													</FormControl>
												</Grid>
											</Grid>
										</Paper>
										<Paper style={{ padding: 15, display: 'flex', position: 'relative', flexDirection: 'column' }}>
											<Typography variant="title" gutterBottom>Como profesor</Typography>
											<Divider/>
											<Grid container>
												<Grid
													item
													xs={12}
													sm={12}
													md={12}
													lg={12}
													xl={12}
												>
													<Calendar
														schedule={applications ? applications.schedule : ''} 
														seletedAll={false}
														onChange={event => console.log(event) }
													/>
												</Grid>
											</Grid>
										</Paper>
									</SwipeableViews>
								</div>
							</Grid>
						</Grid>
					</div>
				</Dialog>
				<SnackbarLoading 
					handleClick={event => this.setState({ onClickSnackLoadingOpen: event })} 
					handleClose={event => this.setState({ onClickSnackLoadingClose: event })} 
				/>
			</div>
		)
	}
}
	export default View;