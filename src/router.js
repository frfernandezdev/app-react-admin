import React, { Component } from 'react';
import {
	BrowserRouter as Router, 
	Route,  
	Switch 
} from 'react-router-dom';
import { CssBaseline } from '@material-ui/core';
import { Loader } from './utils';

import {
	Auth,
	Layout,
	Home,
	Users,
	Languages,
	currentMoney,
	Applications,
	Teachers
} from './component'

const routes = {
	authorized: [
		{
			path: '/',
			component: Layout,
			exact: false,
			routes: [
				{
					path: '/',
					component: Home,
					exact: true
				},
				{
					path: '/users',
					component: Users,
				},
				{
					path: '/languages',
					component: Languages,
				},
				{
					path: '/currentMoney',
					component: currentMoney,
				},
				{
					path: '/applications',
					component: Applications,
				},
				{
					path: '/teachers',
					component: Teachers
				}
			]
		}
	],
	unauthorized: [
		{
			path: '/',
			component: Auth
		}
	]
};

class Routers extends Component {
	state = {
		user: {
			info: {},
			auth: {}
		},
		error: null,
		render: <Loader/>
	}
	constructor(props){
		super();

		this.auth 		 = props.firebase.auth();
		this.firestore = props.firebase.firestore().collection('users');
		
		this.LoopRoutes - this.LoopRoutes.bind(this)
		this.Logout = this.Logout.bind(this)
		this.reloadInfo = this.reloadInfo.bind(this)
		this.resetToken = this.resetToken.bind(this)
	}
	componentWillMount(){
		this.auth.onAuthStateChanged(
			user => {
				if(user){
					user.getIdToken(true)
							.then(token => {
								this.firestore.doc(user.uid)
										.onSnapshot(
											response => {
												let data = {
													info: response.data(),
													auth: {
														...user,
														token: token
													}
												}
												console.log(token)
												data.info.priv != 1 ? this.Logout() : data;
			
												this.setState({
													render: this.LoopRoutes(data.info.priv === 1 ? routes.authorized : routes.unauthorized),
													user: data.info.priv === 1 ? data : null 
												})
											}
										)
							})
				}else {
					this.setState({
						render: this.LoopRoutes(routes.unauthorized), 
					})		
				}
			}
		)
	}
	reloadInfo = () => {
		console.log(this.props.firebase.auth().currentUser)
		this.setState({ 
			user: {
				...this.state.user,
				auth: {
					token: this.state.user.auth.token,
					...this.props.firebase.auth().currentUser
				}
			}  
		})
	}
	resetToken = () => {
		this.auth().currentUser.getIdToken(true)
			.then(token => {
				this.setState({
					user: {
						info: this.user.info,
						auth: {
							...this.auth.user,
							token: token
						}
					}
				})
			})
	}
	LoopRoutes = routes => routes.map((route, i) =>
		<Route
			key={i}
			path={route.path}
			exact={route.exact}
			children={props => 
				<route.component 
					{...props} 
					{...this.props} 
					routes={route.routes} 
					user={this.state.user} 
					reloadInfo={this.reloadInfo} 
					resetToken={this.resetToken} 
					error={this.state.error}
				/>
			}
		/>
	)
	Logout = () => {
		this.setState({
			error: 'No tiene provilegios de adminstrativos'
		}, () => {
			this.auth
					.signOut()
					.then(
						() => {
							console.log(`Ha salido`)
						}
					)
					.catch(err => console.log(`Error: ${err.code} ${err.message}`))
		})
	}
	render(){
		return (
			<Router>
				<div>
					<CssBaseline />
					<Switch>
						{this.state.render}
					</Switch>
				</div>
			</Router>
		)
	}
}

export default Routers;