import firebase from 'firebase/app';
import 'firebase/functions';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/storage';

const config = {
	apiKey: "AIzaSyCS_ozN4h8xZSy4j3ZXaR5Dt_B2PiNnfBw",
	authDomain: "admin-speakenglishsite.firebaseapp.com",
	databaseURL: "https://admin-speakenglishsite.firebaseio.com",
	projectId: "admin-speakenglishsite",
	storageBucket: "admin-speakenglishsite.appspot.com",
	messagingSenderId: "26854321532"
};

firebase.initializeApp(config);

export default firebase;