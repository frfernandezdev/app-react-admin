import React, { Component } from 'react';
import {
	Button,
	Dialog,
	AppBar,
	Toolbar,
	IconButton,
	Typography,
	Grid,
	Paper,
	Slide,
	Avatar,
	List,
	ListSubheader,
	ListItem,
	ListItemText,
	Divider,
	CircularProgress,
	Icon
} from '@material-ui/core';
import green from '@material-ui/core/colors/green';
import SwipeableViews from 'react-swipeable-views';
import { SnackbarLoading } from '../../utils';
import AccountInformation from './accountInformation';
import Credentials from './credentials';
import Language from './language';
import ToolsComunications from './tools-comunications';
import './index.css';

function Transition(props) {
	return <Slide direction="up" {...props} />;
}

class Profile extends Component {
	state = { 
		open: false, 
		uid: null, 
		user: null, 
		page: 0,
		toggle: null,
	}
	constructor(props){
		super(props);

		this.auth 								= this.props.firebase.auth;
		this.handleOpen  					= this.handleOpen.bind(this)
		this.handleClose 					= this.handleClose.bind(this)
		this.props.handleOpen(this.handleOpen)
		this.props.handleClose(this.handleClose)
	}
	handleOpen = uid => {
		this.setState({ open: true })
	}
	handlePage = page => event => {
		this.setState({ page, toggle: null })
	}	
	onClick = () => {
		switch(this.state.page){
			case 0:
				if(!this.state.toggle){
					this.setState({
						toggle: 1
					}, () => this.state.onclickEnabledAccount())
					break;

				}else if(this.state.toggle === 1)
					this.state.onclickSubmitAccount()	
				break; 
			case 1:
				if(!this.state.toggle){
					this.setState({
						toggle: 1 
					}, () => this.state.onclickEnabledCredentials())
					break;
				}else if(this.state.toggle === 1)
					this.state.onclickSubmitCredentials()	
				break;
			case 2:
				if(!this.state.toggle){
					this.setState({
						toggle: 1 
					}, () => this.state.onclickEnabledLanguage())
					break;
				}else if(this.state.toggle === 1)
					this.state.onclickSubmitLanguage()	
				break;
			case 3:
				if(!this.state.toggle){
					this.setState({
						toggle: 1 
					}, () => this.state.onclickEnabledTools())
					break;
				}else if(this.state.toggle === 1)
					this.state.onclickSubmitTools()	
				break;
		}
	}
	handleCancel = () => {
		switch(this.state.page){
			case 0:
				this.setState({ 
					toggle: null 
				}, () => this.state.onclickCancelAccount())
				break;
			case 1:
				this.setState({
					toggle: null
				}, () => this.state.onclickCancelCredentials())
				break;
			case 2:
				this.setState({
					toggle: null
				}, () => this.state.onclickCancelLanguage())
				break; 
			case 3:
				this.setState({
					toggle: null
				}, () => this.state.onclickCancelTools())
				break; 
				
			} 
	}
	handleClose = () => this.setState({ open: false })
	render(){
		const { page, toggle  } = this.state;
		return (
			<div className="root" id="profile">
				<Dialog
					fullScreen
					open={this.state.open}
					onClose={this.handleClose}
					TransitionComponent={Transition}
					PaperProps={{
						className: 'none-transform'
					}}
				>
					<AppBar style={{ position: 'relative' }}>
						<Toolbar>
							<IconButton
								color="inherit"
								onClick={this.handleClose}
							>
								<Icon>close</Icon>
							</IconButton>
							<Typography variant="title" color="inherit" style={{ flex: 1 }}>
								Mi Cuenta
							</Typography>
							{
								toggle === 1
									?
								<Button color="inherit" onClick={this.handleCancel}>
									Cancelar
								</Button>
									:
								null
							}
						</Toolbar>
					</AppBar>
					<div style={{ flexGrow: 1, padding: 16 }}>
						<Grid container>
							<Grid 
								item
								xs={12}
								sm={12}
								md={3}
								lg={3}
								xl={3}
							>
								<Paper style={{ display: 'flex', position: 'relative', flexDirection: 'column' }}>
									<div style={{ display: 'flex', justifyContent: 'center',paddingTop: 16 }}>
										<Avatar
											style={{ width: 160, height: 160, boxShadow: '0 0 1px black' }} 
											alt={this.props.user ? this.props.user.info.username : ''}
											src={this.props.user ? this.props.user.info.photo : ''}
										/>
									</div>
									<List
										component="nav"
										style={{ paddingBottom: 0 }}
										subheader={
											<div>
												<ListSubheader component="div" style={{ textAlign: 'center', lineHeight: '36px', color: 'black', paddingBottom: 12 }}>
													{/* { this.state.user ? this.state.user.username : '' }
													<br/> */}
													{ this.props.user ? this.props.user.info.fullname : '' }
													<br/>
													{
														this.props.user ?
															this.props.user.info.priv === 1 ? 'Administrador'
																:
															this.props.user.info.priv === 2 ? 'Supervisor'
																:
															this.props.user.info.priv === 3 ? 'Usuario'
																:
															null
															:
														''
													}
												</ListSubheader>
											</div>
										}
									>
										<Divider/>
										<ListItem button style={{textAlign: 'center'}} onClick={this.handlePage(0)} divider>
											<ListItemText primary="Información del Usuario"></ListItemText>
										</ListItem>
										<ListItem button style={{textAlign: 'center'}} onClick={this.handlePage(1)} divider>
											<ListItemText primary="Credenciales"></ListItemText>
										</ListItem>
										{
											this.props.user.info.priv !== 1 && this.props.user.info.priv !== 2
												?
											<ListItem button style={{textAlign: 'center'}} onClick={this.handlePage(2)} divider>
												<ListItemText primary="Idiomas"></ListItemText>
											</ListItem>
												:
											null
										}
										{
											this.props.user.info.priv !== 1 && this.props.user.info.priv !== 2
												?
											<ListItem button style={{textAlign: 'center'}} onClick={this.handlePage(3)} divider>
												<ListItemText primary="Herramientas de Comunicación"></ListItemText>
											</ListItem>
												:
											null
										}
									</List>
								</Paper>
							</Grid>
							<Grid 
								item
								xs={12}
								sm={12}
								md={9}
								lg={9}
								xl={9}
							>
								<div style={{padding: 15, paddingTop: 0}}>
									<SwipeableViews
										index={page}
										slideStyle={{
											alignItems: 'inherit',
											display: 'initial',
											padding: 5
										}}
									>
										<AccountInformation 
											handleOnEnabled={event => this.setState({ onclickEnabledAccount: event })} 
											handleOnSubmit={event => this.setState({ onclickSubmitAccount: event })}
											handleLoading={() => this.setState({ toggle: 2 })}
											handleDone={() => this.setState({ toggle: 3 })}
											// handleDataInfoReload={this.handleDataInfoReload}
											handleReset={() => this.setState({ toggle: null }) } 
											handleCancel={event => this.setState({ onclickCancelAccount: event })}
											page={page}
											{...this.props}
										/>
										<Credentials
											handleOnEnabled={event => this.setState({ onclickEnabledCredentials: event })} 
											handleOnSubmit={event => this.setState({ onclickSubmitCredentials: event })} 
											handleLoading={() => this.setState({ toggle: 2 })}
											handleDone={() => this.setState({ toggle: 3 })}
											handleReset={() => this.setState({ toggle: null }) }
											handleCancel={event => this.setState({ onclickCancelCredentials: event })}
											page={page} 
											{...this.props}
										/>
										{
											this.props.user.info.priv !== 1 && this.props.user.info.priv !== 2
												?
											<Language 
												handleOnEnabled={event => this.setState({ onclickEnabledLanguage: event })} 
												handleOnSubmit={event => this.setState({ onclickSubmitLanguage: event })} 
												handleLoading={() => this.setState({ toggle: 2 })}
												handleDone={() => this.setState({ toggle: 3 })}
												handleReset={() => this.setState({ toggle: null }) }
												handleCancel={event => this.setState({ onclickCancelLanguage: event })}
												page={page} 
												{...this.props}
											/>
												:
											''
										}
										{
											this.props.user.info.priv !== 1 && this.props.user.info.priv !== 2
												?
											<ToolsComunications 
												handleOnEnabled={event => this.setState({ onclickEnabledTools: event })} 
												handleOnSubmit={event => this.setState({ onclickSubmitTools: event })} 
												handleLoading={() => this.setState({ toggle: 2 })}
												handleDone={() => this.setState({ toggle: 3 })}
												handleReset={() => this.setState({ toggle: null }) }
												handleCancel={event => this.setState({ onclickCancelTools: event })}
												page={page} 
												{...this.props}
											/>
												:
											''
										}
									</SwipeableViews>
								</div>
							</Grid>
						</Grid>
						<div className={`btn-fab bottom show`}>
							<Button
								id="fab"
								variant="fab"
								color="default"
								style={
									toggle === 3
										?
									{
										backgroundColor: green[500],
										'&:hover': {
											backgroundColor: green[700],
										}
									}
									 : 
									{}
								}
								onClick={this.onClick}
							>
								{
									toggle === null
										?
									<Icon>edit</Icon>
										:
									toggle === 1 || toggle === 2
										? 
									<Icon>done</Icon>
										:
									toggle === 3
										?
									<Icon>done_all</Icon>
										:
									''
								}
							</Button>
							{ 
								toggle === 2 
									&&
								<CircularProgress size={68} style={{color: green[500]}} className="circle-progress" />
							}
						</div>
					</div>
				</Dialog>
				<SnackbarLoading 
					handleClick={event => this.setState({ onClickSnackLoadingOpen: event })} 
					handleClose={event => this.setState({ onClickSnackLoadingClose: event })} 
				/>
			</div>
		)
	}
}

export default Profile;