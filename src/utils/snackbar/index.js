import React,{ Component } from 'react';
import {
	Snackbar as SnackbarMaterial,
	IconButton,
	Icon,
	Slide
} from '@material-ui/core';

const auth = message => {
	let mes;
	switch(message){
		case 'auth/email-already-exists':
			mes = 'Correo Electronico ya esta registrado'
			break;
		case 'auth/user-disabled':
			mes = 'Usuario bloqueado'
			break;
		case 'auth/invalid-phone-number':
			mes = 'Formato de Numero de telefono incorrecto'
			break;
		case 'auth/phone-number-already-exists':
			mes = 'Numero de telefono ya esta registrado'
			break;
		case 'auth/user-not-found':
			mes = 'Usuario no registrado' 
			break;
		case 'auth/internal-error':
			mes = 'Oops, ¡Ha ocurrido un error interno!'
			break;
		default: 
			mes = 'Oops, Ha ocurrido un error de conexion'
			break;
	}
	return mes
}
const storage = message => {
	let mes;
	switch(message){
		case 'storage/unknown':
			mes = 'Ocurrió un error desconocido'
			break;
		case 'unauthenticated':
			mes = 'El usuario no se autenticó'
			break;
		case 'unauthorized':
			mes = 'El usuario no está autorizado para realizar la acción deseada'
			break;
		default: 
			mes = 'Oops, Ha ocurrido un error de conexion'
			break;
	}
	return mes
}
const custom = message => {
	let mes;
	switch(message){
		case '404':
			mes = 'Oops, Ha ocurrido un error de conexion'
			break;
		case 'not-authentication': 
			mes = 'No esta autentificado'
			break;
		case 'not-priv':
			mes = 'No posees privilegios para realizar esta accion'
		default: 
			mes = message;
			break;
	}
	return mes;
}

class Snackbar extends Component {
	state = {
		open: false,
		message: null,
		error: false,
	}
	constructor(){
		super()

		this.handleClick = this.handleClick.bind(this);
		this.handleClose = this.handleClose.bind(this);
	}
	componentWillMount(){
		this.props.handleClick(this.handleClick)
		this.props.handleClose(this.handleClose)
	}
	handleSnackbar = object => {
		return new Promise((resolve, reject) => {
			switch(object.type){
				case 'auth':
					resolve(auth(object.message))
					break;
				case 'storage':
					resolve(storage(object.message))
					break;
				case 'custom':
					resolve(custom(object.message))
			}
		})
	}
	handleClick = object => {
		this.handleSnackbar(object)
			.then(mes => {
				console.log(mes)
				this.setState({ open: true, message: mes, error: object.on });
			})
  };
	handleClose = (event, reason) => {
		if(reason === 'clickaway') {
      return;
    }
    this.setState({ open: false });
  };
	render(){
		const { open, error, message } = this.state;
		return (
			<SnackbarMaterial
				anchorOrigin={{
					vertical: 'bottom',
					horizontal: 'left',
				}}
				open={open}
				onClose={this.handleClose}
				autoHideDuration={6000}
				ContentProps={{
					'aria-describedby': 'message-id',
				}}
				message={
					<div style={{'display': 'flex'}}>
						{
							error
								&&
							<Icon color='error'>error</Icon>
						}
						{
							!error
								&&
							<Icon color='inherit'>check_box</Icon>
						}
						&nbsp;
						<span id="message-id" style={{'paddingTop': '2px'}}>
							{message}
						</span>
					</div>
				}
				action={
					<IconButton
						key="close"
						aria-label="Close"
						color="inherit"
						onClick={this.handleClose}
					>
						<Icon>close</Icon>
					</IconButton>
				}
			/>
		)
	}
}

export default Snackbar;