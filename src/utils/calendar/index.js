import React,{ Component } from 'react';
// import {} from 'prototy'
import moment from 'moment';
import {
  Button,
  IconButton,
  Icon,
  ButtonBase,
  Dialog,
  DialogContent,
  DialogTitle,
  DialogActions,
  AppBar,
  Toolbar,
  Typography,
  Slide
} from '@material-ui/core';
import './index.css';

moment.updateLocale('es', {
  week: {
    dow: 0,
  },
})

function Transition(props) {
	return <Slide direction="up" {...props} />;
}

class Calendar extends Component {
  state = {
    context: moment(),
    today: moment(),
    schedule: [],
    open: false
  }
  constructor(props){
    super(props);

    this.todayChange        = this.todayChange.bind(this);
    this.activeNextWeeck    = this.activeNextWeeck.bind(this);
    this.nextWeek           = this.nextWeek.bind(this);
    this.previousWeek       = this.previousWeek.bind(this);
    this.activeRenderHeader = this.activeRenderHeader.bind(this);
    this.renderRowsHeader   = this.renderRowsHeader.bind(this);
    this.renderWeekHeader   = this.renderWeekHeader.bind(this);
    this.renderAllHours     = this.renderAllHours.bind(this);
    this.renderWeekHours    = this.renderWeekHours.bind(this);
    this.renderWeekBody     = this.renderWeekBody.bind(this);
    this.handleClick        = this.handleClick.bind(this);
    this.handleAvailable    = this.handleAvailable.bind(this);
  }
  weekdays = moment.weekdays();
  weekdayShort = moment.weekdaysMin();
  months = moment.months();
  todayChange = () => {
    this.setState({
      context: this.state.today
    })
  }
  activeNextWeeck = () => this.state.context.week() > this.state.today.week() ? true : false;
  nextWeek = () => {
    let context = Object.assign({}, this.state.context);
    context = moment(context).add(1, 'week');
    this.setState({ context })
  }
  previousWeek = () => {
    let context = Object.assign({}, this.state.context);
    context = moment(context).subtract(1, 'week');
    this.setState({ context })
  }
  cleanAll = () => {
    this.setState({ schedule: [] });
  }
  onChange = event => {
    let schedule = {
      target: {
        value: this.state.schedule
      }
    }
    this.props.onChange && this.props.onChange(schedule);
    return this.handleDialogCalendar();
  }
  handleClick = (day, hour) => {
    let date  = moment().date(day).hour(hour).minute(0).second(0).format('YYYY-MM-DD HH:mm'),
        limit = this.props.limit;

    if(limit && this.state.schedule.length === limit) return;

    if(!this.state.schedule.find(docs => docs.date === date)){
      this.setState({
        schedule: [
          ...this.state.schedule,
          {
            date: date
          }
        ]
      })
    }
  }
  handleAvailable = (day, hour) => {
    let date       = moment().date(day).hour(hour).minute(0).second(0).format('YYYY-MM-DD HH:mm'),
        seletedAll = this.props.seletedAll,
        schedule   = this.props.schedule;

    if(seletedAll === true) {
      if(Array.isArray(this.state.schedule) && this.state.schedule.length > 0){
        for(const d of this.state.schedule) {
          let dd          = new Date(d.date),
              currentDate = moment(dd).format('YYYY-MM-DD HH:mm');
          if(moment(date).isSame(currentDate)){
            d.active = true;
            return d;
          }
        } 
      }
      return true;
    }

    if(Array.isArray(schedule) && schedule.length > 0){
      for(const d of schedule) {
        let dd          = new Date(d.date),
            currentDate = moment(dd).format('YYYY-MM-DD HH:mm');
        if(moment(date).isSame(currentDate)){
          if(Array.isArray(this.state.schedule) && this.state.schedule.length > 0){
            for(const active of this.state.schedule) {
              let c = new Date(active.date),
                  l = moment(c).format('YYYY-MM-DD HH:mm');
              
              if(moment(date).isSame(l)){
                d.active = true;
                break;
              }
            }
          }else d.active = false;
          return d;
        }
      } 
    }
    return false;
  }
  activeRenderHeader = index => {
    let date  = Object.assign({}, this.state.context),
        day   = moment(date).date(index).format('YYYY-MM-DD'),
        today = moment(this.state.today).format('YYYY-MM-DD');

    return moment(today).isSame(day);
  }
  renderRowsHeader = (i, array) => (
    <div 
      key={i} 
      className={ this.activeRenderHeader(i) ? 'active' : '' }
    >
      {i}/ {this.weekdayShort[array.length]}
    </div>
  )
  renderWeekHeader = () => {
    let DayWeek      = [],
        date         = Object.assign({}, this.state.context),
        firstDayWeek = moment(date).startOf('week').format('DD'),
        lastDayWeek  = moment(date).endOf('week').format('DD');
    
    if(firstDayWeek < lastDayWeek){
      for(let a = firstDayWeek - 1; a < lastDayWeek; a++) {
        DayWeek.push(this.renderRowsHeader(a + 1, DayWeek))
      }
      return DayWeek;
    }else {
      let date       = Object.assign({}, this.state.context),
          lastWeek   = moment(date).subtract(1, 'week'),
          daysMonth  = moment(date).subtract(1, 'month').daysInMonth();
      
      for(let i = firstDayWeek; i < daysMonth + 1; i++) {
        DayWeek.push(this.renderRowsHeader(i, DayWeek))
      }
      for(let e = 0; e < lastDayWeek; e++) {
        DayWeek.push(this.renderRowsHeader(e + 1, DayWeek))
      }
      return DayWeek;
    } 
  }
  renderAllHours = () => {
    let hours = [];
    for(let h = 0; h < 24; h++) {
      hours.push(
        <div key={h}>
            <span>
              { 
                h > 9 
                  ? 
                `${h}:00` 
                  : 
                `0${h}:00`
              }
            </span>
        </div> 
      )      
    }
    return hours;
  }
  renderWeekHours = (hour, day) => {
    let data    = this.handleAvailable(day, hour),
        status  = data.booking,
        active  = data.active;

    return (
      <ButtonBase
        key={day} 
        focusRipple
        className={
          `button-grid 
          ${
            this.props.seletedAll === true
              ?
            ''
              :
            data 
              ?
              status === true 
                ? 
              'not-availabled' 
                : 
              'available'
              :
            ''
          }
          ${
            data.active
              ?
            'active'
              :
            ''
          }`
        }
        disabled={
          !data
            ||
          status === true
            ||
          active === true 
        }
        onClick={event => this.handleClick(day, hour)}
      >
        { day }
        &nbsp;
          -
        &nbsp;
        { 
          hour > 9 
            ? 
          `${hour}:00` 
            : 
          `0${hour}:00`
        }
      </ButtonBase>
    )
  }
  renderWeekBody = () => {
    let HoursWeek    = [],
        date         = Object.assign({}, this.state.context),
        firstDayWeek = moment(date).startOf('week').format('DD'),
        lastDayWeek  = moment(date).endOf('week').format('DD');

    if(firstDayWeek < lastDayWeek){
      for(let h = 0; h < 24; h++) {
        HoursWeek[h] = [];
        for(let a = firstDayWeek - 1; a < lastDayWeek; a++) {
          HoursWeek[h].push(this.renderWeekHours(h ,a + 1))
        }        
      }
      return HoursWeek;
    }else {
      let date       = Object.assign({}, this.state.context),
          lastWeek   = moment(date).subtract(1, 'week'),
          daysMonth  = moment(date).subtract(1, 'month').daysInMonth();
      
      for(let h = 0; h < 24; h++) {
        HoursWeek[h] = [];
        for(let i = firstDayWeek; i < daysMonth + 1; i++) {
          HoursWeek[h].push(this.renderWeekHours(h ,i))
        }
        for(let e = 0; e < lastDayWeek; e++) { 
          HoursWeek[h].push(this.renderWeekHours(h ,e + 1))
        }
      }
      return HoursWeek;
    }
  }
  handleDialogCalendar = event => {
    this.setState({ open: !this.state.open })
  }
  render(){
    let date         = Object.assign({}, this.state.context), 
        monthsName   = moment(date).format('M'),
        firstDayWeek = moment(date).startOf('week').format('DD'),
        lastDayWeek  = moment(date).endOf('week').format('DD');
    const { open } = this.state;
    return (
      <div>
        <Button
          onClick={this.handleDialogCalendar}
          variant="raised"
          color="default"
          fullWidth
        >
          <Icon
            className={this.state.schedule.length > 0 ? 'button-active' : ''}
          >calendar_today</Icon>
          &nbsp;
          Calendario
        </Button>
        <Dialog
          fullScreen
          open={open}
          onClose={this.handleDialogCalendar}
          TransitionComponent={Transition}
          PaperProps={{
						className: 'none-transform'
					}}
        >
          <AppBar style={{ position: 'relative' }}>
            <Toolbar>
              <IconButton
                color="inherit"
                onClick={this.handleDialogCalendar}
              >
                <Icon>close</Icon>
              </IconButton>
              <Typography variant="title" color="inherit" style={{ flex: 1 }}>
                Calendario de disponibilidad
              </Typography>
            </Toolbar>
          </AppBar>
          <div className="container">
            <div className="headers">
              {
                this.activeNextWeeck() === true
                  &&
                <IconButton onClick={this.previousWeek}>
                  <Icon>keyboard_arrow_left</Icon>
                </IconButton> 
              }
              <Button size="small" onClick={this.todayChange}>
                Hoy
              </Button>
              <Button size="small" onClick={this.cleanAll}>
                Limpiar
              </Button>
              <div className="title">
                <div>
                  <b>{ firstDayWeek }</b>
                  -
                  <b>{ lastDayWeek }</b>
                  &nbsp;
                  <b>{ this.months[monthsName - 1] }</b>
                </div>
              </div> 
              <IconButton onClick={this.nextWeek}>
                <Icon>keyboard_arrow_right</Icon>
              </IconButton>
            </div>
            <div className="body-content">
              <div className="week-header">
                <div className="blank">
                  <div>00:00</div>
                </div>
                <div className="days">
                  { this.renderWeekHeader().map(docs => docs) }
                </div>
              </div>
              <div className="week-body">
                <div className="hours">
                    { this.renderAllHours().map(docs => docs) }
                </div>
                <div className="selecte-hours">
                  { 
                    this.renderWeekBody().map((docs, i) => (
                      <div className="week-rows" key={i}>
                        {
                          docs.map(docs => docs)
                        }
                      </div>
                    )) 
                  }
                </div>
              </div>
            </div>
          </div>
          <DialogActions>
            <Button onClick={this.handleDialogCalendar}>Cancelar</Button>
            <Button onClick={this.onChange}>Guardar</Button>
          </DialogActions>
        </Dialog>
      </div>
    )
    
  }
}

export default Calendar;

// Save information 
  // schudule: [
  //   {
  //     date: '2017-6-28',
  //     booking: true/false
  //   }
  // ]

// Props this Component 
  // seletedAll: Esta propieda permite que el calendario este completamente vacio con todos los campos seleccionables.
  // onChange: Este propiedad retorna un functions que traera un arreglo con las fechas seleccionadas.
  // limit: Esta propiedad permite limitar el numero de selecciones de fechas.
