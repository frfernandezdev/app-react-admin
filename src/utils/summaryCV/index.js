import React,{ Component } from 'react';
import PropTypes from 'prop-types';
import update from 'react-addons-update'
import {
  FormControl,
  Grid,
  TextField,
  MenuItem,
  Checkbox,
  Radio,
  Icon, 
  Button,
  ButtonBase,
  FormControlLabel,
  Dialog,
	DialogActions,
	DialogContent,
  DialogTitle,
  Typography
} from '@material-ui/core';
import moment from 'moment';
import {
  SelectLocalities,
  UploadButton
} from '../../utils';

class SummaryCv extends Component {
  state = {
    student: [],
    professional_expirence: [],
    option: null,
    object: {},
    open: false,
    flag: false,
  }
  constructor(){
    super();
  }
  componentWillUpdate(prevProps, prevState, snapshot){
		if('delete' in prevProps){
			if(prevProps.delete === true && prevState.student.length > 0 && prevState.professional_expirence.length > 0){
				return this.deleteForm();
			}
		}
		if('value' in prevProps){
			if(prevProps.value !== "" && prevState.student.length === 0 && prevState.professional_expirence.length === 0 && !this.state.flag){
        return this.valueForm(prevProps.value);
			}
		}
  }
  deleteForm = event => {
    return this.setState({
      student: [],
      professional_expirence: [],
    })
  }
  valueForm = event => {
    return this.setState({
      student: event.student ? event.student : [],
      professional_expirence: event.professional_expirence ? event.professional_expirence : [],
      flag: true
    })
  }
  onChange = () => {
    let change = {
      target: {
        value : {
          student: this.state.student,
          professional_expirence: this.state.professional_expirence
        }
      }
    }
    this.props.onChange && this.props.onChange(change);
  }
  handleRemoveLoop = (index, option) => event => {
    console.log(index, option)
    if(option === 'student')
      this.setState(prevState => ({
        student: update(prevState.student, {$splice: [[index, 1]]})
      }))
    else if(option === 'professional_expirence')
      this.setState(prevState => ({
        professional_expirence: update(prevState.professional_expirence, {$splice: [[index, 1]]})
      }))  
  }
  handleChange = name => event => {
    if(name === 'file'){
      return this.setState({
        object: {
          ...this.state.object,
          [name]: event.target.value,
          enableFile: this.state.option === 'stundent' ? this.state.student.length : this.state.professional_expirence.length
        }
      })
    }
    this.setState({
      object: {
        ...this.state.object,
        [name]: event.target.value
      }
    })
  }
  handleDialog = option => event => {
    this.setState({
      open: true,
      option
    })
  }
  handleClose = event => {
    this.setState({
      open: false,
      object: {}
    })
  }
  handleSave = event => {
    if(this.state.option === 'student' && Object.keys(this.state.object).length > 0){
      this.setState({
        student: [
          ...this.state.student,
          {
            ...this.state.object
          }
        ]
      }, () => this.onChange());
    }
    else if(this.state.option === 'professional_expirence' && Object.keys(this.state.object).length > 0){
      this.setState({
        professional_expirence: [
          ...this.state.professional_expirence,
          {
            ...this.state.object
          }
        ]
      }, () => this.onChange());
    }
    this.handleClose(); 
  }
  YearStart = () => {
    let array = [];
    for(let index = 0; index < 73; index++) {
      array[index] = moment().subtract(index, 'year').format('YYYY');
    }
    return array;
  }
  YearEnd = () => {
    let array = [];
    for(let index = 0; index < 5; index++) {
      array[index] = moment().add(index, 'year').format('YYYY');
    }
    return array;
  }
  render(){
    const { 
      limit,
      required, 
      disabled, 
      className 
    } = this.props;
    const { 
      open,
      option,
      object,
      student, 
      professional_expirence 
    } = this.state;
    return (
      <div>
        <Grid container>
          <Grid 
            item 
            xs={12} 
            sm={12} 
            md={12} 
            lg={12} 
            xl={12} 
            style={{
              paddingLeft: 10,
              paddingRight: 10
            }}
          >
            <Typography variant="body2" style={{marginTop: 10}}>Educacion</Typography>
          </Grid>
          {
            student.map((docs, i) => (
              !disabled
                &&
              <Grid container key={i}>
                <Grid
                  item
                  xs={12}
                  sm={12}
                  md={6}
                  lg={6}
                  xl={6}
                >
                  <FormControl
                    fullWidth
                    margin="normal"
                    style={{
                      marginTop: 26
                    }}
                  >
                    <Typography variant="body1">Escuela ó Institución:</Typography>
                    <Typography variant="body2" style={{ textAlign: 'center' }}>{ docs.institute }</Typography>
                  </FormControl>
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={12}
                  md={5}
                  lg={5}
                  xl={5}
                >
                  <FormControl
                    fullWidth
                    margin="normal"
                    style={{
                      marginTop: 26
                    }}
                  >
                    <Typography variant="body1">Titulo:</Typography>
                    <Typography variant="body2" style={{ textAlign: 'center' }}>{ docs.title }</Typography>
                  </FormControl>
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={12}
                  md={1}
                  lg={1}
                  xl={1}
                >
                  <FormControl
                    fullWidth
                    margin="normal"
                    style={{
                      alignItems: 'center',
                      marginTop: 26
                    }}
                    required={required}
                    disabled={disabled}
                    className={className}
                  >
                    <Button
                      type="button"
                      variant="fab"
                      mini
                      color="secondary"
                      aria-label="remove"
                      onClick={this.handleRemoveLoop(i, 'student')}
                    >
                      <Icon>remove</Icon>
                    </Button>
                  </FormControl>
                </Grid>
              </Grid>
            ))
          }
          {
            limit !== student.length && !disabled
              ?
            <Grid
              container
              justify="center"
              style={{
                padding: 16,
                paddingBottom: 16
              }}
            >
              <Button
                type="button"
                variant="fab"
                mini
                color="primary"
                aria-label="add"
                onClick={this.handleDialog('student')}
              > 
                <Icon>add</Icon>
              </Button>
            </Grid>
              :
            ''
          }       
        </Grid>
        <Grid container>
          <Grid 
            item 
            xs={12} 
            sm={12} 
            md={12} 
            lg={12} 
            xl={12} 
            style={{
              paddingLeft: 10,
              paddingRight: 10
            }}
          >
            <Typography variant="body2" style={{marginTop: 10}}>Experiencia profesional</Typography>
          </Grid>
          {
            professional_expirence.map((docs, i) => (
              !disabled
                &&
              <Grid container key={i}>
                <Grid
                  item
                  xs={12}
                  sm={12}
                  md={6}
                  lg={6}
                  xl={6}
                >
                  <FormControl
                    fullWidth
                    margin="normal"
                    style={{
                      marginTop: 26
                    }}
                  >
                    <Typography variant="body1">Empresa:</Typography>
                    <Typography variant="body2" style={{ textAlign: 'center' }}>{ docs.enterprise }</Typography>
                  </FormControl>
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={12}
                  md={5}
                  lg={5}
                  xl={5}
                >
                  <FormControl
                    fullWidth
                    margin="normal"
                    style={{
                      marginTop: 26
                    }}
                  >
                    <Typography variant="body1">Puesto:</Typography>
                    <Typography variant="body2" style={{ textAlign: 'center' }}>{ docs.position }</Typography>
                  </FormControl>
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={12}
                  md={1}
                  lg={1}
                  xl={1}
                >
                  <FormControl
                    fullWidth
                    margin="normal"
                    style={{
                      alignItems: 'center',
                      marginTop: 26
                    }}
                    required={required}
                    disabled={disabled}
                    className={className}
                  >
                    <Button
                      type="button"
                      variant="fab"
                      mini
                      color="secondary"
                      aria-label="remove"
                      onClick={this.handleRemoveLoop(i, 'professional_expirence')}
                    >
                      <Icon>remove</Icon>
                    </Button>
                  </FormControl>
                </Grid>
              </Grid>
            ))
          }
          {
            limit !== professional_expirence.length && !disabled
              ?
            <Grid
              container
              justify="center"
              style={{
                padding: 16,
                paddingBottom: 16
              }}
            >
              <Button
                type="button"
                variant="fab"
                mini
                color="primary"
                aria-label="add"
                onClick={this.handleDialog('professional_expirence')}
              > 
                <Icon>add</Icon>
              </Button>
            </Grid>
              :
            ''
          }
        </Grid>
        <Dialog
          open={open}
          onClose={this.handleClose}
        >
          <DialogTitle id="responsive-dialog-title">
            {
              option === 'student'
                ?
              'Agregar Educacion'
                :
              'Agregar experiencia profesional'
            }
          </DialogTitle>
          <DialogContent className="dialog-content">
            {
              option === 'student'
                &&
              <Grid container>
                <Grid
                  item
                  xs={12}
                  sm={12}
                  md={6}
                  lg={6}
                  xl={6}
                  style={{
										paddingRight: 10
									}}
                >
                  <TextField
                    select
                    label="Fecha de Inicio"
                    fullWidth
                    margin="normal"
                    required={required}
                    disabled={disabled}
                    className={className}
                    value={object.start ? object.start : ''}
                    onChange={this.handleChange('start')}
                  >
                    {
                      this.YearStart().map((docs) => (
                        <MenuItem key={docs} value={docs}>
                          {docs}
                        </MenuItem>
                      ))
                    }
                  </TextField>
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={12}
                  md={6}
                  lg={6}
                  xl={6}
                  style={{
										paddingLeft: 10
									}}
                >
                  <TextField
                    select
                    label="Fecha de Culminacion"
                    fullWidth
                    margin="normal"
                    required={required}
                    disabled={disabled}
                    className={className}
                    value={object.end ? object.end : ''}
                    onChange={this.handleChange('end')}
                  >
                    {
                      this.YearEnd().map((docs) => (
                        <MenuItem key={docs} value={docs}>
                          {docs}
                        </MenuItem>
                      ))
                    }
                  </TextField>                  
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={12}
                  md={12}
                  lg={12}
                  xl={12}
                >
                  <TextField
                    label="Escuela ó Institución"
                    fullWidth
                    margin="normal"
                    required={required}
                    disabled={disabled}
                    className={className}
                    value={object.institute ? object.institute : ''}
                    onChange={this.handleChange('institute')}
                  />
                  <TextField
                    label="Especialidad ó Tema de estudio"
                    fullWidth
                    margin="normal"
                    required={required}
                    disabled={disabled}
                    className={className}
                    value={object.especial_team ? object.especial_team : ''}
                    onChange={this.handleChange('especial_team')}
                  />
                  <TextField
                    select
                    label="Titulo"
                    fullWidth
                    margin="normal"
                    required={required}
                    disabled={disabled}
                    className={className}
                    value={object.title ? object.title : ''}
                    onChange={this.handleChange('title')}
                  >
                    <MenuItem value="Licenciatura">
                      Licenciatura
                    </MenuItem>
                    <MenuItem value="Maestría">
                      Maestría
                    </MenuItem>
                    <MenuItem value="Doctorado">
                      Doctorado
                    </MenuItem>
                    <MenuItem value="Postdoctorado">
                      Postdoctorado
                    </MenuItem>
                    <MenuItem value="Otros">
                      Otros
                    </MenuItem>
                  </TextField>
                  <TextField
                    label="Description (Options)"
                    multiline
                    rows="4"
                    fullWidth
                    margin="normal"
                    className={className}
                    value={object.description ? object.description : ''}
                    onChange={this.handleChange('description')}
                  />
                  <UploadButton
                    value={object.file ? object.file.file : ''}
                    onChange={this.handleChange('file')}
                    labelButton="Subir image"
                    labelButtonSuccess="Ver image"
                    labelDialog="Subit image del documento"
                    style={
                      {
                        textAlign: 'center',
                        paddingTop: 50,
                        paddingBottom: 20
                      }
                    }
                  />                      
                </Grid>
              </Grid>
            }
            {
              option === 'professional_expirence'
                &&
              <Grid container>
                <Grid
                  item
                  xs={12}
                  sm={12}
                  md={6}
                  lg={6}
                  xl={6}
                  style={{
										paddingRight: 10
									}}
                >
                  <TextField
                    select
                    label="Fecha de Inicio"
                    fullWidth
                    margin="normal"
                    required={required}
                    disabled={disabled}
                    className={className}
                    value={object.start ? object.start : ''}
                    onChange={this.handleChange('start')}
                  >
                    {
                      this.YearStart().map((docs) => (
                        <MenuItem key={docs} value={docs}>
                          {docs}
                        </MenuItem>
                      ))
                    }
                  </TextField>
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={12}
                  md={6}
                  lg={6}
                  xl={6}
                  style={{
										paddingLeft: 10
									}}
                >
                  <TextField
                    select
                    label="Fecha de Culminacion"
                    fullWidth
                    margin="normal"
                    required={required}
                    disabled={disabled}
                    className={className}
                    value={object.end ? object.end : ''}
                    onChange={this.handleChange('end')}
                  >
                    {
                      this.YearEnd().map((docs) => (
                        <MenuItem key={docs} value={docs}>
                          {docs}
                        </MenuItem>
                      ))
                    }
                  </TextField>
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={12}
                  md={12}
                  lg={12}
                  xl={12}
                >
                  <TextField
                    label="Empresa"
                    margin="normal"
                    fullWidth
                    required={required}
                    disabled={disabled}
                    className={className}
                    value={object.enterprise ? object.enterprise : ''}
                    onChange={this.handleChange('enterprise')}
                  />
                  <TextField
                    label="Puesto"
                    margin="normal"
                    fullWidth
                    required={required}
                    disabled={disabled}
                    className={className}
                    value={object.position ? object.position : ''}
                    onChange={this.handleChange('position')}
                  />
                  <SelectLocalities
                    label="Pais"
                    value={object.country ? object.country : ''}
                    onChange={this.handleChange('country')}
                  />
                  <TextField
                    label="Ciudad"
                    margin="normal"
                    fullWidth
                    required={required}
                    disabled={disabled}
                    className={className}
                    value={object.city ? object.city : ''}
                    onChange={this.handleChange('city')}
                  />
                  <TextField
                    label="Description (Options)"
                    multiline
                    rows="4"
                    fullWidth
                    margin="normal"
                    className={className}
                    value={object.description ? object.description : ''}
                    onChange={this.handleChange('description')}
                  />
                  <UploadButton
                    value={object.file ? object.file.file : ''}
                    onChange={this.handleChange('file')}
                    labelButton="Subir image"
                    labelButtonSuccess="Ver image"
                    labelDialog="Subir image del documento"
                    style={
                      {
                        textAlign: 'center',
                        paddingTop: 50,
                        paddingBottom: 20
                      }
                    }
                  />     
                </Grid>
              </Grid>
            }
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose}>Cerrar</Button>
            <Button onClick={this.handleSave}>Agregar</Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default SummaryCv;