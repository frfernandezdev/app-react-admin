import React, { Component } from 'react';
import { FormControl, Typography } from '@material-ui/core';
import Moment from 'react-moment';
import 'moment/locale/es';

class DateNow extends Component {
	state = {}
	componentWillMount(){
		var now = new Date();
		this.setState({ timeNow: new Date( now.getTime() + (now.getTimezoneOffset()) * 60000) })
	}
	componentDidMount(){
		this.timer = setInterval(
			() => this.tick(),
			1000
		)
	}
	componentWillUnmount(){
		clearInterval(this.timer)
	}
	tick(){
		var now = new Date();
		this.setState({ timeNow: new Date( now.getTime() + (now.getTimezoneOffset()) * 60000) })
	}
	render(){
		const { timeNow } = this.state;
		const { timezone, watch } = this.props;
		return (
			<FormControl
				margin="normal"
				fullWidth
			>
				{/* <label>&nbsp;</label> */}
				<Typography
					align="center"
					variant="subheading"
					style={{ marginTop: 8 }}
				>
					{
						timezone
							?
							timezone > 0
								?
							<Moment 
								locale="es" 
								format={`DD MMMM YYYY ${watch === 1 ? 'HH' : 'h'}:mm:ss a`}
								add={{ hours : timezone.offset }}
								date={timeNow}
							/>
								:
							<Moment 
								locale="es" 
								format={`DD MMMM YYYY ${watch === 1 ? 'HH' : 'h'}:mm:ss a`}
								subtract={{ hours : -(timezone.offset) }}
								date={timeNow}
							/>
							:
						<Moment 
							locale="es" 
							format={`DD MMMM YYYY ${watch === 1 ? 'HH' : 'h'}:mm:ss a`}
							date={timeNow}
						/>
					}
					&nbsp;
					{
						timezone
							?
						timezone.abbr
							:
						'UTC'
					}
				</Typography>
			</FormControl>
		)
	}
}

export default DateNow;