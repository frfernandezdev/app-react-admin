import React,{ Component } from 'react';
import axios from 'axios';
import {
  Dialog,
  DialogTitle,
  Icon,
  Button,
  DialogActions,
  Slide,
  CircularProgress
} from '@material-ui/core';
import green from '@material-ui/core/colors/green';

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class DialogDelete extends Component {
  state = { open: false, disabled: false }
  constructor(props){
    super(props);

    this.handleDelete = this.handleDelete.bind(this)
    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);

    this.props.handleOpen(this.handleOpen)
  }
  handleOpen = (event,uid) => {
    this.setState({ open: true, uid, event })
  }
  handleClose = () => {
    if(!this.state.disabled)
      this.setState({ open: false, uid: null })
  }
  handleDelete(){
    this.setState({ disabled: true })
    axios.delete(`http://localhost:5000/admin-speakenglishsite/us-central1/api/${this.props.url}/${this.state.uid}`,{ data: { token: this.props.user.auth.token } })
      .then(response => {
        if(response.data.success === true){
          this.setState({ disabled: false })
          this.props.handleSnackbar({type: 'custom', message: response.data.mes})
          this.handleClose()
        }else {
          this.setState({ disabled: false })
          if(response.data.error.code)
            return this.props.handleSnackbar({ type: 'auth', message: response.data.error.code, on: true })
          return this.props.handleSnackbar({ type: 'custom', message: response.data.error, on: true })
        }
      })
      .catch(error => {
        console.log(error.response)
        if(error.response.data && error.response.data.error)
				  return this.props.handleSnackbar({ type: 'auth', message: error.response.data.error.code, on: true })
        return this.props.handleSnackbar({ type: 'custom', message: '404', on: true })
      })
  }
  render(){
    const { label } = this.props;
    const { disabled, event } = this.state;
    return (
      <div>
        <Dialog 
          open={this.state.open}
          TransitionComponent={Transition}
          onClose={this.handleClose}
        >
          <DialogTitle>Desea Eliminar {label}: {event}</DialogTitle>
          <DialogActions style={{ justifyContent: 'center', borderBottom: '1px solid #e1e1e1' }}>
            <div style={{ position: 'relative', margin: 8 }}>
              <Button
                disabled={disabled}
                onClick={this.handleDelete}
              >
                Eliminar
              </Button>
              {
                disabled 
                  && 
                <CircularProgress 
                  size={24} 
                  style={{
                    color: green[500],
                    position: 'absolute',
                    top: '50%',
                    left: '50%',
                    marginTop: -12,
                    marginLeft: -12,
                  }} 
                />
              }
            </div>
            <Button 
              onClick={this.handleClose}
              disabled={disabled}
            >
              Cancelar
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}

export default DialogDelete;