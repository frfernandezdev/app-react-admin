import React, { Component } from 'react';
import {
	TableHead,
	TableRow,
	TableCell,
	TableSortLabel, 
	Tooltip 
} from '@material-ui/core'; 

class Head extends Component {
	state = {}
	handleClick = event => {
		this.props.handleClick && this.props.handleClick(event);
	}
	render(){
		const { collection, orderBy, order} = this.props;
		return (
			<TableHead>
				<TableRow>
					{
						collection.map((docs, i) => (
							<TableCell 
								key={i}
								style={{
									paddingLeft: '20px',
									paddingRight: '20px'
								}}
								sortDirection={
									docs.disabled === true
										?
									false
										:
									orderBy === docs.ref 
										? 
									order 
										: 
									false
								}
							>
								{
									docs.tooltip 
										?
									<Tooltip
										title={docs.title}
										placement={docs.position}
										enterDelay={docs.delay}
									>
										<TableSortLabel
											active={orderBy === docs.ref}
											direction={order}
											onClick={
												event => 
													this.props.handleClick 
														&& 
													this.props.handleClick(docs)
											}
											disabled={docs.disabled}
										>
											{docs.title}
										</TableSortLabel>
									</Tooltip>
										:
									docs.title
								}
							</TableCell>
						))
					}
					<TableCell>Acciones</TableCell>
				</TableRow>
			</TableHead>
		)
	}
}

export default Head;