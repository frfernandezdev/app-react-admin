import React, { Component } from 'react';
import { 
	Table,
	TableFooter 
} from '@material-ui/core';
import Head from './head';
import Body from './body';
import Pagination from './pagination';

class TableCustom extends Component {
	constructor(props) {
		super()
	}
	render(){
		const { 
			headCollection, 
			bodyCollection, 
			footer, 
			template, 
			style, 
			handleEdit, 
			handleDelete, 
			handleView,
			handleClickHead,
			order,
			orderBy,
			page,
			rowsPerPage,
			onChangePage,
			onChangeRowsPerPage,
			count,
			options
		} = this.props;
		return (
			<div>
				<Table style={style}>
					<Head 
						collection={headCollection} 
						template={template}
						order={order}
						orderBy={orderBy}
						handleClick={handleClickHead}

					/>
					<Body 
						collection={bodyCollection} 
						template={template} 
						handleEdit={handleEdit} 
						handleDelete={handleDelete} 
						handleView={handleView}
						options={options}
						/>
					{
						footer
							? 
						<TableFooter>{footer}</TableFooter>
							:
						null
					}					
				</Table>
				<Pagination
					count={count ? count : 0}
					page={page ? page : 0}
					rowsPerPage={rowsPerPage ? rowsPerPage : 0}
					onChangePage={onChangePage ? onChangePage : () => {}}
					onChangeRowsPerPage={onChangeRowsPerPage ? onChangeRowsPerPage : () => {}}
				/>
			</div>
		)
	}
}

export default TableCustom;