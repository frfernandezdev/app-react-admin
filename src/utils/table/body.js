import React, { Component } from 'react';
import {
	Avatar,
	Button,
	Icon,
	TableBody,
	TableRow,
	TableCell,
} from '@material-ui/core';
import green from '@material-ui/core/colors/green';
import moment from 'moment';

const Users = (props) => {
	const { 
		docs, 
		collection, 
		i, 
		handleDelete, 
		handleEdit, 
		handleView 
	} = props;
	return (
		<TableRow
			hover
			style={{
				cursor: 'pointer',
			}}
		>
			<TableCell 
				numeric
				onClick={event => handleView(event, docs.id)}
				style={{
					paddingLeft: '20px',
					paddingRight: '20px'
				}}
			>
				{i+1}
			</TableCell>
			<TableCell 
				padding="dense"
				onClick={event => handleView(event, docs.id)}
			>
				{
					collection.photo
						?
					<Avatar src={collection.photo} alt={collection.username}/>
						:
					<Icon style={{ fontSize: 40 }}>account_circle</Icon>
				}
			</TableCell>
			<TableCell
				onClick={event => handleView(event, docs.id)}
				style={{
					paddingLeft: '20px',
					paddingRight: '20px'
				}}
			>
				{collection.username}
			</TableCell>
			<TableCell 
				onClick={event => handleView(event, docs.id)}
				padding="none" style={{
					paddingLeft: '20px',
					paddingRight: '20px'
				}}
			>
				{collection.fullname}
			</TableCell>
			<TableCell
				onClick={event => handleView(event, docs.id)}
				style={{
					paddingLeft: '20px',
					paddingRight: '20px'
				}}
			>
				{collection.email}
			</TableCell>
			<TableCell 
				onClick={event => handleView(event, docs.id)}
				padding="none" 
				style={{
					paddingLeft: '20px',
					paddingRight: '20px'
				}}
			>
				{
					collection.priv === 1 ? 'Administrador'
						:
					collection.priv === 2 ? 'Supervisor'
						:
					collection.priv === 3 ? 'Usuario'
						:
					null
				}
			</TableCell>
			<TableCell padding="none">
				<Button 
					variant="raised" 
					size="small" 
					color="primary" 
					style={{marginLeft: 5, marginRight: 5}}
					onClick={event => handleEdit(event, 2, docs.id)}
				>
					<Icon>edit</Icon>
				</Button>
				<Button 
					variant="raised" 
					size="small" 
					color="secondary" 
					style={{marginLeft: 5, marginRight: 5}}
					onClick={event => handleDelete(collection.username, docs.id)}
				>
					<Icon>delete_forever</Icon>
				</Button>
			</TableCell>
		</TableRow>
	)
}
const LanguagesCurrentMoney = (props) => {
	const { 
		docs, 
		collection, 
		i, 
		handleDelete, 
		handleEdit, 
		handleView 
	} = props;
	return (
		<TableRow
			hover
			style={{
				cursor: 'pointer',
			}}
		>
			<TableCell 
				numeric
				onClick={event => handleView(event, docs.id)}
				style={{
					paddingLeft: '20px',
					paddingRight: '20px'
				}}
			>
				{i+1}
			</TableCell>
			<TableCell
				onClick={event => handleView(event, docs.id)}
				style={{
					paddingLeft: '20px',
					paddingRight: '20px'
				}}
			>
				{collection.name}
			</TableCell>
			<TableCell 
				onClick={event => handleView(event, docs.id)}
				padding="none" style={{
					paddingLeft: '20px',
					paddingRight: '20px'
				}}
			>
				{collection.acronym}
			</TableCell>
			<TableCell padding="none">
				<Button 
					variant="raised" 
					size="small" 
					color="primary" 
					style={{marginLeft: 5, marginRight: 5}}
					onClick={event => handleEdit(event, 2, docs.id)}
				>
					<Icon>edit</Icon>
				</Button>
				<Button 
					variant="raised" 
					size="small" 
					color="secondary" 
					style={{marginLeft: 5, marginRight: 5}}
					onClick={event => handleDelete(collection.name, docs.id)}
				>
					<Icon>delete_forever</Icon>
				</Button>
			</TableCell>
		</TableRow>
	)
}
const Applications = (props) => {
	const { 
		docs, 
		collection, 
		i, 
		handleDelete, 
		handleEdit, 
		handleView 
	} = props;
	return (
		<TableRow
			hover
			style={{
				cursor: 'pointer',
			}}
		>
			<TableCell 
				numeric
				onClick={event => handleView(event, collection.id)}
				style={{
					paddingLeft: '20px',
					paddingRight: '20px'
				}}
			>
				{i+1}
			</TableCell>
			<TableCell 
				padding="dense"
				onClick={event => handleView(event, collection.id)}
			>
				{
					collection.user.photo
						?
					<Avatar src={collection.user.photo} alt={collection.user.username}/>
						:
					<Icon style={{ fontSize: 40 }}>account_circle</Icon>
				}
			</TableCell>
			<TableCell
				onClick={event => handleView(event, collection.id)}
				style={{
					paddingLeft: '20px',
					paddingRight: '20px'
				}}
			>
				{collection.user.username}
			</TableCell>
			<TableCell 
				onClick={event => handleView(event, collection.id)}
				padding="none" style={{
					paddingLeft: '20px',
					paddingRight: '20px'
				}}
			>
				{collection.user.fullname}
			</TableCell>
			<TableCell
				onClick={event => handleView(event, collection.id)}
				style={{
					paddingLeft: '20px',
					paddingRight: '20px'
				}}
			>
				{collection.user.email}
			</TableCell>
			<TableCell
				onClick={event => handleView(event, collection.id)}
				style={{
					paddingLeft: '20px',
					paddingRight: '20px'
				}}
			>
				{moment(collection.application_date).format('YYYY-MM-DD')}
			</TableCell>
			<TableCell 
				onClick={event => handleView(event, collection.id)}
				padding="none" 
				style={{
					paddingLeft: '20px',
					paddingRight: '20px'
				}}
			>
				{
					collection.status === 1 ? 'Evaluando'
						:
					collection.status === 2 ? 'Aceptado'
						:
					collection.status === 3 ? 'Denegado'
						:
					null
				}
			</TableCell>
			<TableCell padding="none">
				<Button 
					variant="raised" 
					size="small" 
					color="primary" 
					
					style={
						{
							marginLeft: 5, 
							marginRight: 5,
							color: 'white',
							backgroundColor: green[500]
						}
					}
					onClick={event => handleEdit(collection.id)}
				>
					<Icon>done_all</Icon>
				</Button>
				<Button 
					variant="raised" 
					size="small" 
					color="secondary" 
					style={{marginLeft: 5, marginRight: 5}}
					onClick={event => handleDelete(collection.id)}
				>
					<Icon>delete_forever</Icon> 
				</Button>
			</TableCell>
		</TableRow>
	)
}
const Teachers = (props) => {
	const { 
		docs, 
		collection, 
		i, 
		handleDelete, 
		handleEdit, 
		handleView 
	} = props;
	return (
		<TableRow
			hover
			style={{
				cursor: 'pointer',
			}}
		>
			<TableCell 
				numeric
				onClick={event => handleView(event, collection.id)}
				style={{
					paddingLeft: '20px',
					paddingRight: '20px'
				}}
			>
				{i+1}
			</TableCell>
			<TableCell 
				padding="dense"
				onClick={event => handleView(event, collection.id)}
			>
				{
					collection.user.photo
						?
					<Avatar src={collection.user.photo} alt={collection.user.username}/>
						:
					<Icon style={{ fontSize: 40 }}>account_circle</Icon>
				}
			</TableCell>
			<TableCell
				onClick={event => handleView(event, collection.id)}
				style={{
					paddingLeft: '20px',
					paddingRight: '20px'
				}}
			>
				{collection.user.username}
			</TableCell>
			<TableCell 
				onClick={event => handleView(event, collection.id)}
				padding="none" style={{
					paddingLeft: '20px',
					paddingRight: '20px'
				}}
			>
				{collection.user.fullname}
			</TableCell>
			<TableCell
				onClick={event => handleView(event, collection.id)}
				style={{
					paddingLeft: '20px',
					paddingRight: '20px'
				}}
			>
				{collection.user.email}
			</TableCell>
			<TableCell
				onClick={event => handleView(event, collection.id)}
				style={{
					paddingLeft: '20px',
					paddingRight: '20px'
				}}
			>
				{moment(collection.application_date).format('YYYY-MM-DD')}
			</TableCell>
			<TableCell 
				onClick={event => handleView(event, collection.id)}
				padding="none" 
				style={{
					paddingLeft: '20px',
					paddingRight: '20px'
				}}
			>
				{
					collection.status === 1 ? 'Evaluando'
						:
					collection.status === 2 ? 'Aceptado'
						:
					collection.status === 3 ? 'Denegado'
						:
					null
				}
			</TableCell>
			<TableCell padding="none">
				<Button 
					variant="raised" 
					size="small" 
					color="primary" 
					style={{marginLeft: 5, marginRight: 5}}
					onClick={event => handleEdit(event, 2, collection.id)}
				>
					<Icon>edit</Icon>
				</Button>
				<Button 
					variant="raised" 
					size="small" 
					color="secondary" 
					style={{marginLeft: 5, marginRight: 5}}
					onClick={event => handleDelete(collection.user.username, collection.id)}
				>
					<Icon>delete_forever</Icon>
				</Button>
			</TableCell>
		</TableRow>
	)
}
class Body extends Component {
	state = {}
	render(){
		const { 
			collection, 
			template, 
			handleEdit, 
			handleDelete, 
			handleView,
			options 
		} = this.props;
		return (
			<TableBody>
				{
					!options && collection.length > 0
						?
					collection.map((docs, i) => {
						let collection = docs.data();
						switch(template){
							case 'users':
								return 	<Users 
													key={i} 
													docs={docs} 
													collection={collection} 
													i={i} 
													handleEdit={handleEdit} 
													handleDelete={handleDelete} 
													handleView={handleView}
												/>
							case 'languagesCurrentMoney':
								return 	<LanguagesCurrentMoney
													key={i} 
													docs={docs} 
													collection={collection} 
													i={i} 
													handleEdit={handleEdit} 
													handleDelete={handleDelete} 
													handleView={handleView}
											 	/>
						}
					})
					:
					null
				}
				{
					options
						&&
					collection.length > 0
						?
					collection.map((docs, i) => {
						switch(template){
							case 'applications':
								return 	<Applications 
													key={i}
													collection={docs} 
													i={i} 
													handleEdit={handleEdit} 
													handleDelete={handleDelete} 
													handleView={handleView}
												/>
							case 'teachers':
								return  <Teachers
													key={i}
													collection={docs} 
													i={i} 
													handleEdit={handleEdit} 
													handleDelete={handleDelete} 
													handleView={handleView}
												/>
						}
					})
						:
					null
				}
				{
					!collection.length > 0
						&&
					<TableRow>
						<TableCell>No se ha encontrado </TableCell>
					</TableRow>
				}
			</TableBody>
		)
	}
}

export default Body;