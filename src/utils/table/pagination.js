import React, { Component } from 'react';
import { TablePagination } from '@material-ui/core';

class Pagination extends Component {
	state = {}
	render(){
		const { count, rowsPerPage, page, onChangePage, onChangeRowsPerPage } = this.props;
		return (
			<TablePagination
				component="div"
				count={count}
				rowsPerPage={rowsPerPage}
				page={page}
				backIconButtonProps={{
					'aria-label': 'Anterior',
				}}
				nextIconButtonProps={{
					'aria-label': 'Siguiente',
				}}
				onChangePage={onChangePage}
				onChangeRowsPerPage={onChangeRowsPerPage}
				labelRowsPerPage="Items por pagina"
				labelDisplayedRows={({from, to, count}) => `${from}-${to} de ${count}`}
			/>
		)
	}
}

export default Pagination;