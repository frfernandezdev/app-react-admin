import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
	FormControl,
	Grid,
	TextField,
	MenuItem,
	Checkbox,
	Radio,
	Icon,
	Button,
	FormControlLabel
} from '@material-ui/core';

class SelectLanguageToLearn extends Component {
	state = {
    object: [],
    objectRemove: [],
    copyRows: [],
		show: 0,
    language: [
      {
        language: '',
        level: '',
      }
    ]
	}
	constructor(props){
		super()

		this.firestore = props.firebase.firestore().collection('language');
	}
	getCurrent = () => {
		this.setState({
			object: [],
			objectRemove: [],
			copyRows: [],
			show: 0,
			language: [
				{
					language: '',
					level: '',
				}
			]
		})
		this.firestore
			.orderBy('name')
			.onSnapshot(snapshot => {
				this.state.object[0] = snapshot.docs;
				this.state.objectRemove = snapshot.docs;
				this.state.show = snapshot.docs.length;
				this.setState(this.state)
			}, error => {
				console.log(error)
			})
	}
	componentWillMount(){
		this.getCurrent();
	}
	componentWillUpdate(prevProps, prevState, snapshot){
		if('delete' in prevProps){
			if(prevProps.delete === true && prevState.language[0].language !== ''){
				this.deleteForm();
			}
		}
		if('value' in prevProps){
			// if(prevProps.value !== "" && prevState.language[0].language === '' && prevState.object[0] !== undefined){
			// 	this.valueForm(prevProps.value);
			// }
			if(prevProps.value !== "" && prevState.language[0].language !== prevProps.value[0].language && prevState.object[0] !== undefined){
				console.log(prevProps.value)
				this.valueForm(prevProps.value);
			}
		}
	}
	deleteForm = event => {
		this.state.language = [
			{
        language: '',
        level: '',
      }
		]
	}
	valueForm = event => {
		if(this.state.language[0].language !== '')
			this.getCurrent();
		for(const k in event){
			if(parseInt(k) !== 0){
				let object = [];
				this.state.object[parseInt(k) - 1].map((element, index) => {
					if(element.id !== event[parseInt(k) - 1].language){
						object.push(element)
					}
				})
				this.state.object[parseInt(k)] = object;
			}
		}
		this.state.language = event;
		this.setState(this.state, () => this.handleCopyRows())
	}
	handleCopyRows = () => {
		for(const k in this.state.language){
			if(parseInt(k) !== 0){
				this.state.object[parseInt(k) - 1].map((element, index) => {
					if(element.id === this.state.language[parseInt(k)].language){
						this.state.object[parseInt(k) - 1].splice(index, 1)
					}
				})
			}
		}
	}
	handleChange = () => {
		let object = {
			target: { value: this.state.language }
		}
		this.props.onChange(object)
  }
  handleLoop = event => {
    if(this.state.language[this.state.language.length - 1].language !== "" || this.state.language[this.state.language.length - 1].language !== null){
      let object = [...this.state.object[this.state.object.length - 1]]
      this.state.object[this.state.object.length - 1].map((element ,index) => {
        if(this.state.language[this.state.language.length - 1].language === element.id)
					object.splice(index, 1)
      })
      // object.length === 1 ? this.state.show = false : false;
      this.state.object.push(object)
    }else 
      this.state.object.push(this.state.object[0])
    this.state.language.push({
      language: '',
      level: '',
    })
    this.setState(this.state)
    this.handleChange()
  }
  loop = (a, i, event) => {
    return new Promise((resolve, reject) => {
      if(this.state.language[i].language !== '' && this.state.language[i].language !== null && this.state.language[i].language !== event.target.value){
        this.state.object[a].push(
					this.state.objectRemove.find(
						el => 
							el.id === this.state.language[i].language
						)
				)
				resolve(a, i, event)
      }else 
				resolve(a, i, event)
			this.handleChange()
    })
	}
	PriorityOnChange = index => (event, checked) => {
		let object = this.state.language.map((el, i) => {
			if(el.priority === true)
				this.state.language[i].priority = false
			if(index === i)
				el.priority = true
			return el
		})
		if(!object.length > 0)
			this.state.language = [...object]
		else 
			this.state.language = object
		this.setState(this.state)
		this.handleChange()
	}
  FindRemove = index => {
    return new Promise((resolve, reject) => {
      this.state.objectRemove.find(el => {
        if(el.id === this.state.language[index].language)
          for(let i = 0; i < this.state.object.length; i++) {
            this.state.object[i].push(el)
          }
          resolve(index)
      })
    })
  }
  handleRemoveLoop = index => event => {
    this.FindRemove(index)
    .then(index => {
    	this.state.object.splice(index, 1)
      this.state.language.splice(index, 1)
      this.setState(this.state)
		})
		this.handleChange()
	}
	render() {
		const { required, disabled, className } = this.props;
		return (
      <div>
        {
          this.state.language.map((el ,i) => (
						<Grid 
							key={i}  
							container
							>
								<Grid
									item
									xs={12}
									sm={12}
									md={6}
									lg={6}
									xl={6}
									style={{
										paddingLeft: 10,
										paddingRight: 10
									}}
								>
									<TextField
										select
										label="Languaje"
										required={required}
										disabled={disabled}
										className={className}
										value={this.state.language[i].language ? this.state.language[i].language : ''}
										onChange={event => {
											for (let a = 0; a < this.state.object.length; a++) {
												if(a !== (i)){
													this.loop(a, i, event)
													.then(() => {
														for (let e = 0; e < this.state.object[a].length; e++) {
															if(this.state.object[a][e].id === event.target.value){
																this.state.object[a].splice(e, 1)
															}
														}
														this.state.language[i].language = event.target.value;
														this.setState(this.state, () => this.handleChange())
													})
												}else if(this.state.language.length <= 1){
													this.state.language[i].language = event.target.value;
													this.setState(this.state, () => this.handleChange())
												}
											}
										}}
										fullWidth
										margin="normal"
									>
										{
											this.state.object.length > 0
												?
											this.state.object[i].map((docs, i) => {
												let collection = docs.data();
												return (
													<MenuItem key={docs.id} value={docs.id}>
														{collection.name}								
													</MenuItem>
												)
											})
												:
											<MenuItem value="">No hay Lenguajes</MenuItem>
										}
									</TextField>
								</Grid>
								<Grid
									item
									xs={12}
									sm={12}
									md={i > 0 ? 5 : 6}
									lg={i > 0 ? 5 : 6}
									xl={i > 0 ? 5 : 6}
									style={{
										paddingLeft: 10,
										paddingRight: 10
									}}
								>
									<TextField
										select
										label="Nivel"
										required={required}
										disabled={disabled}
										className={className}
										value={this.state.language[i].level ? this.state.language[i].level : ''}
										onChange={event => {
											this.state.language[i].level = event.target.value;
											this.setState(this.state, () => this.handleChange())
										}}
										fullWidth
										margin="normal"
									>
										<MenuItem value={1}>
											Principiante
										</MenuItem>
										<MenuItem value={2}>
											Elemental
										</MenuItem>
										<MenuItem value={3}>
											Intermedio
										</MenuItem>
										<MenuItem value={4}>
											Intermedio Avanzado
										</MenuItem>
										<MenuItem value={5}>
											Avanzado
										</MenuItem>
										<MenuItem value={6}>
											Nativo
										</MenuItem>
									</TextField>
								</Grid>
								{
									!disabled && i > 0
										&&
									<Grid
										item
										xs={12}
										sm={12}
										md={1}
										lg={1}
										xl={1}
									>
										<FormControl
											fullWidth
											margin="normal"
											style={{
												alignItems: 'center',
												marginTop: 26
											}}
											required={required}
											disabled={disabled}
											className={className}
										>
											<Button 
												type="button"
												variant="fab" 
												mini 
												color="secondary" 
												aria-label="remove"
												onClick={this.handleRemoveLoop(i)}
											>
												<Icon>remove</Icon>
											</Button>
										</FormControl>
									</Grid>
								}
							</Grid>
						)
          )
        }
        {
          this.state.show !== this.state.language.length && !disabled
           ?
          <Grid 
            container
            justify="center"
            style={{
              paddingTop: 16,
              paddingBottom: 16
            }}
          >
						<Button 
							type="button"
              variant="fab" 
              mini 
              color="primary" 
              aria-label="add"
              onClick={this.handleLoop}
              >
              <Icon>add</Icon>
            </Button>
          </Grid>
            :
          ''
        }
      </div>
    )
	}
}

SelectLanguageToLearn.propTypes = {
	limit: PropTypes.number,
	onChange: PropTypes.func
}

export default SelectLanguageToLearn;