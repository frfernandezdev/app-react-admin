import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
	Grid,
	TextField,
	MenuItem,
	Icon,
	Button,
	FormControl
} from '@material-ui/core'

class SelectLanguageNative extends Component {
	state = {
    object: [],
    objectRemove: [],
    copyRows: [],
    show: true,
    language: [
      {
        language: '',
      }
    ]
	}
	constructor(props){
		super()

		this.firestore = props.firebase.firestore().collection('language');
	}
	componentWillMount(){
		this.firestore
			.orderBy('name')
			.onSnapshot(snapshot => {
        this.state.object[0] = snapshot.docs;
        this.state.objectRemove = snapshot.docs;
				// if(!snapshot.docs.length > 0)
        //   this.state.show = false;
        this.setState(this.state)
			}, error => {
				console.log(error)
      })
	}
	componentWillUpdate(prevProps, prevState, snapshot){
		if('delete' in prevProps){
			if(prevProps.delete === true && prevState.language[0].language !== ''){
				this.deleteForm();
			}
		}
		if('value' in prevProps){
			if(prevProps.value !== "" && prevState.language[0].language === '' && this.state.object[0] !== undefined){
				this.valueForm(prevProps.value);
			}
		}
	}
	deleteForm = event => {
		this.state.language = [
			{ language: '' }
		]
	}
	valueForm = (event) => {
		for(const k in event){
			if(parseInt(k) !== 0){
				let objects = [];
				this.state.object[parseInt(k) - 1].map((element, index) => {
					if(element.id !== event[parseInt(k) - 1].language){
						objects.push(element)
					}
				})
				this.state.object[parseInt(k)] = objects;
			}
		}
		this.state.language = event;
		this.setState(this.state, () => this.handleCopyRows())
	}
	handleCopyRows = () => {
		for(const k in this.state.language){
			if(parseInt(k) !== 0){
				this.state.object[parseInt(k) - 1].map((element, index) => {
					if(element.id === this.state.language[parseInt(k)].language){
						this.state.object[parseInt(k) - 1].splice(index, 1)
					}
				})
			}
		}
	}
	handleChange = () => {
		let object = {
			target: { value: this.state.language }
		}
		this.props.onChange(object)
	}
	handleLoop = event => {
    if(this.state.language[this.state.language.length - 1].language !== "" || this.state.language[this.state.language.length - 1].language !== null){
      let object = [...this.state.object[this.state.object.length - 1]]
      this.state.object[this.state.object.length - 1].map((element ,index) => {
        if(this.state.language[this.state.language.length - 1].language === element.id)
          object.splice(index, 1)
      })
      object.length === 1 ? this.state.show = false : false;
      this.state.object.push(object)
    }else 
      this.state.object.push(this.state.object[0])
    this.state.language.push({ language: '' })
    this.setState(this.state, () => this.handleChange())
	}
	// a position the array this.state.object 
	// i position the array this.state.language
	loop = (a, i, event) => {
		return new Promise((resolve, reject) => {
      if(this.state.language[i].language !== '' && this.state.language[i].language !== null && this.state.language[i].language !== event.target.value){
				this.state.object[a].push(
					this.state.objectRemove.find(
						el => 
							el.id === this.state.language[i].language
						)
				)
				resolve(a, i, event)
      }else 
				resolve(a, i, event)
    })
	}
	FindRemove = index => {
    return new Promise((resolve, reject) => {
      this.state.objectRemove.find(el => {
        if(el.id === this.state.language[index].language)
          for (let i = 0; i < this.state.object.length; i++) {
            this.state.object[i].push(el)
          }
          resolve(index)
      })
    })
	}
	handleRemoveLoop = index => {
    this.FindRemove(index)
    .then(index => {
    	this.state.object.splice(index, 1)
      this.state.language.splice(index, 1)
      this.setState(this.state, () => this.handleChange())
		})
	}
	render() {
		const { limit, disabled, required, className } = this. props;
		return (
			<div>
				<Grid container>
					{
						this.state.language.map((el, i) => (
							<Grid 
								key={i}
								item
								xs={6}
								sm={6}
								md={6}
								lg={6}
								xl={6}
								style={{
									paddingLeft: 10,
									paddingRight: 10
								}}
							>
								<Grid
									container
								>
									<Grid
										item
										xs={i > 0 ? 11 : 12}
										sm={i > 0 ? 11 : 12}
										md={i > 0 ? 11 : 12}
										lg={i > 0 ? 11 : 12}
										xl={i > 0 ? 11 : 12}
									>
										<TextField
											select
											label="Languaje Nativo"
											required={required}
											disabled={disabled}
											className={className}
											value={this.state.language[i].language ? this.state.language[i].language : ''}
											onChange={event => {
												for(let a = 0; a < this.state.object.length; a++) {
													if(a !== i){
														this.loop(a, i, event)
														.then(() => {
															for(let e = 0; e < this.state.object[a].length; e++) {
																if(this.state.object[a][e].id === event.target.value){
																	this.state.object[a].splice(e, 1)
																}
															}
															this.state.language[i].language = event.target.value;
															this.setState(this.state, () => this.handleChange())
														})
													}else if(this.state.language.length <= 1){
														this.state.language[i].language = event.target.value;
														this.setState(this.state, () => this.handleChange())
													}
												}
											}}
											fullWidth
											margin="normal"
										>
											{
												this.state.object.length > 0
													?
												this.state.object[i].map((docs, i) => {
													let collection = docs.data();
													return (
														<MenuItem key={docs.id} value={docs.id}>
															{collection.name}								
														</MenuItem>
													)
												})
													:
												<MenuItem value="">No hay Lenguajes</MenuItem>
											}
										</TextField>
									</Grid>
									{
										i > 0 && !disabled
											? 
										<Grid
											item
											xs={2}
											sm={2}
											md={1}
											lg={1}
											xl={1}
										>
											<FormControl
												fullWidth
												margin="normal"
												style={{
													alignItems: 'center',
													marginTop: 26
												}}
											>	
												<Button 
													type="button"
													variant="fab" 
													mini 
													color="secondary" 
													aria-label="remove"
													onClick={event => this.handleRemoveLoop(i)}
												>
													<Icon>remove</Icon>
												</Button>
											</FormControl>
										</Grid>
										:
										''
									}
								</Grid>
							</Grid>
						))
					}
					{
						this.state.show && this.state.language.length < limit && !disabled
							?
						<Grid 
							container
							justify="center"
							style={{
								paddingTop: 16,
								paddingBottom: 16
							}}
						>
							<Button 
								type="button"
								variant="fab" 
								mini 
								color="primary" 
								aria-label="add"
								onClick={this.handleLoop}
								>
								<Icon>add</Icon>
							</Button>
						</Grid>
							:
						''
					}
				</Grid>
			</div>
		)
	}
}

SelectLanguageNative.propTypes = {
	limit: PropTypes.number,
	onChange: PropTypes.func
}

export default SelectLanguageNative;