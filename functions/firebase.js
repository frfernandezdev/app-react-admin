const functions = require('firebase-functions'),
			admin 		= require('firebase-admin');

const serviceAccount = require('./admin-speakenglishsite-firebase-adminsdk-5202y-2c7992e22e.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://admin-speakenglishsite.firebaseio.com"
});

const db  		= admin.firestore();
const storage = admin.storage();
const auth    = admin.auth();
const message = admin.messaging();

module.exports = {
	db,
	storage,
	auth,
	message,
	functions,
	admin
}