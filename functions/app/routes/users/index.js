const { auth, storage }					 = require('../../../firebase'),
				express 								 = require('express'),
				router  								 = express.Router(),
			{ Users }   							 = require('../../objects'),
			{ MultipartGcs }           = require('../../middleware'),
			{ deleteFile, uploadFile } = require('../../utils');

const multipartGcs = MultipartGcs({});

const User = new Users();

router.route('/')
			.post(multipartGcs.array(),(req, res, next) => {
				var body = JSON.parse(req.body.data),
						user = req.user,
						file = req.files['photo'] ? req.files['photo'][0]: false;
				
				if(!user)
					return res.json({ success: false, error: 'not-authentication' })
				if(user.priv !== 1)
					return res.json({ success: false, error: 'not-priv' })
	
				if(!body.email && typeof body.email !== 'string')
					return res.json({ success: false, error: "No enviaste correo electronico ó enviaste un parametro invalido" })
				if(!body.password && typeof body.password !== 'string')
					return res.json({ success: false, error: "No enviaste una contraseña ó enviaste un parametro invalido" })
				if(!body.fullname && typeof body.fullname !== 'string')
					return res.json({ success: false, error: "No enviaste un nombre completo ó enviaste un parametro invalido" })
				if(!body.username && typeof body.username !== 'string')
					return res.json({ success: false, error: "No enviaste un nombre de usuario ó enviaste un parametro invalido" })
				if(!body.phone && typeof body.phone !== 'string')
					return res.json({ success: false, error: "No enviaste Phone ó enviaste un parametro invalido" })
				if(!file)
					return res.json({ success: false, error: 'No enviaste tu foto de perfil ó enviaste un parametro invalido' })
				if(!body.watch && !Number.isInteger(body.watch))
					return res.json({success: false, error: "No enviaste tipo de hora ó enviaste un parametro invalido"})
				if(!body.timezone && typeof body.timezone !== 'object')
					return res.json({success: false, error: "No enviaste una zona horaria ó enviaste un parametro invalido"})
				if(parseInt(body.priv) > 2 && !body.bornLocalities && !Array.isArray(body.bornLocalities))
					return res.json({success: false, error: "No enviaste una localidad de nacimiento ó enviaste un parametro invalido"})
				if(parseInt(body.priv) > 2 && !body.liveLocalities && !Array.isArray(body.liveLocalities))
					return res.json({success: false, error: "No enviaste una localidad donde vives ó enviaste un parametro invalido"})
				if(parseInt(body.priv) > 2 && !body.languageNative && !Array.isArray(body.languageNative))
					return res.json({success: false, error: "No enviaste un languaje nativo ó enviaste un parametro invalido"})
				if(parseInt(body.priv) > 2 && !body.language && typeof body.language !== 'object')
					return res.json({success: false, error: "No enviaste los lenguajes ó enviaste un parametro invalido"})
				if(!body.priv && !Number.isInteger(body.priv))
					return res.json({success: false, error: "No enviaste el privilegio ó enviaste un parametro invalido"})

				var search = (body.username+body.fullname).toLowerCase().replace(/\s/g, ''),
						uid = null,
						object = {
							email: body.email,
							passoword: body.password,
							username: body.username,
							fullname: body.fullname,
							search: search,
							gender: body.gender,
							birthdate: body.birthdate,
							phone: body.phone,
							mobile: body.mobile ? body.mobile : null,
							address: body.address ? body.address : null,
							online: false,
							status: 1,
							priv: body.priv ? body.priv : 3,
							teacher: false,
							state: true,
							photo: null,
							photoRef: null,
							// cover: null,
							company: body.company ? body.company : null,
							watch: body.watch ? body.watch : null, // true is 24 Hrs 
							money: body.money ? body.money : null,
							tools: {
								skype: body.tools ? body.tools.skype ? body.tools.skype : '' : '',
								facetime: body.tools ? body.tools.facetime ? body.tools.facetime : '' : '',
								gHangouts: body.tools ? body.tools.gHangouts ? body.tools.gHangouts : '' :'',
								qq: body.tools ? body.tools.qq ? body.tools.qq : '' : '',
								wechat: body.tools ? body.tools.wechat ? body.tools.wechat : '' : ''
							},
							website: body.website ? body.website : null,
							liveLocalities: body.liveLocalities ? body.liveLocalities : [],
							bornLocalities: body.bornLocalities ? body.bornLocalities : [],
							timezone: body.timezone ? body.timezone : {},
							languageNative: body.languageNative ? body.languageNative : [],
							language: body.language ? body.language : [],
							rating : {
								students: {
									total: 0,
									totalTallied: 0,
									tallied: {
										zero: 0,
										one: 0,
										two: 0,
										three: 0,
										four: 0
									}
								},
								teacher: {
									total: 0,
									totalTallied: 0,
									tallied: {
										zero: 0,
										one: 0,
										two: 0,
										three: 0,
										four: 0
									}
								}
							},
							joined: Date.now(),
							lastactivity: 0
						}

				return User.create(object, file)
					.then(
						response => {
							return res.json({success: true, mes: 'Operacion Exitosa', id: response})
						}, error => {
							console.log(error)
							return res.status(500).json({success: false, error: error})
						}
					)
					.catch(error => {
						console.log(error)
						return res.status(500).json({success: false, error: '404'})
					})
			})
router.route('/:id')
			.get((req, res, next) => {
				var id   = req.params.id,
						user = req.user;

				if(!id)
					return res.json({ success: false, error: 'No enviaste el id del usuario a modificar' })
				if(!user)
					return res.json({ success: false, error: 'not-authentication' })
				if(user.priv !== 1)
					return res.json({ success: false, error: 'not-priv' })

				var item = {};
				return auth.getUser(id)
					.then(
						response => {
							item = response.toJSON();

							return User.findOne(id);
						},
						error => {
							console.log(error)
							if(error.code)
								return res.status(500).json({success: false, error: error})
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
						}
					).then(
						response => {
							let data = {};
							if(response){
								data = response.data();
								return res.json({ success: true, item: Object.assign(data, item), id})
							}else 
								return res.json({ success: false, error: 'Usuario no Existente' })						
						},
						error => {
							console.log(error)
							if(error.code)
								return res.status(500).json({success: false, error: error})
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
						}
					)
					.catch(error => {
						console.log(error)
						return res.status(500).json({success: false, error: '404'})
					})
			})
			.put(multipartGcs.array(), (req, res, next) => {
				var id   = req.params.id,
						user = req.user,
						body = JSON.parse(req.body.data),
						file = req.files['photo'] ? req.files['photo'][0] : false;
				
				if(!file)
					return next();
						
				if(!id)
					return res.json({ success: false, error: 'No enviaste el id del usuario a modificar' })
				if(!user)
					return res.json({ success: false, error: 'not-authentication' })
				if(user.priv !== 1)
					return res.json({ success: false, error: 'not-priv' })

				var data = {};
				return User.findOne(id)
					.then(
						response => {
							data = response.data();
							if(Object.keys(data).length){
								return deleteFile(data.photoRef);
							}else return res.json({ success: false, error: 'Usuario no existente' })
						}, 
						error => {
							console.log(error)
							if(error)
									return res.status(500).json({success: false, error: error})
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
						}
					)
					.then(
						response => {
							let destination = `users/${id}/thumbnails`,
									name 				= `${id}_${Date.now()}`;
							
							return uploadFile(file, destination, name);
						},
						error => {
							console.log(error)
							if(error)
				      	return res.status(500).json({success: false, error: error})
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
						}
					)
					.then(
						response => {
							body.photo = response.reference.url;
							body.photoRef = response.reference.ref;
							
							return User.update(data, body, id);
						},
						error => {
							console.log(error)
							if(error.code)
								return res.status(500).json({success: false, error: error})
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
						}
					)
					.then(
						response => {
							return res.json({ success: true, mes: 'Operacion Exitosa', id: response })	
						},
						error => {
							console.log(error)
							if(error.code)
								return res.status(500).json({success: false, error: error})
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
						}
					)
					.catch(error => {
						console.log(error)
						return res.status(500).json({success: false, error: '404'})
					})
			})
			.put((req, res, next) => {
				var id   = req.params.id,
						user = req.user;
						body = JSON.parse(req.body.data);
				
				if(!id)
					return res.json({ success: false, error: 'No enviaste el id del usuario a modificar' })
				if(!user)
					return res.json({ success: false, error: 'not-authentication' })
				if(user.priv !== 1)
					return res.json({ success: false, error: 'not-priv' })

				var data = {};
				return User.findOne(id)
					.then( 
						response => {
							data = response.data();
							if(Object.keys(data).length){
								return User.update(data, body, id);
							}else return res.json({ success: false, error: 'Usuario no existente' })					
						},
						error => {
							console.log(error)
							if(error)
									return res.status(500).json({success: false, error: error})
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
						}
					)
					.then(
						response => {
							return res.json({ success: true, mes: 'Operacion Exitosa', id: response })	
						},
						error => {
							console.log(error)
							if(error.code)
								return res.status(500).json({success: false, error: error})
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
						}
					)
					.catch(error => {
						console.log(error)
						return res.status(500).json({success: false, error: '404'})
					})
			})
			.delete((req, res) => { 
				var id   = req.params.id,
						user = req.user;

					if(!id)
						return res.json({ success: false, error: 'No enviaste el id del usuario a modificar' })
					if(!user)
						return res.json({ success: false, error: 'not-authentication' })
					if(user.priv !== 1)
						return res.json({ success: false, error: 'not-priv' })

					var data = {};
					return User.findOne(id)
						.then(
							response => {
								data = response.data();
								if(Object.keys(data).length) 
									return User.delete(id);
								else
									return res.json({ success: false, error: 'Usuario no existente' })
							},
							error => {
								console.log(error)
								if(error.code)
									return res.status(500).json({success: false, error: error})
								return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
							}
						)
						.then(
							response => {
								return deleteFile(data.photoRef);
							},
							error => {
								console.log(error)
								if(error)
									return res.status(500).json({success: false, error: 'Error al eliminar la foto'})
								return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
							}
						)
						.then(
							response => {
								return res.json({ success: true, mes: 'Operacion Exitosa', id: id })
							}, error => {
								console.log(error)
								if(error)
									return res.status(500).json({success: false, error: error})
								return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
							}
						)
						.catch(error => {
							console.log(error)
							return res.status(500).json({success: false, error: '404'})
						})
			})

module.exports = router;