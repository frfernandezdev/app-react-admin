const express   = require('express'),
      router    = express.Router(),
    { Moneys }  = require('../../objects');

const Money = new Moneys();
router.route('/')
      .post((req, res, next) => {
        var body = req.body,
            user = req.user;

        if(!user)
          return res.json({ success: false, error: 'not-authentication' })
        if(user.priv !== 1)
          return res.json({ success: false, error: 'not-priv' })

        if(!body.name && typeof body.name !== 'string')
          return res.json({success: false, error: "No enviaste nombre ó enviaste un parametro invalido"})
        if(!body.acronym && typeof body.acronym !== 'string')
          return res.json({success: false, error: "No enviaste acronimo ó enviaste un parametro invalido"})
        
        var object = {
          name: body.name,
          acronym: body.acronym
        }
        return Money.create(object)
          .then(response => {
            return res.json({ success: true, mes: 'Operacion Exitosa', id: response })
          })
          .catch(error => {
            console.log(error)
            if(error)
              return res.status(500).json({ success: false, error: error })
						return res.status(500).json({ success: false, error: '404' })
					})

      }) 
router.route('/:id')
      .get((req, res, next) => {
        var id   = req.params.id,
            user = req.user;
            
        if(!id)
          return res.json({ success: false, error: 'No enviaste el id del usuario a modificar' })
        if(!user)
          return res.json({ success: false, error: 'not-authentication' })
        if(user.priv !== 1)
          return res.json({ success: false, error: 'not-priv' })
        
        return Money.findOne(id)
          .then(response => {
            if(response)
              return res.json({ success: true, item: response.data(), id})
            else return res.json({ success: false, error: 'Usuario no Existente' })
          })
          .catch(error => {
            console.log(error)
            if(error)
              return res.status(500).json({success: false, error: error})
						return res.status(500).json({success: false, error: '404'})
					})
      })
      .put((req, res, next) => {
        var id   = req.params.id,
						user = req.user,
            body = req.body;
        
        if(!id)
          return res.json({ success: false, error: 'No enviaste el id del usuario a modificar' })
        if(!user)
          return res.json({ success: false, error: 'not-authentication' })
        if(user.priv !== 1)
          return res.json({ success: false, error: 'not-priv' })

        return Money.findOne(id)
          .then(
            response => {
              let data = response.data();

              if(data)
                return Money.update(data, body, id)
              else return res.json({ success: false, error: 'Lenguaje no existente' })
            }, error => {
              console.log(error)
              if(error)
                return res.status(500).json({success: false, error: error})
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
            }
          )
          .then(
            response => {
              return res.json({ success: true, mes: 'Operacion Exitosa', id: response })	
            }, error => {
              console.log(error)
							if(error)
								return res.status(500).json({success: false, error: error})
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
            }
          )
          .catch(error => {
            console.log(error)
						return res.status(500).json({success: false, error: '404'})
          })
      })
      .delete((req, res, next) => {
        var id   = req.params.id,
						user = req.user,
            body = req.body;
        
        if(!id)
          return res.json({ success: false, error: 'No enviaste el id del usuario a modificar' })
        if(!user)
          return res.json({ success: false, error: 'not-authentication' })
        if(user.priv !== 1)
          return res.json({ success: false, error: 'not-priv' })
        
        return Money.findOne(id)
          .then(
            response => {
              let data = response.data();

              if(data)
                return Money.delete(id)
              else return res.json({ success: false, error: 'Lenguaje no existente' }) 
            }, error => {
              console.log(error)
              if(error)
								return res.status(500).json({success: false, error: error})
              return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
            }
          )
          .then(
            response => {
              return res.json({ success: true, mes: 'Operacion Exitosa', id: id })
            }, error => {
              console.log(error)
              if(error)
								return res.status(500).json({success: false, error: error})
              return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
            }
          )
          .catch(error => {
            console.log(error)
            return res.status(500).json({success: false, error: '404'})
          })
      })

module.exports = router;