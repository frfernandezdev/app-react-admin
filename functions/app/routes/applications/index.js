const { auth, storage } 					= require('../../../firebase'),
				express         					= require('express'),
				router          					= express.Router(),
			{ MultipartGcs }            = require('../../middleware'),
			{ 
				Applications, 
				Users,
				Teachers 
			}  													= require('../../objects'),
			{ 
				uploadFile, 
				uploadFiles,
				deleteFile 
			}														= require('../../utils'),
			async  											= require('asyncawait/async'),
			await  											= require('asyncawait/await');
	
const multipartGcs 	= MultipartGcs();

const Application 	= new Applications();
const Teacher     	= new Teachers();
const User 					= new Users();

function urlValid(url) {
	let pattern = /^(ftp|http|https):\/\/[^ '']+$/,
			regex = new RegExp(pattern);

	return regex.test(url);
}

const deleteFiles = async(data => {
	if(data.fileDni)
		await (deleteFile(data.fileDniRef));
	
	if(data.student && Array.isArray(data.student)){
		for(const iterador of data.student){
			if(iterador.file && iterador.file.ref)
				await (deleteFile(iterador.file.ref));
			
		}
	}
		
	if(data.professional_expirence && Array.isArray(data.professional_expirence)){
		for(const iterador of data.professional_expirence){
			if(iterador.file && iterador.file.ref)
				await (deleteFile(iterador.file.ref));
			
		}
	}
	
	return Promise.resolve(true);
})

router.route('/')
			.get((req, res, next) => {
				var where 	= req.query.where,
						orderBy = req.query.orderBy,
						limit 	= req.query.limit,
						start 	= req.query.start,
						user  	= req.user;
				
				if(!user)
					return res.json({ success: false, error: 'not-authentication' })
				if(user.priv !== 1 && user.priv !== 2)
					return res.json({ success: false, error: 'not-priv' })

				if(limit)
					limit 	= parseInt(limit);
				if(orderBy)
					orderBy = JSON.parse(orderBy);
				if(where)
					where   = JSON.parse(where);
				
				var count = 0;
				return Application.count()
					.then(
						response => {
							count = response.size;
							return Application.findAll(where, orderBy, limit ,start)
						},
						error => {
							console.log(error)
							if(error.code)
								return res.json({ success: false, error: error })
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
						}
					)
					.then(
						response => {
							return res.json({ success: true, mes: 'Operacion Exitosa', items: response, count })
						},
						error => {
							console.log(error)
							if(error.code)
								return res.json({ success: false, error: error })
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
						}
					)
					.catch(error => {
						console.log(error)
						return res.status(500).json({ success: false, error: '404' })
					})
			})
      .post(multipartGcs.array(), (req, res, next) => {
				var body = JSON.parse(req.body.data),
						user = req.user,
						files = req.files;
				
				if(!user)
					return res.json({ success: false, error: 'not-authentication' })
				if(!body.user)
					return res.json({ success: false, error: 'No enviaste el identificador del usuario a postularse' })
				if(!body.video && !urlValid(body.video))
					return res.json({ success: false, error: 'No enviaste la url del video, o la url tiene un formato incorrecto' })
				if(!body.student && !Array.isArray(body.student))
					return res.json({ success: false, error: 'No enviaste tu educacion, o enviaste un parametro invalido' })
				if(!body.professional_expirence && !Array.isArray(body.professional_expirence))
					return res.json({ success: false, error: 'No enviaste tu experiencia profesional, o enviaste un parametro invalido' })
				if(!body.schedule && !Array.isArray(body.schedule))
					return res.json({ success: false, error: 'No enviaste el horario de disposicion, o enviaste un parametro invalido' })
				if(!files['fileDni'] && !files['fileDni'][0])
					return res.json({ success: false, error: 'No enviaste el comprovante de documento de identificacion, o enviaste un parametro invalido' })
				if(!body.skill && typeof body.skill !== 'object')
					return res.json({ success: false, error: 'No enviaste las habilidades, o enviaste el parametro invalido' })
				if(!body.aboutMe && typeof body.aboutMe !== 'string')
					return res.json({ success: false, error: 'No enviaste el descripcion, o enviaste un parametro invalido' })
				if(!body.methodology && typeof body.methodology !== 'string')
					return res.json({ success: false, error: 'No enviaste la metodologia de estudio a desempañar, o enviaste un parametro invalido' })
				if(!body.materialLearn && typeof body.materialLearn !== 'object')
					return res.json({ success: false, error: 'No enviaste los materiales de apoyo, o enviaste un parametro invalido' })
				if(!body.languageToLearn && !Array.isArray(body.languageToLearn))
					return res.json({ success: false, error: 'No enviaste los lenguajes a enseñar, o enviaste un parametro invalido' })

				// if(user.uid !== body.user)
				// 	return res.json({ success: false, error: 'No estas autorizado para realizar esta acción' })
				
				var uid = body.user,
						object = {
							user: body.user,
							video: body.video,
							student: body.student,
							professional_expirence: body.professional_expirence,
							schedule: body.schedule,
							fileDni: null,
							fileDniRef: null,
							aboutMe: body.aboutMe,
							skill: {
								junnior: body.skill.junnior ? Boolean(body.skill.junnior) : false,
								kids: body.skill.kids ? Boolean(body.skill.kids) : false,
								teen: body.skill.teen ? Boolean(body.skill.teen) : false,
								bussiness: body.skill.bussiness ? Boolean(body.skill.bussiness) : false,
								test: {
									IELTS: body.skill.test ? body.skill.test.IELTS ? body.skill.test.IELTS : false : false,
									BEC: body.skill.test ? body.skill.test.DEC ? body.skill.test.DEC : false : false,
									TOEFL: body.skill.test ? body.skill.test.TOEFL ? body.skill.test.TOEFL : false : false,
									PET: body.skill.test ? body.skill.test.PET ? body.skill.test.PET : false : false,
									TOEIC: body.skill.test ? body.skill.test.TOEIC ? body.skill.test.TOEIC : false : false,
									CAE: body.skill.test ? body.skill.test.CAE ? body.skill.test.CAE : false : false,
									DELE: body.skill.test ? body.skill.test.DELE ? body.skill.test.DELE : false : false,
									CPE: body.skill.test ? body.skill.test.CPE ? body.skill.test.CPE : false : false,
									CELU: body.skill.test ? body.skill.test.CELU ? body.skill.test.CELU : false : false,
									KET: body.skill.test ? body.skill.test.KET ? body.skill.test.KET : false : false,
									FCE: body.skill.test ? body.skill.test.FCE ? body.skill.test.FCE : false : false,
									ILEC: body.skill.test ? body.skill.test.ILEC ? body.skill.test.ILEC : false : false       
								}
							},
							methodology: body.methodology,
							materialLearn: {
								file_pdf: body.materialLearn.file_pdf ? body.materialLearn.file_pdf : false,
								template_quiz: body.materialLearn.template_quiz ? body.materialLearn.template_quiz : false,
								file_text: body.materialLearn.file_text ? body.materialLearn.file_text : false,
								file_image: body.materialLearn.file_image ? body.materialLearn.file_image : false,
								article_news: body.materialLearn.article_news ? body.materialLearn.article_news : false,
								grafic_table: body.materialLearn.grafic_table ? body.materialLearn.grafic_table : false,
								presentation: body.materialLearn.presentation ? body.materialLearn.presentation : false,
								questionnaries: body.materialLearn.questionnaries ? body.materialLearn.questionnaries : false,
								task: body.materialLearn.task ? body.materialLearn.task : false,
								file_video: body.materialLearn.file_video ? body.materialLearn.file_video : false,
								file_audio: body.materialLearn.file_audio ? body.materialLearn.file_audio : false            
							},
							languageToLearn: body.languageToLearn ? body.languageToLearn : [],
							application_date: Date.now(),
							status: 1 // it handle three status, 1 for status send, 2 for status evaluando, 3 for status aceptado || rechazado
						}

				return User.findOne(uid)
								.then(
									response => {
										if(response.data()){
											where = { 
												field: 'user',
												operator: '==',
												value: uid
											 }
											return Application.find(where, null, null, null);
										}else 
											return res.json({ success: false, error: 'Usuario no existente' })
									},
									error => {
										console.log(error)
										if(error.code)
											return res.status(500).json({success: false, error: error})
										return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
									}
								)
								.then(
									response => {
										let destination = `users/${uid}/teacher`,
											  name = `${body.user}_${Date.now()}`,
												data = response;

										if(!data.length > 0){
											if(Object.keys(files).length > 1)
												return uploadFiles(files, destination, name);
											else if(Object.keys(files).length === 1 && files['fileDni'][0])
												return uploadFile(files['fileDni'][0], destination, name);
											else 
												return Promise.resolve(true);
										}else {
											res.json({ success: false, error: 'Ya existe una postulacion para este usuario' })
											return Promise.resolve(false)
										}
									},
									error => {
										console.log(error)
										if(error.code)
											return res.status(500).json({success: false, error: error})
										return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
									}
								)
								.then(
									response => {
										console.log(response) 
										if(response){
											if(!response.uploads){
												object.fileDniRef = response.reference.ref;
												object.fileDni 		= response.reference.url;
											}else {
												if(response.reference['fileDni']){
													object.fileDni 		= response.reference['fileDni'][0].reference.url;
													object.fileDniRef = response.reference['fileDni'][0].reference.ref;
												}
												if(response.reference['fileStudents'] && Array.isArray(response.reference['fileStudents']))
													for(k in object.student)
														object.student[k].file = {
															file: response.reference['fileStudents'][k].reference.url,
															ref: response.reference['fileStudents'][k].reference.ref
														}
												if(response.reference['filePE'] && Array.isArray(response.reference['filePE']))
													for(e in object.professional_expirence)
														object.professional_expirence[e].file = {
															file: response.reference['filePE'][e].reference.url,
															ref: response.reference['filePE'][e].reference.ref
														}
											}
											return Application.create(object)
										}
										throw new Error('Problems to upload images');
									},
									error => {
										console.log(error)
										if(error.code)
											return res.status(500).json({success: false, error: error})
										return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
									}
								)
								.then(
									response => {
										if(response)
											return res.json({ success: true, mes: 'Operacion Exitosa', id: body.user })
										throw new Error('Problems to create applications')
									},
									error => {
										console.log(error)
										if(error.code)
											return res.status(500).json({success: false, error: error})
										return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
									}
								)
								.catch(error => {
									console.log(error)
									return res.status(500).json({ success: false, error: '404' })
								})
			}) 
router.route('/:id')
			.get((req, res, next) => {
				var id   = req.params.id,
						user = req.user;

				if(!id)
					return res.json({ success: false, error: 'No enviaste el id del ' })
				if(!user)
					return res.json({ success: false, error: 'not-authentication' })
				
				return Application.findOne(id)
					.then(
						response => {							
							return res.json({ success: true, item: response, id})
						},
						error => {
							console.log(error)
							if(error.code)
								return res.status(500).json({success: false, error: error})
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
						}
					)
					.catch(error => {
						console.log(error)
						return res.status(500).json({success: false, error: '404'})
					})
			})
router.route('/:options/:id')
			.put((req, res, next) => {
				var id 			= req.params.id,
						options = req.params.options,
						user    = req.user;

				if(options !== 'accept')	
					return next();

				if(!user)
					return res.json({ success: false, error: 'not-authentication' })
				if(!id)
					return res.json({ success: false, error: '' })
				if(!options)
					return res.json({ success: false, error: '' })

				return Application.findOne(id)
					.then(
						response => {
							response.user = response.user.id;
							return Teacher.create(response);
						}, 
						error => {
							console.log(error)
							if(error.code)
								return res.status(500).json({success: false, error: error})
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
						}
					)
					.then(
						response => {
							return Application.delete(id); 
						}, 
						error => {
							console.log(error)
							if(error.code)
								return res.status(500).json({success: false, error: error})
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
						}
					)
					.then(
						response => {
							if(response)
								return res.json({ success: true, mes: 'Operacion Exitosa' })
							throw new Error('Problems to create teacher')
						},
						error => {
							console.log(error)
							if(error.code)
								return res.status(500).json({success: false, error: error})
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
						}
					)
					.catch(error => {
						console.log(error)
						return res.status(500).json({success: false, error: '404'})
					})
			})
			.put((req, res, next) => {
				var id 			= req.params.id,
						options = req.params.options,
						user    = req.user;

				if(options !== 'cancel')	
					return res.json({ success: false, error: 'Opcion no permitida' })
				
				if(!user)
					return res.json({ success: false, error: 'not-authentication' })
				if(!id)
					return res.json({ success: false, error: '' })
				if(!options)
					return res.json({ success: false, error: '' })
				
				var data = {};
				return Application.findOne(id)
					.then(
						response => {
							data = response;
							return Application.delete(id);
						}, 
						error => {
							console.log(error)
							if(error.code)
								return res.status(500).json({success: false, error: error})
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
						}
					)
					.then(
						response => {
							return deleteFiles(data);
						},
						error => {
							console.log(error)
							if(error)
								return res.status(500).json({success: false, error: 'Error al eliminar la foto'})
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
						}
					)
					.then(
						response => {
							if(response)
								return res.json({ success: true, mes: 'Operacion Exitosa', id: id })
							throw new Error('Problems to delete files')
						},
						error => {
							console.log(error)
							if(error.code)
								return res.status(500).json({success: false, error: error})
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
						}
					)
					.catch(error => {
						console.log(error)
						return res.status(500).json({success: false, error: '404'})
					})
			})
module.exports = router;