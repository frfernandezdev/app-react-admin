const { auth, storage } 					= require('../../../firebase'),
				express 									= require('express'),
				router  									= express.Router(),
			{ MultipartGcs }            = require('../../middleware'),
			{ Users } 									= require('../../objects'),
			{ uploadFile, deleteFile  } = require('../../utils');

const multipartGcs = MultipartGcs({});

const User = new Users();
router.route('/')
	.get((req, res ,next) => {
		var user = req.user; 
		
		if(!user)
			return res.json({ success: false, error: 'not-authentication' })
		
		return res.json({ success: true, item: user })
	});
router.route('/register')
	.post((req, res, next) => {
		var user = req.user,
				body = req.body;

		if(!user)
			return res.json({ success: false, error: 'not-authentication' })	

		if(!body.email && typeof body.email !== 'string')
			return res.json({ success: false, error: "No enviaste correo electronico ó enviaste un parametro invalido" });
		if(!body.password && typeof body.password !== 'string')
			return res.json({ success: false, error: "No enviaste una contraseña ó enviaste un parametro invalido" });
		if(!body.fullname && typeof body.fullname !== 'string')
			return res.json({ success: false, error: "No enviaste un nombre completo ó enviaste un parametro invalido" });
		if(!body.username && typeof body.username !== 'string')
			return res.json({ success: false, error: "No enviaste un nombre de usuario ó enviaste un parametro invalido" });
		if(!body.mobile && typeof body.mobile !== 'string')
			return res.json({ success: false, error: "No enviaste tu numero de telefono ó enviaste un parametro invalido" });
		if(!file)
			return res.json({ success: false, error: 'No enviaste tu foto de perfil ó enviaste un parametro invalido' });
	
		var search = (body.username+body.fullname).toLowerCase().replace(/\s/g, ''),
				object = {
					search: search,
					email: body.email, //
					password: body.password, //
					username: body.username, //
					fullname: body.fullname, //
					mobile: body.mobile, //
					gender: body.gender, //
					birthdate: body.birthdate, //
					phone: null,
					address: null,
					online: false,
					status: 1,
					priv: 3,
					company: null,
					watch: null, // true is 24 Hrs 
					money: null,
					tools: {
						skype: false,
						facetime: false,
						gHangouts: false,
						qq: false,
						wechat: false
					},
					website: null,
					liveLocalities: [],
					bornLocalities: [],
					timezone: {},
					languageNative: [],
					language: [],
					teacher: false,
					state: false,
					photo: null,
					photoRef: null,
					rating: {
						students: {
							total: 0,
							totalTallied: 0,
							tallied: {
								zero: 0,
								one: 0,
								two: 0,
								three: 0,
								four: 0
							}
						},
						teacher: {
							total: 0,
							totalTallied: 0,
							tallied: {
								zero: 0,
								one: 0,
								two: 0,
								three: 0,
								four: 0
							}
						}
					},
					joined: Date.now(),
					lastactivity: 0
				};

		return User.create(object, file)
			.then(
				response => {
					return res.json({success: true, mes: 'Operacion Exitosa', id: response})
				}, error => {
					console.log(error)
					return res.status(500).json({success: false, error: error})
				}
			)
			.catch(error => {
				console.log(error)
				return res.status(500).json({success: false, error: '404'})
			})
	});
router.route('/complete')
	.put((req, res) => {
		var id   = req.user.uid,
				user = req.user,
				body = req.body;

		if(!user)
			return res.json({ success: false, error: 'not-authentication' })
		if(!body.languageNative && !Array.isArray(body.languageNative))
			return res.json({success: false, error: "No enviaste un languaje nativo ó enviaste un parametro invalido"})
		if(!body.watch && !Number.isInteger(body.watch))
			return res.json({success: false, error: "No enviaste tipo de hora ó enviaste un parametro invalido"})
		if(!body.timezone && typeof body.timezone !== 'object')
			return res.json({success: false, error: "No enviaste una zona horaria ó enviaste un parametro invalido"})
		if(!body.language && typeof body.language !== 'object')
			return res.json({success: false, error: "No enviaste los lenguajes ó enviaste un parametro invalido"})
		
		var object = {
			languageNative: body.languageNative,
			watch: body.watch,
			timezone: body.timezone,
			language: body.language
		};

		return User.update(user, object, id)
			.then(
				response => {
					return res.json({ success: true, mes: 'Operacion Exitosa', id: response })	
				}, error => {
					console.log(error)
					if(error.code)
						return res.status(500).json({success: false, error: error})
					return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
				}
			)
	})
router.route('/')
	.put(multipartGcs.array(), (req, res, next) => {
		var id   = req.user.uid,
				user = req.user,
				body = JSON.parse(req.body.data),
				file = req.files['photo'] ? req.files['photo'][0] : false;

		if(!file)
			return next();
				
		if(!user)
			return res.json({ success: false, error: 'not-authentication' })

		var data = {};
		return User.findOne(id)
			.then(
				response => {
					data = response.data();
					if(Object.keys(data).length){
						return deleteFile(data.photoRef);
					}else return res.json({ success: false, error: 'Usuario no existente' })
				}, 
				error => {
					console.log(error)
					if(error)
							return res.status(500).json({success: false, error: error})
					return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
				}
			)
			.then(
				response => {
					let destination = `users/${id}/thumbnails`,
							name 				= `${id}_${Date.now()}`;
					return uploadFile(file, destination, name);
				},
				error => {
					console.log(error)
					if(error)
							return res.status(500).json({success: false, error: error})
					return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
				}
			)
			.then(
				response => {
					if(response){
						body.photo = response.reference.url;
						body.photoRef = response.reference.ref;
					}
					return User.update(data, body, id);
				},
				error => {
					console.log(error)
					if(error.code)
						return res.status(500).json({success: false, error: error})
					return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
				}
			)
			.then(
				response => {
					return res.json({ success: true, mes: 'Operacion Exitosa', id: response })	
				},
				error => {
					console.log(error)
					if(error.code)
						return res.status(500).json({success: false, error: error})
					return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
				}
			)
			.catch(error => {
				console.log(error)
				return res.status(500).json({success: false, error: '404'})
			})
	})
	.put((req, res, next) => {
		var id   = req.user.uid,
				user = req.user,
				body = JSON.parse(req.body.data);

		if(!user)
			return res.json({ success: false, error: 'not-authentication' })

		return User.update(user, body, id)
			.then(
				response => {
					return res.json({ success: true, mes: 'Operacion Exitosa', id: response })	
				},
				error => {
					console.log(error)
					if(error.code)
						return res.status(500).json({success: false, error: error})
					return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
				}
			)
			.catch(error => {
				console.log(error)
				return res.status(500).json({success: false, error: '404'})
			})
	});
router.route('/forgotpassword')
	.post((req, res, next) => {

	})
router.route('/validate/:mail/:tokenValid')
	.put((req, res, next) => {

	})
router.route('/reauthentication')
	.post((req, res, next) => {

	})

module.exports = router;
