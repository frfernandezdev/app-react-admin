const { auth, storage }           = require('../../../firebase'),
       express                    = require('express'),
       router                     = express.Router(),
      { MultipartGcs }            = require('../../middleware'),
      {
        Applications,
        Users,
        Teachers
      }                           = require('../../objects'),
      {
        uploadFile,
        uploadFiles,
        deleteFile
      }                            = require('../../utils'),
      async                        = require('asyncawait/async'),
      await                        = require('asyncawait/await');

const multipartGcs 	= MultipartGcs();

const Application 	= new Applications();
const Teacher     	= new Teachers();
const User 					= new Users();

function urlValid(url) {
	let pattern = /^(ftp|http|https):\/\/[^ '']+$/,
			regex = new RegExp(pattern);

	return regex.test(url);
}

const deleteFiles = async((data, body) => {
	if(data.fileDni && body.fileDni)
		await (deleteFile(data.fileDniRef));
	
	if(data.student && Array.isArray(data.student) && body.student)
		for(let index in data.student)
			if(data.student[index].file && data.student[index].file.ref && body.student[index].enableFile === index)
				await (deleteFile(data.student[index].file.ref));
	
	if(data.professional_expirence && Array.isArray(data.professional_expirence) && body.professional_expirence)
		for(let index in data.professional_expirence)
			if(data.professional_expirence[index].file && data.professional_expirence[index].file.ref && body.professional_expirence[index].enableFile === index)
				await (deleteFile(data.professional_expirence[index].file.ref));

	return Promise.resolve(true);
})
const processUpdate = async((response, body) => {
	if(!response.uploads){
		body.fileDniRef = response.reference.ref;
		body.fileDni 		= response.reference.url;
	}else {
		if(response.reference['fileDni']){
			body.fileDni 		= response.reference['fileDni'][0].reference.url;
			body.fileDniRef = response.reference['fileDni'][0].reference.ref;
		}
		console.log(response.reference)
		if(response.reference['fileStudents'] && Array.isArray(response.reference['fileStudents'])){
			let i = 0;
			await (body.student.forEach((docs, k) => {
				if(body.student[k].enableFile && body.student[k].enableFile === k){
					body.student[k].file = {
						file: response.reference['fileStudents'][i].reference.url,
						ref: response.reference['fileStudents'][i].reference.ref
					}
					i++;
				}
			}))
		}
		if(response.reference['filePE'] && Array.isArray(response.reference['filePE'])){
			let i = 0;
			await (body.professional_expirence.forEach((docs, e) => {
				if(body.professional_expirence[e].enableFile && body.professional_expirence[e].enableFile === e){
					body.professional_expirence[e].file = {
						file: response.reference['filePE'][i].reference.url,
						ref: response.reference['filePE'][i].reference.ref
					}
					i++;
				}
			}))
		}
	}
	console.log(body)
	return Promise.resolve(body);
})

router.route('/')
      .get((req, res, next) => {
        var where 	= req.query.where,
						orderBy = req.query.orderBy,
						limit 	= req.query.limit,
						start 	= req.query.start,
            user  	= req.user;
        
        if(!user)
          return res.json({ success: false, error: 'not-authentication' })
        if(user.priv !== 1 && user.priv !== 2)
					return res.json({ success: false, error: 'not-priv' })

        if(limit)
					limit 	= parseInt(limit);
				if(orderBy)
					orderBy = JSON.parse(orderBy);
				if(where)
          where   = JSON.parse(where);
          
        var count = 0;

        return Teacher.count()
					.then(
						response => {
							count = response.size;
							return Teacher.findAll(where, orderBy, limit ,start)
						},
						error => {
							console.log(error)
							if(error.code)
								return res.json({ success: false, error: error })
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
						}
					)
					.then(
						response => {
							return res.json({ success: true, mes: 'Operacion Exitosa', items: response, count })
						},
						error => {
							console.log(error)
							if(error.code)
								return res.json({ success: false, error: error })
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
						}
					)
					.catch(error => {
						console.log(error)
						return res.status(500).json({ success: false, error: '404' })
					})
			})
router.route('/:id')
			.get((req, res) => {
				var id 		= req.params.id,
						user  = req.user;
						
				if(!id)
					return res.json({ success: false, error: 'No enviaste el id del ' })
				if(!user)
					return res.json({ success: false, error: 'not-authentication' })
				
				return Teacher.findOne(id)
					.then(
						response => {
							return res.json({success: true, item: response})
						},
						error => {
							console.log(error)
							if(error.code)
								return res.status(500).json({success: false, error: error})
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
						}
					)
					.catch(error => {
						console.log(error)
						return res.status(500).json({success: false, error: '404'})
					})
			})
			.put(multipartGcs.array(), (req, res, next) => {
				var id    = req.params.id,
						user  = req.user,
						body  = JSON.parse(req.body.data),
						files = req.files;
								
				if(!Object.keys(files).length > 0)
					return next();

				if(!Object.keys(body).length > 0)
					return res.json({ success: false, error: 'No has enviado ningun campo a editar'})
				if(!user)
					return res.json({ success: false, error: 'not-authentication' })
				if(!id)
					return res.json({ success: false, error: 'No enviaste el id del usuario a modificar' })
				if(user.priv !== 1)
					return res.json({ success: false, error: 'not-priv' })

				var data = {};
				return Teacher.findOne(id)
					.then(
						response => {
							data = response;
							if(Object.keys(data).length){
								return deleteFiles(data, body)
							}else return res.json({ success: false, error: 'Profesor no existente'})
						}, 
						error => {
							console.log(error)
							if(error)
									return res.status(500).json({success: false, error: error})
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
						}
					)
					.then(
						response => {
							let destination = `users/${data.user.id}/teacher`,
												name = `${data.user.id}_${Date.now()}`;
												
							if(!data.length > 0){
								if(Object.keys(files).length > 1)
									return uploadFiles(files, destination, name);
								else if(Object.keys(files).length === 1 && files['fileDni'])
									return uploadFile(files['fileDni'][0], destination, name);
								else if(Object.keys(files).length === 1 && files['fileStudents'])
									return uploadFiles(files, destination, name);
								else if(Object.keys(files).length === 1 && files['filePE'])
									return uploadFiles(files, destination, name);
								else
									return Promise.resolve(true);
							}else {
								res.json({ success: false, error: 'Ya existe una postulacion para este usuario' })
								return Promise.resolve(false)
							}						
						},
						error => {
							console.log(error)
							if(error.code)
								return res.status(500).json({success: false, error: error})
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
						}
					)
					.then(
						response => {
							if(response)
								return processUpdate(response, body)
							
							throw new Error('Problems to upload images');
						}, 
						error => {
							console.log(error)
							if(error.code)
								return res.status(500).json({success: false, error: error})
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
						}
					)
					.then(
						response => {
							return Teacher.update(data, response, id);
						},
						error => {
							console.log(error)
							if(error.code)
								return res.status(500).json({success: false, error: error})
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
						}
					)
					.then(
						response => {
							return res.json({ success: true, mes: 'Operacion Exitosa' })	
						},
						error => {
							console.log(error)
							if(error.code)
								return res.status(500).json({success: false, error: error})
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
						}
					)
					.catch(error => {
						console.log(error)
						return res.status(500).json({success: false, error: '404'})
					})
			})
			.put((req, res, next) => {
				var id    = req.params.id,
						user  = req.user;
						body  = JSON.parse(req.body.data);
				
				if(!Object.keys(body).length > 0)
					return res.json({ success: false, error: 'No has enviado ningun campo a editar'})
				if(!user)
					return res.json({ success: false, error: 'not-authentication' })
				if(!id)
					return res.json({ success: false, error: 'No enviaste el id del usuario a modificar' })
				// if(user.priv !== 1)
				// 	return res.json({ success: false, error: 'not-priv' })
				console.log(body)
				var data = {};
				return Teacher.findOne(id)
					.then(
						response => {
							data = response;
							if(Object.keys(data).length){
								return Teacher.update(data, body, id);
							}else return res.json({ success: false, error: 'Profesor no existente'})
						}, 
						error => {
							console.log(error)
							if(error)
									return res.status(500).json({success: false, error: error})
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
						}
					)
					.then(
						response => {
							return res.json({ success: true, mes: 'Operacion Exitosa' })	
						},
						error => {
							console.log(error)
							if(error.code)
								return res.status(500).json({success: false, error: error})
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
						}
					)
					.catch(error => {
						console.log(error)
						return res.status(500).json({success: false, error: '404'})
					})
			})
			.delete((req, res) => {
				var id   = req.params.id,
						user = req.user;
				
				if(!id)
					return res.json({ success: false, error: 'No enviaste el id del usuario a modificar' })
				if(!user)
					return res.json({ success: false, error: 'not-authentication' })
				// if(user.priv !== 1)
				// 	return res.json({ success: false, error: 'not-priv' })

				var data = {};
				return Teacher.findOne(id)
					.then(
						response => {
							data = response;
							if(Object.keys(data).length)
								return Teacher.delete(id);
							else
								return res.json({ success: false, error: 'Profesor no existente' })
						},
						error => {
							console.log(error)
							if(error.code)
								return res.status(500).json({success: false, error: error})
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
						}
					)
					.then(
						response => {
							return deleteFiles(data, data);
						},
						error => {
							console.log(error)
							if(error)
								return res.status(500).json({success: false, error: 'Error al eliminar la foto'})
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
						}
					)
					.then(
						response => {
							return res.json({ success: true, mes: 'Operacion Exitosa', id: id })
						}, error => {
							console.log(error)
							if(error)
								return res.status(500).json({success: false, error: error})
							return res.status(500).json({success: false, error: 'Oops, Ha ocurrido un error interno'})
						}
					)
					.catch(error => {
						console.log(error)
						return res.status(500).json({success: false, error: '404'})
					})
			})

module.exports = router;