const async  = require('asyncawait/async'),
      await  = require('asyncawait/await'),
      fs     = require('fs'),
      os     = require('os'),
      path   = require('path'),
      uuid4  = require('uuid/v4');

const config = {
  projectId: 'admin-speakenglishsite',
	keyFilename: 'admin-speakenglishsite-firebase-adminsdk-5202y-2c7992e22e.json'
};

const gcs = require('@google-cloud/storage')(config);

const bucket = gcs.bucket('admin-speakenglishsite.appspot.com');

function uploadBucket(file, destination, name) {
  return new Promise((resolve, reject) => {

    if(!file && typeof file !== 'object')
      return reject(new Error('Not send file'));
    if(!destination && typeof destination !== 'string')
      return reject( new Error('Not send destination'));
    if(!name && typeof name !== 'string')
      return reject( new Error('Not send name'));

    let correctURL, type, ref, fileUpload, uuid = uuid4();

    if(destination.indexOf('/') > -1){
      correctURL = destination.split('/').map(docs => `${docs}`);
    }else
      correctURL = `${destination}/`;

    type = file.mimetype.split('/')[1];

    ref = `${correctURL.join('/')}/${name}.${type}`;
    
    fileUpload = bucket.file(ref);

    const blobStream = fileUpload.createWriteStream({
      metadata: {
        contentType: file.mimetype,
        metadata: {
          firebaseStorageDownloadTokens: uuid
        }
      }
    })
    blobStream.on('error', error => reject(error))
    
    blobStream.on('finish', res => {
      let url = `https://firebasestorage.googleapis.com/v0/b/${bucket.name}/o/${correctURL.join('%2F')}%2F${name}.${type}?alt=media&token=${uuid}`;

      return resolve({ success: true, reference: {url, ref}, uploads: false})
    })

    blobStream.end(file.buffer);
  })
}
/* eslint-disable */
const uploadsBucket = async(function(files, destination, name){
/* eslint-enable */
  if(typeof files !== 'object'  && Object.keys(files).length > 1)
    return Promise.reject(new Error('Error parameter files, is not a array'));

  let response = [];

  for(var key in files) {
    if(Array.isArray(files[key])){
      response[key] = [];
      for(e in files[key]){
        response[key][e] = await (uploadBucket(files[key][e], destination, `${name}${Date.now()}`));
      }
    }else {
      response[key] = await (uploadBucket(files[key][0], destination, `${name}${Date.now()}`));
    }
  }

  return Promise.resolve({ success: true, reference: response, uploads: true });
})

function deleteBucket(ref) {
  return new Promise((resolve, reject) => {
    if(!ref)
      return reject(new Error('Not send reference the file to deleted'));
    

    const file = bucket.file(ref);

    return file.delete(() => {
      
      return resolve(true); 
    })
  })
}

module.exports = {
  uploadBucket,
  uploadsBucket,
  deleteBucket
};