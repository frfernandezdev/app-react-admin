module.exports = {
  uploadFile: require('./uploads').uploadBucket,
  uploadFiles: require('./uploads').uploadsBucket,
  deleteFile: require('./uploads').deleteBucket
}