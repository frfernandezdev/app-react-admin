const 	getRawBody     = require('raw-body'),
				contentType    = require('content-type'),
        Busboy 				 = require('busboy');
        
function MultipartGcs(options){}

MultipartGcs.prototype._parseRequest = function(req, res, next){
  if(req.rawBody === undefined && req.method === 'POST' && req.headers['content-type'].startsWith('multipart/form-data')){
    getRawBody(req, {
      length: req.headers['content-length'],
      limit: '10mb',
      encoding: contentType.parse(req).parameters.charset
    }, function(err, string){
      if (err) return next(err)
      req.rawBody = string
      return next()
    })
  } else {
    return next()
  }
}
MultipartGcs.prototype._makeMiddleware = function(fields, fileStrategy){
  return (req, res, next) => {
    var busboy,
        file = {},
        fileBuffer = new Buffer('');

    req.body = Object.create(null);

    try {
      busboy = new Busboy({ headers: req.headers })
    }catch(err) {
      return next(err);
    }

    busboy.on('field', (fieldname, value) => {
      req.body[fieldname] = value;
    })
    busboy.on('file', (fieldname, fileStream, filename, encoding, mimetype) => {
      fileStream.on('data', data => {
        fileBuffer = Buffer.concat([fileBuffer, data]);
      })
      fileStream.on('end', () => {
        const file_object = {
          fieldname: fieldname,
          originalname: filename,
          encoding: encoding,
          mimetype: mimetype,
          buffer: fileBuffer
        }
        let array = [];
        
        if(file.hasOwnProperty(fieldname)){
          array = file[fieldname].concat(file_object);
        }else array = [file_object];
        
        Object.defineProperty(file, fieldname, {
          configurable: true,
          enumerable: true,
          writable: true,
          value: array
        })
      })
    })
    busboy.on('finish', () => {
      req.files = file;
      return next()
    })
    busboy.end(req.rawBody);
    req.pipe(busboy);
  }
}
MultipartGcs.prototype.single = function(name){
  return this._makeMiddleware([{ name: name, maxCount: 1 }],'VALUE')
}
MultipartGcs.prototype.array = function(name, maxCount){
  return this._makeMiddleware([{ name: name, maxCount: maxCount }], 'ARRAY')
}

function multipartGcs(options){
  if(options === undefined)
    return new MultipartGcs({})
  if(typeof options === 'object' && options !== null)
    return new MultipartGcs(options)
}

module.exports = multipartGcs;