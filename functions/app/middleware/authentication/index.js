const { auth } = require('../../../firebase');
const { Users } = require('../../objects');

const User = new Users();

class Authentication {
	constructor(){
		this.Authentication = this.Authentication.bind(this)
	}
	Authentication(req, res, next){
		var query 	= req.query,
				params 	= req.params,
				body 		= req.body; 

		var token = query.token ? query.token : params.token ? params.token : body.token ? body.token : false;
		
		if(token)
			return auth.verifyIdToken(token)
				.then(
					data => {
						if(data){
							return auth.getUser(data.uid)
						}else
							req.user = false;
							return next();
					}
				)
				.then(
					data => {
						if(data){
							req.user = Object.assign(data, req.user);
							return User.findOne(data.uid)
						}else
							req.user = false;
							return next();
					}
				)
				.then(
					response => {
						if(response.exists){
							req.user = Object.assign(response.data(), req.user)
							return next();
						}else {
							req.user = false; 
							return next();
						}
					}
				)
				.catch(error => {
					console.log(error)
					req.user = false;
					return next();
				})
		else
			req.user = false;
			return next()
	}
}

module.exports = Authentication;