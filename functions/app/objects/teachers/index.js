const { db, auth }    = require('../../../firebase'),
        Users         = require('../users'),
        DB            = db.collection('teachers'),
        async         = require('asyncawait/async'),
        await         = require('asyncawait/await');

const User = new Users(); 

const processUpdate = async((body, old) => {
  let data = {};
  if(body.video && body.video !== old.video)
    data.video = body.video;
  if(body.fileDni && body.fileDni !== old.fileDni && body.fileDniRef && body.fileDniRef !== old.fileDniRef){
    data.fileDni    = body.fileDni;
    data.fileDniRef = body.fileDniRef;
  }
  if(body.aboutMe && body.aboutMe !== old.aboutMe)
    data.aboutMe = body.aboutMe;
  if(body.methodology && body.methodology !== old.methodology)
    data.methodology = body.methodology;
  if(body.student && old.student){
    let student = [];
    if(body.student.length === old.student.length){
      await (body.student.forEach((docs, i) => {
        student[i] = {
          description:   body.student[i].description   !== old.student[i].description   ? body.student[i].description   : old.student[i].description, 
          start:         body.student[i].start         !== old.student[i].start         ? body.student[i].start         : old.student[i].start,
          end:           body.student[i].end           !== old.student[i].end           ? body.student[i].end           : old.student[i].end,
          especial_team: body.student[i].especial_team !== old.student[i].especial_team ? body.student[i].especial_team : old.student[i].especial_team,
          institute:     body.student[i].institute     !== old.student[i].institute     ? body.student[i].institute     : old.student[i].institute,
          title:         body.student[i].title         !== old.student[i].title         ? body.student[i].title         : old.student[i].title 
        }
        if(old.student[i].file && old.student[i].file.hasOwnProperty('file') && old.student[i].file.hasOwnProperty('ref')){
          student[i].file = {};
          if(body.student[i].file){
            student[i].file.file = body.student[i].file.file !== old.student[i].file.file ? body.student[i].file.file : old.student[i].file.file;
            student[i].file.ref  = body.student[i].file.ref  !== old.student[i].file.ref  ? body.student[i].file.ref  : old.student[i].file.ref;
          }else {
            student[i].file.file = old.student[i].file.file;
            student[i].file.ref  = old.student[i].file.ref;
          }
        }
      }))
    }else student = body.student;
    data.student = student;
  }
  if(body.professional_expirence && old.professional_expirence){
    let professional_expirence = [];
    if(body.professional_expirence.length === old.professional_expirence){
      await (body.professional_expirence.forEach((docs, i) => {
        professional_expirence[i] = {
          start:       body.professional_expirence[i].start       !== old.professional_expirence[i].start       ? body.professional_expirence[i].start       : old.professional_expirence[i].start,
          end:         body.professional_expirence[i].end         !== old.professional_expirence[i].end         ? body.professional_expirence[i].end         : old.professional_expirence[i].end,
          city:        body.professional_expirence[i].city        !== old.professional_expirence[i].city        ? body.professional_expirence[i].city        : old.professional_expirence[i].city,
          description: body.professional_expirence[i].description !== old.professional_expirence[i].description ? body.professional_expirence[i].description : old.professional_expirence[i].description,
          enterprise:  body.professional_expirence[i].enterprise  !== old.professional_expirence[i].enterprise  ? body.professional_expirence[i].enterprise  : old.professional_expirence[i].enterprise
        }
        if(old.professional_expirence[i].country && old.professional_expirence[i].country.hasOwnProperty('country') && old.professional_expirence[i].country.hasOwnProperty('state')){
          professional_expirence[i].country = {};
          if(body.professional_expirence[i].file){
            professional_expirence[i].country.country = body.professional_expirence[i].country.country !== old.professional_expirence[i].country.country ? body.professional_expirence[i].country.country : old.professional_expirence[i].country.country;
            professional_expirence[i].country.state   = body.professional_expirence[i].country.state   !== old.professional_expirence[i].country.state   ? body.professional_expirence[i].country.state   : old.professional_expirence[i].country.state;
          }else {
            professional_expirence[i].country.country = old.professional_expirence[i].country.country;
            professional_expirence[i].country.state   = old.professional_expirence[i].country.state;
          }
        }
        if(old.professional_expirence[i].file && old.professional_expirence[i].file.hasOwnProperty('file') && old.professional_expirence[i].file.hasOwnProperty('ref')){
          professional_expirence[i].file = {};
          if(body.professional_expirence[i].file){
            professional_expirence[i].file.file = body.professional_expirence[i].file.file !== old.professional_expirence[i].file.file ? body.professional_expirence[i].file.file : old.professional_expirence[i].file.file;
            professional_expirence[i].file.ref  = body.professional_expirence[i].file.ref  !== old.professional_expirence[i].file.ref  ? body.professional_expirence[i].file.ref  : old.professional_expirence[i].file.ref;
          }else {
            professional_expirence[i].file.file = old.professional_expirence[i].file.file;
            professional_expirence[i].file.ref  = old.professional_expirence[i].file.ref;
          }
        }
      }))
    }else professional_expirence = body.professional_expirence;
    data.professional_expirence = professional_expirence;
  }
  if(body.schedule && old.schedule){
    let schedule = [];
    if(body.schedule.length === old.schedule.length){
      await (body.schedule.forEach((docs, i) => {
        schedule[i] = {
          date:   body.schedule[i].date   !== old.schedule[i].date   ? body.schedule[i].date   : old.schedule[i].date,
          active: body.schedule[i].active !== old.schedule[i].active ? body.schedule[i].active : old.schedule[i].active             
        }
      }))
    }else schedule = body.schedule;
    data.schedule = schedule;
  }
  if(body.languageToLearn && old.languageToLearn){
    let languageToLearn = [];
    if(body.languageToLearn.length === old.languageToLearn.length){
      await (body.languageToLearn.forEach((docs, i) => {
        languageToLearn[i].language = body.languageToLearn[i].language !== old.languageToLearn[i].language ? body.languageToLearn[i].language : old.languageToLearn[i].language; 
        languageToLearn[i].level    = body.languageToLearn[i].level    !== old.languageToLearn[i].level    ? body.languageToLearn[i].level    : old.languageToLearn[i].level;        
      }))
    }else languageToLearn = body.languageToLearn;
    data.languageToLearn = languageToLearn;
  }
  if(body.skill && old.skill){
    data.skill = {};
    data.skill.junnior   = body.skill.hasOwnProperty('junnior')      && body.skill.junnior   !== old.skill.junnior   ? body.skill.junnior   : old.skill.junnior   ? old.skill.junnior   : false;
    data.skill.kids      = body.skill.hasOwnProperty('kids')         && body.skill.kids      !== old.skill.kids      ? body.skill.kids      : old.skill.kids      ? old.skill.kids      : false;
    data.skill.teen      = body.skill.hasOwnProperty('teen')         && body.skill.teen      !== old.skill.teen      ? body.skill.teen      : old.skill.teen      ? old.skill.teen      :false;
    data.skill.bussiness = body.skill.hasOwnProperty('bussiness')    && body.skill.bussiness !== old.skill.bussiness ? body.skill.bussiness : old.skill.bussiness ? old.skill.bussiness : false;
    data.skill.test = {};
    data.skill.test.IELTS = body.skill.test.hasOwnProperty('IELTS') && body.skill.test.IELTS   !== old.skill.test.IELTS  ? body.skill.test.IELTS : old.skill.test.IELTS ? old.skill.test.IELTS : false;
    data.skill.test.BEC   = body.skill.test.hasOwnProperty('BEC')   && body.skill.test.BEC     !== old.skill.test.BEC    ? body.skill.test.BEC   : old.skill.test.BEC   ? old.skill.test.BEC   : false ;
    data.skill.test.TOEFL = body.skill.test.hasOwnProperty('TOEFL') && body.skill.test.TOEFL   !== old.skill.test.TOEFL  ? body.skill.test.TOEFL : old.skill.test.TOEFL ? old.skill.test.TOEFL : false;
    data.skill.test.PET   = body.skill.test.hasOwnProperty('PET')   && body.skill.test.PET     !== old.skill.test.PET    ? body.skill.test.PET   : old.skill.test.PET   ? old.skill.test.PET   : false;
    data.skill.test.TOEIC = body.skill.test.hasOwnProperty('TOEIC') && body.skill.test.TOEIC   !== old.skill.test.TOEIC  ? body.skill.test.TOEIC : old.skill.test.TOEIC ? old.skill.test.TOEIC : false;
    data.skill.test.CAE   = body.skill.test.hasOwnProperty('CAE')   && body.skill.test.CAE     !== old.skill.test.CAE    ? body.skill.test.CAE   : old.skill.test.CAE   ? old.skill.test.CAE   : false;
    data.skill.test.DELE  = body.skill.test.hasOwnProperty('DELE')  && body.skill.test.DELE    !== old.skill.test.DELE   ? body.skill.test.DELE  : old.skill.test.DELE  ? old.skill.test.DELE  : false;
    data.skill.test.CPE   = body.skill.test.hasOwnProperty('CPE')   && body.skill.test.CPE     !== old.skill.test.CPE    ? body.skill.test.CPE   : old.skill.test.CPE   ? old.skill.test.CPE   : false;
    data.skill.test.CELU  = body.skill.test.hasOwnProperty('CELU')  && body.skill.test.CELU    !== old.skill.test.CELU   ? body.skill.test.CELU  : old.skill.test.CELU  ? old.skill.test.CELU  : false;
    data.skill.test.KET   = body.skill.test.hasOwnProperty('KET')   && body.skill.test.KET     !== old.skill.test.KET    ? body.skill.test.KET   : old.skill.test.KET   ? old.skill.test.KET   : false;
    data.skill.test.FCE   = body.skill.test.hasOwnProperty('FCE')   && body.skill.test.FCE     !== old.skill.test.FCE    ? body.skill.test.FCE   : old.skill.test.FCE   ? old.skill.test.FCE   : false;
    data.skill.test.ILEC  = body.skill.test.hasOwnProperty('ILEC')  && body.skill.test.ILEC    !== old.skill.test.ILEC   ? body.skill.test.ILEC  : old.skill.test.ILEC  ? old.skill.test.ILEC  : false;
  }
  if(body.materialLearn && old.materialLearn){
    data.materialLearn = {};
    data.materialLearn.file_pdf       = body.materialLearn.hasOwnProperty('file_pdf')       && body.materialLearn.file_pdf       !== old.materialLearn.file_pdf        ? body.materialLearn.file_pdf       : old.materialLearn.file_pdf       ? old.materialLearn.file_pdf        : false;
    data.materialLearn.template_quiz  = body.materialLearn.hasOwnProperty('template_quiz')  && body.materialLearn.template_quiz  !== old.materialLearn.template_quiz   ? body.materialLearn.template_quiz  : old.materialLearn.template_quiz  ? old.materialLearn.template_quiz   : false;
    data.materialLearn.file_text      = body.materialLearn.hasOwnProperty('file_text')      && body.materialLearn.file_text      !== old.materialLearn.file_text       ? body.materialLearn.file_text      : old.materialLearn.file_text      ? old.materialLearn.file_text       : false;
    data.materialLearn.file_image     = body.materialLearn.hasOwnProperty('file_image')     && body.materialLearn.file_image     !== old.materialLearn.file_image      ? body.materialLearn.file_image     : old.materialLearn.file_image     ? old.materialLearn.file_image      : false;
    data.materialLearn.article_news   = body.materialLearn.hasOwnProperty('article_news')   && body.materialLearn.article_news   !== old.materialLearn.article_news    ? body.materialLearn.article_news   : old.materialLearn.article_news   ? old.materialLearn.article_news    : false;
    data.materialLearn.grafic_table   = body.materialLearn.hasOwnProperty('grafic_table')   && body.materialLearn.grafic_table   !== old.materialLearn.grafic_table    ? body.materialLearn.grafic_table   : old.materialLearn.grafic_table   ? old.materialLearn.grafic_table    : false;
    data.materialLearn.presentation   = body.materialLearn.hasOwnProperty('presentation')   && body.materialLearn.presentation   !== old.materialLearn.presentation    ? body.materialLearn.presentation   : old.materialLearn.presentation   ? old.materialLearn.presentation    : false;
    data.materialLearn.questionnaries = body.materialLearn.hasOwnProperty('questionnaries') && body.materialLearn.questionnaries !== old.materialLearn.questionnaries  ? body.materialLearn.questionnaries : old.materialLearn.questionnaries ?  old.materialLearn.questionnaries : false;
    data.materialLearn.task           = body.materialLearn.hasOwnProperty('task')           && body.materialLearn.task           !== old.materialLearn.task            ? body.materialLearn.task           : old.materialLearn.task           ? old.materialLearn.task            : false;
    data.materialLearn.file_video     = body.materialLearn.hasOwnProperty('file_video')     && body.materialLearn.file_video     !== old.materialLearn.file_video      ? body.materialLearn.file_video     : old.materialLearn.file_video     ? old.materialLearn.file_video      : false;
    data.materialLearn.file_audio     = body.materialLearn.hasOwnProperty('file_audio')     && body.materialLearn.file_audio     !== old.materialLearn.file_audio      ? body.materialLearn.file_audio     : old.materialLearn.file_audio     ? old.materialLearn.file_audio      : false;
  }
  return Promise.resolve(data);
})

class Teachers {
  constructor(){
    this.find = this.find.bind(this);
    this.findOne = this.findOne.bind(this);
    this.create = this.create.bind(this);
    this.update = this.update.bind(this);
    this.delete = this.delete.bind(this);
  }
  clean(docs) {
    let r = [];
    for (let i = 0; i < docs.length; i++) {
      if(docs[i]){
        r.push(docs[i]);
      }
    }

    return Promise.all(r)
  }
  prepareResponseFind(collection){
    let response = [];

    for(const iterator of collection){
      let data = iterator.data();
      data.id = iterator.id;
      response.push(
        DB
          .doc(data.id)
          .get()
          .then(docs => {
            if(docs.exists){
              let a = docs.data();
              delete data.token && data.online && data.priv;
              a.id = docs.id;
              a.user = data;
              return a;
            }else return;
          })
      )
    }
    return Promise.all(response)
      .then(this.clean)
  }
  prepareResponseFindOne(doc){
    let response = {},
        data     = doc.data();

    return DB
      .doc(doc.id)
      .get()
      .then(
        response => {
          if(response.exists){
            let a = response.data();
            delete data.token && data.online && data.priv;
            data.id = doc.id
            a.user = data;
            return Promise.resolve(a);
          }else 
            return Promise.resolve({})
        },
        error => Promise.reject(error)
      )
      .then(
        response => Promise.resolve(response),
        error => Promise.reject(error)  
      )
      .catch(err => Promise.reject(err));
  }
  count() {
    return DB
      .get();
  }
  findAll(where, orderBy, limit, start){

    return User.find(where, orderBy, limit, start)
      .then(
        response => {
          if(response.size > 0)
            return this.prepareResponseFind(response.docs)
          else return []
        },
        error => Promise.reject(error)
      )
      .catch(err => Promise.reject(err))
  }
  find(where, order, limit) {
    var query;

    if(where)
			query = DB.where(where.field, where.operator, where.value).limit(limit || 10);
		// if(orderBy)
    //   query = DB.orderBy(orderBy.name, orderBy.value).limit(limit || 10);
    // if(where && orderBy)
    //   query = DB.where(where.field, where.operator, where.value).orderBy(orderBy.name, orderBy.value).limit(limit || 10);
    if(!query)
      query = DB.limit(limit || 10);

    return DB.get();
  }
  findOne(filter){
    if(!Object.keys(filter).length > 0)
      return Promise.reject(new Error('No hay enviado parametro para buscar'))
    
    return User.findOne(filter)
      .then(
        response => {
          if(response.exists)
            return this.prepareResponseFindOne(response)
          else return []
        },
        error => Promise.reject(error)
      )
      .catch(err => Promise.reject(err))
  }
  create(body){      
    return DB
      .doc(body.user)
      .set(body);
  }
  update(old, body, id){ 
    return processUpdate(body, old)
      .then(
        response => {
          console.log(response, 'object')
          if(Object.keys(response).length > 0)
            return DB
              .doc(id)
              .update(response)
              .then(() => Promise.resolve(id))
              .catch(error => Promise.reject(error))
          else {
            /* eslint-disable */
              return Promise.reject('No se ha enviado datos para modificar')
            /* eslint-enable */
          }
        },
        error => Promise.reject(error)
      )
  }
  delete(id){
    if(id)
      return DB
        .doc(id)
        .delete();
    else {
      /* eslint-disable */
      return Promise.reject('No se ha enviado el Id')
      /* eslint-enable */
    } 
  }
}

module.exports = Teachers