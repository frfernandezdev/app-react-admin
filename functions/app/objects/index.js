module.exports = {
	Users: 				require('./users'),
	Languages:  	require('./languages'),
	Moneys:     	require('./money'),
	Applications: require('./applications'),
	Teachers:      require('./teachers')
}