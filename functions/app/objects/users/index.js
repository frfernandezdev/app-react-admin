const { db, auth } 		= require('../../../firebase'),
				DB 	 					= db.collection('users'),
			{ uploadFile }	= require('../../utils');

class Users {
	constructor(){
		this.find    = this.find.bind(this)
		this.findOne = this.findOne.bind(this)
		this.create  = this.create.bind(this);
		this.update  = this.update.bind(this);
		this.delete  = this.delete.bind(this);
	}
	find(where, order, limit){
		var query;

		if(where)
			query = DB.where(where.field, where.operator, where.value).limit(limit || 10);
		// if(orderBy)
    //   query = DB.orderBy(orderBy.name, orderBy.value).limit(limit || 10);
    // if(where && orderBy)
    //   query = DB.where(where.field, where.operator, where.value).orderBy(orderBy.name, orderBy.value).limit(limit || 10);
    if(!query)
      query = DB.limit(limit || 10);

		return DB.get(); 
	}
	findOne(filter){
		
		if(!Object.keys(filter).length > 0){
			/* eslint-disable */
      return Promise.reject('No hay enviado parametro para buscar')
			/* eslint-enable */
		}
		
		return DB
						.doc(filter)
						.get();
	}
	create(body, file){
		var id;

		if(!Object.keys(body).length > 0){
			/* eslint-disable */
      return Promise.reject('No hay enviado parametro para guardar')
			/* eslint-enable */
		}

		let auth = auth.createUser({
			email: body.email,
			password: body.password,
			displayName: body.username,
			phoneNumber: body.mobile
		});

		if(Object.keys(file).length) {
			return auth.then(
					response => {
						id = response.uid;
						let destination = `users/${id}/thumbnails`,
								name = `${id}_${Date.now()}`;
						
						return uploadFile(file, destination, name);
					}
				)
				.then(response => {
					if(response){
						body.photo = response.reference.url;
						body.photoRef = response.reference.ref;
					}
					delete body.password
					return DB
									.doc(id)
									.set(body) 
				})
				.then(() => Promise.resolve(id))
				.catch(error => Promise.reject(error))
		}else {
			return auth.then(
				response => {
					id = response.uid;

					delete body.password
					return DB
									.doc(id)
									.set(body)
				}
			)
			.then(() => Promise.resolve(id))
			.catch(error => Promise.reject(error))
		}
	}
	update(user ,body ,id){
		var data 				= {},
				currentData = {};

		if(!Object.keys(body).length > 0){
			/* eslint-disable */
			return Promise.reject('No hay enviado parametro para actualizar')
			/* eslint-enable */
		}

		if(body.email && user.email !== body.email){
			data.email        = body.email;
			currentData.email = body.email;
		}
		if(body.password)
			currentData.password = body.password;
		if(body.username && user.username !== body.username){
			data.username 			    = body.username;
			currentData.displayName = body.username;
		}
		if(body.mobile && user.mobile !== body.mobile){
			data.mobile 			      = body.mobile;
			currentData.phoneNumber = body.mobile
		}
		if(body.fullname && user.fullname !== body.fullname)
			data.fullname = body.fullname;
		if(body.gender && user.gender !== body.gender)
			data.gender = body.gender;
		if(body.birthdate && user.birthdate !== body.birthdate)
			data.birthdate = body.birthdate;
		if(body.phone && user.phone !== body.phone)
			data.phone = body.phone;
		if(body.address && user.address !== body.address)
			data.address = body.address;
		if(body.priv && Number.isInteger(priv) && user.priv !== body.priv)
			data.priv = body.priv;
		if(body.photo && user.photo !== body.photo)
			data.photo = body.photo;
		if(body.photoRef && user.photoRef !== body.photoRef)
			data.photoRef = body.photoRef;
		if(body.company && user.company !== body.company)
			data.company = body.company;
		if(body.watch && Number.isInteger(body.watch) && user.watch !== body.watch)
			data.watch = body.watch;
		if(body.money && user.money !== body.money)
			data.money = body.money;
		if(body.website && user.website !== body.website)
			data.website = body.website;
		if(body.liveLocalities && Array.isArray(body.liveLocalities))
			data.liveLocalities = body.liveLocalities;
		if(body.bornLocalities && Array.isArray(body.bornLocalities))
			data.bornLocalities = body.bornLocalities;
		if(body.languageNative && Array.isArray(body.languageNative))
			data.languageNative = body.languageNative;
		if(body.language && Array.isArray(body.language))
			data.language = body.language;
		if(body.timezone && user.timezone.text !== body.timezone.text)
			data.timezone = body.timezone;
		if(body.tools && body.tools.skype && user.tools.skype !== body.tools.skype){
			data.tools = Object.assign({}, data.tools);
			data.tools.skype = body.tools.skype;
		}
		if(body.tools && body.tools.facetime && user.tools.facetime !== body.tools.facetime){
			data.tools = Object.assign({}, data.tools);
			data.tools.facetime = body.tools.facetime;
		}
		if(body.tools && body.tools.gHangouts && user.tools.gHangouts !== body.tools.gHangouts){
			data.tools = Object.assign({}, data.tools);
			data.tools.gHangouts = body.tools.gHangouts;
		}
		if(body.tools && body.tools.qq && user.tools.qq !== body.tools.qq){
			data.tools = Object.assign({}, data.tools)
			data.tools.qq = body.tools.qq;
		}
		if(body.tools && body.tools.wechat && user.tools.wechat !== body.tools.wechat){
			data.tools = Object.assign({}, data.tools);
			data.tools.wechat = body.tools.wechat;
		}
		if(data.username && data.fullname)
			data.search = (data.username + data.fullname).toLowerCase().replace(/\s/g, '')

		return auth.updateUser(id, currentData)
			.then(
				response => {
					if(Object.keys(data).length > 0)
						return DB
										.doc(id)
										.update(data)
					else 
						return Promise.resolve(response.toJSON())
				}
			)
			.then(() => Promise.resolve(id))
			.catch(error => Promise.reject(error))
	}
	delete(id){
		if(id)
			return auth.deleteUser(id)
				.then(response => {
					return DB
									.doc(id)
									.delete();
				})
				.catch(error => Promise.reject(error))
		else {
			/* eslint-disable */
			return Promise.reject('No se ha enviado el Id')
			/* eslint-enable */
		} 
	}
}

module.exports = Users;