const { db, auth } = require('../../../firebase'),
        DB         = db.collection('currentMoney');

class Moneys {
  constructor(){
    this.find = this.find.bind(this);
    this.findOne = this.findOne.bind(this);
    this.create = this.create.bind(this);
    this.update = this.update.bind(this);
    this.delete = this.delete.bind(this);
  }
  find(where, order, limit){
    if(where)
      DB.where(where);
    if(order)
      DB.orderBy(order);
    if(limit)
      DB.limit(limit);
    
      return DB.get();
  }
  findOne(filter){

    if(!Object.keys(filter).length > 0){
      /* eslint-disable */
      return Promise.reject('No hay enviado parametro para buscar')
      /* eslint-enable */
    }

    return DB
            .doc(filter)
            .get();
  }
  create(body){

    if(!Object.keys(body).length > 0){
      /* eslint-disable */
      return Promise.reject('No hay enviado parametro para guardar')
      /* eslint-enable */
    }

    return DB
            .add(body)
  }
  update(old, body, id){
    var data = {};

    if(!Object.keys(body).length > 0){
      /* eslint-disable */
			return Promise.reject('No hay enviado parametro para actualizar')
      /* eslint-enabled */
    }

    if(body.acronym && old.acronym !== body.acronym)
      data.acronym = body.acronym;
    if(body.name && old.name !== body.name)
      data.name = body.name;
    
    if(Object.keys(data).length > 0)
      return DB
              .doc(id)
              .update(data)
              .then(() => Promise.resolve(id))
              .catch(error => Promise.reject(error))
    else {
      /* eslint-disable */
      return Promise.reject('No hay datos al modificar')
      /* eslint-enable */
    } 
  }
  delete(id){
    if(id)
      return DB
              .doc(id)
              .delete();
    else {
      /* eslint-disable */
      return Promise.reject('No se ha enviado el Id')
      /* eslint-enable */
    } 
  }
}

module.exports = Moneys;