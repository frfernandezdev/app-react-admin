const { db, auth }     = require('../../../firebase'),
        Users          = require('../users'),
        DB             = db.collection('applications');

const User = new Users();

class Applications {
  constructor(){
    this.find = this.find.bind(this);
    this.findOne = this.findOne.bind(this);
    this.create = this.create.bind(this);
    // this.update = this.update.bind(this);
    this.delete = this.delete.bind(this);
  } 
  clean(docs) {
    let r = [];

    for (let i = 0; i < docs.length; i++) {
      if(docs[i]){
        r.push(docs[i]);
      }
    }   
    return Promise.all(r)
  }
  prepareResponseFind(collection){
    let response = [];

    for(const iterator of collection){
      let data = iterator.data();
      data.id = iterator.id;
      response.push(
        DB
          .doc(data.id)
          .get()
          .then(docs => {
            if(docs.exists){
              let a = docs.data();
              delete data.token && data.online && data.priv;
              a.id = docs.id;
              a.user = data;
              return a;
            }else return;
          })
      )
    }
    return Promise.all(response)
      .then(this.clean)
  }
  prepareResponseFindOne(doc){
    let response = {},
        data     = doc.data();

    return DB
      .doc(doc.id)
      .get()
      .then(
        response => {
          if(response.exists){
            let a = response.data();
            delete data.token && data.online && data.priv;
            data.id = doc.id
            a.user = data;
            return Promise.resolve(a);
          }else 
            return Promise.resolve({})
        },
        error => Promise.reject(error)
      )
      .then(
        response => Promise.resolve(response),
        error => Promise.reject(error)  
      )
      .catch(err => Promise.reject(err));
  }
  count() {
    return DB
            .get();
  }
  findAll(where, orderBy, limit, start){

    return User.find(where, orderBy, limit, start)
      .then(
        response => {
          if(response.size > 0)
            return this.prepareResponseFind(response.docs)
          else return []
        },
        error => Promise.reject(error)
      )
      .catch(err => Promise.reject(err))
  }
  find(where, order, limit){
    var query;

		if(where)
			query = DB.where(where.field, where.operator, where.value).limit(limit || 10);
		// if(orderBy)
    //   query = DB.orderBy(orderBy.name, orderBy.value).limit(limit || 10);
    // if(where && orderBy)
    //   query = DB.where(where.field, where.operator, where.value).orderBy(orderBy.name, orderBy.value).limit(limit || 10);
    if(!query)
      query = DB.limit(limit || 10);

    return DB.get();
  }
  findOne(filter){

    if(!Object.keys(filter).length > 0)
      return Promise.reject(new Error('No hay enviado parametro para buscar'))
    
    return User.findOne(filter)
      .then(
        response => {
          if(response.exists){
            return this.prepareResponseFindOne(response)
          }
          else return []
        },
        error => Promise.reject(error)
      )
      .catch(err => Promise.reject(err))
  }
  create(body){      
    return DB
            .doc(body.user)
            .set(body);
  }
  // update(old, body, id){
  //   var data = {};    

  //   data = this.processUpdate(body, old);
    
  //   if(Object.keys(data).length > 0)
  //     return DB
  //             .doc(id)
  //             .update(data)
  //             .then(() => Promise.resolve(id))
  //             .catch(error => Promise.reject(error))
  //   else {
  //     /* eslint-disable */
  //       return Promise.reject('No se ha enviado datos para modificar')
  //     /* eslint-enable */
  //   }
  // }
  delete(id){
    if(id)
      return DB
              .doc(id)
              .delete();
    else {
      /* eslint-disable */
      return Promise.reject('No se ha enviado el Id')
      /* eslint-enable */
    } 
  }
}

module.exports = Applications;