const { functions }    			= require('./firebase'),
				cors 					 			= require('cors'),
				bodyParser 		 			= require('body-parser'),
				express 			 			= require('express'),
				{ 
					Authentication,
					MultipartGcs  
				}  									= require('./app/middleware');

const {
	Auth,
	Users,
	Languages,
	Money,
	Applications,
	Teachers
} = require('./app/routes');

const authentication = new Authentication();

const multipart = MultipartGcs({})

const app = express();

app.use(cors({ origin: true }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(multipart._parseRequest)

app.use(authentication.Authentication);
app.use('/auth', Auth);
app.use('/users', Users);
app.use('/languages', Languages);
app.use('/money', Money);
app.use('/applications', Applications);
app.use('/teachers', Teachers);

app.get('/', (req, res) => {
	// res.json(req.user)
	res.json('Hello, this is RESTful APIs the SpeakEnglishsite.com')
})
app.post('/', multipart.array(), (req, res ,next) => {
	console.log(req.body, req.files)
	res.json('hello');
})

exports.api = functions.https.onRequest(app);

